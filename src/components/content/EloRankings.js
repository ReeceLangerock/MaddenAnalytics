import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Container,
	Button,
	Divider,
	Header,
	Modal,
	Segment,
	Icon,
	Table
} from 'semantic-ui-react'
import { connect } from 'react-redux'
import gen from './../../content_generation/eloRankings.js'
import * as actions from './../../redux/actions/actions'
import LeagueDashboard from '../league/LeagueDashboard';
import { VictoryLine, VictoryChart, VictoryTheme } from 'victory';
import TeamStyleSheet from './../../TeamStyleSheet.js'
import AutoAd from './../general/ads/AutoAd'
import Loader from './../general/Loader'


class EloRankings extends Component {
	constructor(props) {
		super(props)

		this.handleClick = this.handleClick.bind(this)

		this.state = {
			eloCompleted: false,
			currentSeasonOnly: false

		}
	}

	async componentWillMount() {
		if (!this.props.teams || !this.props.weeklyData || this.props.eloRankings) {
			return
		}
		this.formatChartData(this.props.eloRankings)
	}



	renderRankingsTable() {
		let data = this.props.eloRankings

		if (!data) {
			return
		}

		let sorted = Object.keys(data).map((key, index) => {

			return {
				name: data[key].name,
				elo: data[key].elo,
				teamID: key
			}
		})

		const sortedTeams = sorted.sort((a, b) => {
			return b.elo[b.elo.length - 2] - a.elo[a.elo.length - 2]
		})

		const teamsWithPreviousRank = sortedTeams.map((team, index) => {
			team.previousRank = index
			return team
		})


		const finalSortedTeams = teamsWithPreviousRank.sort((a, b) => {
			return b.elo[b.elo.length - 1] - a.elo[a.elo.length - 1]
		})

		return finalSortedTeams.map((key, index) => {
			let icon
			if (key.previousRank - index > 0) {
				icon = <Icon name="arrow up" style={{ color: 'green' }} />
			} else if (key.previousRank - index < 0) {
				icon = <Icon name="arrow down" style={{ color: 'red' }} />
			}
			return (
				<Table.Row key={key.teamID}>
					<Table.Cell textAlign="center">{index + 1} </Table.Cell>
					<Table.Cell textAlign="center"> {key.name}</Table.Cell>
					<Table.Cell textAlign="center">{key.elo[key.elo.length - 1].toPrecision(6) || 0}</Table.Cell>
					<Table.Cell textAlign="center">{(key.elo[key.elo.length - 1] - key.elo[key.elo.length - 2]).toPrecision(4) || 0}</Table.Cell>
					{(key.elo.length >= 2) &&
						<Table.Cell textAlign="center">{key.elo[key.elo.length - 2].toPrecision(6) || 0}</Table.Cell>}
					{(key.elo.length < 2) &&
						<Table.Cell textAlign="center">-</Table.Cell>}

					<Table.Cell textAlign="center">{key.elo.length - 1}</Table.Cell>
					<Table.Cell textAlign="center">{key.previousRank + 1}</Table.Cell>
					<Table.Cell textAlign="center">
						{key.previousRank - index}{' '}
						{icon}
					</Table.Cell>

				</Table.Row>
			)
		})
	}



	renderModal() {
		return (
			<Modal trigger={<Button circular icon='question' />}>
				<Modal.Header>Elo Rating System</Modal.Header>
				<Modal.Content >

					<Modal.Description>
						<Header>The System Explained:</Header>
						<p>The ELO Rating system is based on a relatively simple system developed by the physicist Arpad Elo to rate chess players. The website FiveThirtyEight adapted the system to extend to Football, and we have tweaked it further to work with Madden. Teams gain and lose ground based on the final score of each game and how unexpected the result was in the eyes of the pregame ratings</p>
						<Header>Calculating Elo Ratings:</Header>
						<p>Elo ratings are theoretically simple. The standard method of calculating and implementing an Elo rating system starts with setting every team at a starting value of 1500.
						 As teams play each other, the expected score of team A and team B can be calculated using the logistic curve with the following formula:</p>
						<img alt='expected score formula' src="https://model284.com/wp-content/ql-cache/quicklatex.com-02fc5f9d55c2ea55cd51376446fd5328_l3.svg" />
						<br />
						<img alt='expected score formula' src="https://model284.com/wp-content/ql-cache/quicklatex.com-32ac0a68236cde36b1d775a4dc410846_l3.svg" />
						<br />

						<p>After the game is played, each team’s new rating is calculated as the difference between their expected score and their actual score, meaning underdogs receive a larger bump to their rating for a win than favorites would. The last important component is the K-factor, a constant term that determines how drastically each new game changes a team’s Elo rating.</p>
						<p>As teams play each other throughout the season, ratings will adjust, and teams will begin to separate themselves based on their performances. Thus, Elo ratings provide a good ranking that tracks performance over time.</p>
						<Header>Changes Made For Madden:</Header>
						<p>Madden will never be able to completely capture the dynamics of the NFL, so we made a few tweaks to the ELO to try to accurately consider user skills:</p>
						<p>First, the original system gives 2.6 points of homefield advantage. We believe Madden does give a slight homefield advantage ( and we're working to quantify this), but we adjust it to half a point of advantage.</p>
						<p>Second, ELO reverts a team by 1/3 after each season to account for all the changes that take place during the offseason. While this is still important in Madden, user skills is more important than the players on the team so we revert it by 1/5.  </p>
						<p>Third, ELO uses a K factor of 20 to determine how important each individual game is. Because a typical Madden connected franchise will last six or seven seasons, and because there is turnover in users, we bumped the K factor up to 22.5</p>
						<p>Fourth, the rating of a team is much more reliant on the user than it is on the team. To account for this a teams rating will be reverted by 1/2 when a new user for that team is detected.</p>

					</Modal.Description>
				</Modal.Content>
			</Modal>)
	}

	formatChartData(elo) {

	}

	renderChartLines() {

		let elo = this.props.eloRankings

		return Object.keys(elo).map((team) => {
			let strokeColor = 'black'
			try {
				
				strokeColor = TeamStyleSheet.teams[elo[team].name].primary;
			} catch(e){}
			return (
				<VictoryLine key={`${team}eloLineGraph `}
					style={{
						data: { stroke: `rgb(${strokeColor}` },
						parent: { border: "1px solid #ccc" }
					}}
					data={elo[team].elo}

				/>)

		})


	}

	async handleClick() {

		const elo = await gen.calculateEloRankings(this.props.weeklyData, this.props.teams, !this.state.currentSeasonOnly)
		this.setState({currentSeasonOnly: !this.state.currentSeasonOnly })
		this.formatChartData(elo)
		this.props.saveEloRankings(elo)

	}


	render() {
		return (
			<Container>
				<LeagueDashboard />
				<Segment>
					<HeaderContainer>
						<Header
							as="h1"
							content="ELO Rating System"
						/>
						<Button toggle active={this.state.currentSeasonOnly} onClick={this.handleClick}>
							Season {this.props.league.currentSeason + 1} Only
      </Button>

						{this.renderModal()}

					</HeaderContainer>
				</Segment>


				{(!this.props.eloRankings) &&
					<Loader loaderMessage="Generating ELO Ratings" />}
				{this.props.eloRankings &&
					<Container>

						<Table celled unstackable singleline="true" compact>
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell textAlign="center">#</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Team</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">ELO</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">
										ELO +/-
								</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">
										Prev. ELO
								</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">
										Games Played
									</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">
										Prev. Rank
									</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">
										Rank +/-
									</Table.HeaderCell>

								</Table.Row>
							</Table.Header>

							<Table.Body>{this.renderRankingsTable()}


							</Table.Body>
						</Table>

						
						<Divider style={{ margin: '3rem 0' }} />


						<HeaderContainer>
							<Header
								as="h1"
								content={`The Complete History Of ${this.props.league.name}`}
								subheader='Every franchise’s relative strength after every game.'
							/>

							<p>Messy Work in Progress... </p>

						</HeaderContainer>

						<VictoryChart
							theme={VictoryTheme.material}
						>
							{this.renderChartLines()}
						</VictoryChart>
						


					</Container>
				}
			</Container>
		)
	}
}

const mapStateToProps = state => ({
	weeklyData: state.leagueReducer.weeklyData,
	league: state.leagueReducer.league,
	teams: state.leagueReducer.teams,
	eloRankings: state.contentReducer.eloRankings
})
export default connect(mapStateToProps, actions)(EloRankings)

const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`