import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Container,
	Button,
	Modal,
	Dimmer, Loader,
	Header,
	Segment,
	Icon,
	Table
} from 'semantic-ui-react'
import { connect } from 'react-redux'
import AutoAd from './../general/ads/AutoAd'

import * as actions from './../../redux/actions/actions'
import LeagueDashboard from '../league/LeagueDashboard';

class Pythagorean extends Component {
	constructor(props) {
		super(props)

		this.state = {
			pythCompleted: false,
			pythagoreanData: []

		}
	}

	renderRankingsTable() {
		let data = this.props.pythagoreanData
		if (!this.props.pythagoreanData) {

			return
		}

		return Object.keys(data).map((key, index) => {
			// let gamesPlayed = data[key].wins || 0 + data[key].losses || 0
			let icon
			if (data[key].previousRank - index > 0) {
				icon = <Icon name="arrow up" style={{ color: 'green' }} />
			} else if (data[key].previousRank - index < 0) {
				icon = <Icon name="arrow down" style={{ color: 'red' }} />
			}
			return (
				<Table.Row key={key}>
					<Table.Cell textAlign="center">{index + 1} </Table.Cell>
					<Table.Cell textAlign="center"> {data[key].name}</Table.Cell>
					<Table.Cell textAlign="center">{data[key].wins || 0}</Table.Cell>
					<Table.Cell textAlign="center">
						{(data[key].pythagoreanScore.toPrecision(3) || 0)}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{((data[key].wins ||0) - data[key].pythagoreanScore).toPrecision(3)}{' '}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{((data[key].pointsScored))}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{((data[key].pointsAllowed))}
					</Table.Cell>
					<Table.Cell textAlign="center">{data[key].previousRank + 1}</Table.Cell>

					<Table.Cell textAlign="center">
						{data[key].previousRank - index}{' '}
						{icon}
					</Table.Cell>
				</Table.Row>
			)
		})
	}

	renderLoader() {
		if (this.props.pythagoreanData === undefined) {




			return (<Segment key='loader' style={{ height: '300px' }}>

				<Dimmer active style={{ height: '300px' }}>
					<Loader size='massive' content='Generating Rankings' />
				</Dimmer>
			</Segment>)

		}

	}

	renderModal() {
		return (
			<Modal trigger={<Button circular icon='question' />}>
				<Modal.Header>Football’s Pythagorean Theorem</Modal.Header>
				<Modal.Content >

					<Modal.Description>
						<Header>The Theorem Explained:</Header>
						<p>The principle, made famous by baseball analyst Bill James, that states that the record of a baseball team can be approximated by taking the square of team runs scored and dividing it by the square of team runs scored plus the square of team runs allowed. Statistician Daryl Morey later extended this theorem to other sports including professional football. Teams that win a game or more over what the Pythagorean theorem would project tend to regress the following year; teams that lose a game or more under what the Pythagorean theorem would project tend to win more the following year, particularly if they were 8-8 or better despite underachieving.</p>

						<Header>How It Works:</Header>
						<p>The formula to calculate a team's Pythagorean Expected Wins is</p>
						<code>Expected Wins = (Points For ^ 2.37) / (Points For ^ 2.37 + Points Against ^ 2.37) * Games Played</code>

						<Header>Why It Works:</Header>
						<p>Because all wins aren’t created equal. Take for example Kansas City beats the Raiders 28-0 in a game in which they forced six interceptions. Pretty impressive.
						A week later, they beat the Chargers 23-20 in overtime. For each of those two performances, Kansas City got the same exact mark on their record: one win. Nobody in his right mind would think that Kansas City looked equally good in both of those games, even if they got the same result. That’s where the “All that matters is the W” argument falls apart. It’s like saying the pass/fail system is just as useful as the traditional grading scale when figuring out how well somebody did in a class.</p>

						<Header>Understanding the Table</Header>
						<p>The table you see <strike>(which is sortable by any field)</strike>, ranks the teams by their <strong>Expected Wins</strong>.
			The <strong>Win Differential</strong> is the difference between how many games a team actually won versus how many they are expected ot have won based on their points scored and allowed.
			A positive <strong>Win differential</strong> indicates that a team is over achieving, or winning more games than their point differential would indicate. Conversely, a negaitve <strong>Win Differential</strong> indicates they are underperforming, and losing more games than would be expected.</p>

					</Modal.Description>
				</Modal.Content>
			</Modal>)
	}

	render() {

		return (
			<Container>
				<LeagueDashboard />
				<Segment>
					<HeaderContainer>
						<Header
							as="h1"
							content="Pythagorean Power Rankings"
						/>

						{this.renderModal()}

					</HeaderContainer>

				</Segment>
				{this.renderLoader()}

				{this.props.pythagoreanData &&

					<Container style={{ overflowX: 'scroll' }}>
						<Table celled unstackable singleline="true" compact>
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell textAlign="center">#</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Team</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wins</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">
										Expected Wins
								</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">
										Win Differential
								</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">
										Points Scored
								</Table.HeaderCell><Table.HeaderCell textAlign="center">
										Points Allowed
								</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Prev. Rank</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Rank +/-</Table.HeaderCell>
								</Table.Row>
							</Table.Header>

							<Table.Body>{this.renderRankingsTable()}</Table.Body>
						</Table>

					</Container>
				}
				
			</Container>
		)
	}
}

const mapStateToProps = state => ({
	league: state.leagueReducer.league,
	pythagoreanData: state.contentReducer.pythRankings
})
export default connect(mapStateToProps, actions)(Pythagorean)


const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`