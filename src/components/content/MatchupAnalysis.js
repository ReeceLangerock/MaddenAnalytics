import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Container,
	Button,
	Modal,
	Dimmer, Loader,
	Header,
	Segment,
	List,
	Image,
	Grid,
	Dropdown,
} from 'semantic-ui-react'
import { connect } from 'react-redux'
import util from './../../content_generation/contentUtilities'

import * as actions from './../../redux/actions/actions'
import LeagueDashboard from '../league/LeagueDashboard';
import MatchupHeader from './MatchupHeader'
import gen from './../../content_generation/eloRankings.js'
import AutoAd from './../general/ads/AutoAd'

class Schedule extends Component {
	constructor(props) {
		super(props)

		this.state = {
			selectedWeek: undefined

		}
	}

	componentWillMount() {
	}

	 componentWillReceiveProps(newProps) {
		 
	
	}



	renderMatchups() {

		

		let matchups = this.props.matchupAnalysis
		let SW = this.state.selectedWeek || this.props.league.currentWeek
		let CS = this.props.league.currentSeason

		if (!matchups) {

			return
		}

		if (!matchups[SW]) {

			return <p>Schedule for week {SW + 1} hasn't been received yet</p>
		}
		const weeksMatchups = matchups[SW]
		return weeksMatchups.map((m) => {

			if(!m){
				return
			}
			
			
			// GENERATE USER VS USER STATS
			const homeGP = (m.homeTeam.wins || 0 ) + (m.homeTeam.losses || 0) + (m.homeTeam.ties || 0) 
			const awayGP = (m.awayTeam.losses || 0 ) + (m.awayTeam.losses || 0) + (m.awayTeam.ties || 0) 
			const ht_OU = m.homeTeam.pointsScored / homeGP
			const at_OU = m.awayTeam.pointsScored / awayGP


			// GATHER TEAM VS TEAM STATS
let lastTeamMatchupSeason, lastTeamMatchupWeek
			try {

				lastTeamMatchupWeek = m.teamMatchups[m.teamMatchups.length - 1].week >= 0 ? m.teamMatchups[m.teamMatchups.length - 1].week :  null
				lastTeamMatchupSeason = m.teamMatchups[m.teamMatchups.length - 1].season >= 0 ? m.teamMatchups[m.teamMatchups.length - 1].season :  null

				if(lastTeamMatchupWeek > 16){ // 16 because week is zero indexed
					lastTeamMatchupWeek = util.getPlayoffWeekName(lastTeamMatchupWeek)
				}
			} catch(e){}
			let htLogo, atLogo
			try {
				htLogo = require(`./../../assets/logos/${(m.homeTeamName).toLowerCase()}.svg`)
				atLogo = require(`./../../assets/logos/${(m.awayTeamName).toLowerCase()}.svg`)
	
			} catch (e) {
				htLogo = require(`./../../assets/logos/notfound.jpg`)
				atLogo = require(`./../../assets/logos/notfound.jpg`)
	
			}
			return (

				<Segment>
				<Grid >
				<Grid.Row>
				  <Grid.Column width={5}>
					<Header style = {{marginBottom: 0, textAlign: 'center'}}>{m.awayTeamName} ({m.awayTeamCSWins || 0}-{m.awayTeamCSLosses || 0}-{m.awayTeamCSTies || 0})</Header>
					<Image centered src={atLogo} width = {200} height = {200}/>
				  </Grid.Column>
				  <Grid.Column width={6} style ={{
					 display: 'flex',
					 alignItems: 'center', 
					 fontSize: '28px',
					 justifyContent: 'center',
					 padding: '.1rem'
				   }} >
					vs.
				  </Grid.Column>
				  <Grid.Column width={5}>
					<Header style = {{marginBottom: 0, textAlign: 'center'}}>{m.homeTeamName} ({m.homeTeamCSWins || 0}-{m.homeTeamCSLosses || 0}-{m.homeTeamCSTies || 0})</Header>
				  
					<Image centered src={htLogo} width = {200} height = {200}/>
					
				  </Grid.Column>
				</Grid.Row>


				<Grid.Row>
				  <Grid.Column width={6} style ={{
					 padding: '.5rem'
				   }} >
				  <Segment>

				  <h2>{m.awayUserName}</h2>

				<List>
					<List.Item>ELO Rating: {(m.awayElo).toFixed(1)}</List.Item>
					<List.Item>Record With Team: {m.awayTeam.wins || 0}-{m.awayTeam.losses || 0}-{m.awayTeam.ties || 0}</List.Item>
					<List.Item>Avg Points Scored: {((m.awayTeam.pointsScored / awayGP).toFixed(1) || 0)}</List.Item>
					<List.Item>Avg Points Allowed: {(m.awayTeam.pointsAllowed / awayGP).toFixed(1)}</List.Item>
				</List>
	  
					  </Segment>
				  </Grid.Column>

				  <Grid.Column width ={4}/>
				  <Grid.Column width={6} style ={{
					 padding: '.5rem'
				   }} >
				  <Segment>

				  <h2>{m.homeUserName}</h2>

				  <List>
					<List.Item>ELO Rating: {(m.homeElo).toFixed(1)}</List.Item>
					<List.Item>Record With Team: {m.homeTeam.wins || 0}-{m.homeTeam.losses || 0}-{m.homeTeam.ties || 0}</List.Item>
					<List.Item>Avg Points Scored: {(m.homeTeam.pointsScored / homeGP).toFixed(1)}</List.Item>
					<List.Item>Avg Points Allowed: {(m.homeTeam.pointsAllowed / homeGP).toFixed(1)}</List.Item>
				</List>
					  </Segment>
				  </Grid.Column>
				 
				
				 
				</Grid.Row>


				<Grid.Row>
				
				  <Grid.Column width={11}>
  <p>{m.gamesPlayed}{m.recordText}</p>


				  {(lastTeamMatchupSeason && typeof lastTeamMatchupWeek === 'number') &&
	<p>Last Matchup: Season {lastTeamMatchupSeason+ 1} - Week {lastTeamMatchupWeek + 1}</p>
					
				  }
				   {(lastTeamMatchupSeason && typeof lastTeamMatchupWeek === 'string') &&
	<p>Last Matchup: Season {lastTeamMatchupSeason+ 1} - {lastTeamMatchupWeek}</p>
					
				  }
				  {(lastTeamMatchupSeason === undefined || lastTeamMatchupWeek === undefined) &&
					  <p>This is the first matchup between the {m.awayTeamName} and the {m.homeTeamName}</p>
				  }
				  
				  </Grid.Column>
				  <Grid.Column width={5}>
			<Segment>
				  <Header as="h4">{m.homeTeamName}: {(Math.round(-m.spread*2)/2).toFixed(1)}</Header>
				  <p as="h5">{m.homeTeamName} O/U: {(Math.round(ht_OU*2)/2).toFixed(1)}</p>
					<Header as="h4">{m.awayTeamName}: {(Math.round(m.spread*2)/2).toFixed(1)}</Header>
					<p as="h5">{m.awayTeamName} O/U: {(Math.round(at_OU*2)/2).toFixed(1)}</p>
					<Header as="h4">Over/Under: {(Math.round((ht_OU+at_OU)*2)/2).toFixed(1)}</Header>
					
				</Segment>
				  
				  </Grid.Column>
				

				 
				
				 
				</Grid.Row>
			  </Grid>
			  </Segment>

			)
		})
	}

	renderLoader() {
		if (this.state.pythCompleted === false) {




			return (<Segment key='loader' style={{ height: '300px' }}>

				<Dimmer active style={{ height: '300px' }}>
					<Loader size='massive' content='Generating Rankings' />
				</Dimmer>
			</Segment>)

		}

	}

	r
	handleChange = (e, { value }) => {
		this.setState({ selectedWeek: value })
	}
	render() {
		const options = [
			{ key: 0, text: 'Week One', value: 0 },
			{ key: 1, text: 'Week Two', value: 1 },
			{ key: 2, text: 'Week Three', value: 2 },
			{ key: 3, text: 'Week Four', value: 3 },
			{ key: 4, text: 'Week Five', value: 4 },
			{ key: 5, text: 'Week Six', value: 5 },
			{ key: 6, text: 'Week Seven', value: 6 },
			{ key: 7, text: 'Week Eight', value: 7 },
			{ key: 8, text: 'Week Nine', value: 8 },
			{ key: 9, text: 'Week Ten', value: 9 },
			{ key: 10, text: 'Week Eleven', value: 10 },
			{ key: 11, text: 'Week Twelve', value: 11 },
			{ key: 12, text: 'Week Thirteen', value: 12 },
			{ key: 13, text: 'Week Fourteen', value: 13 },
			{ key: 14, text: 'Week Fifteen', value: 14 },
			{ key: 15, text: 'Week Sixteen', value: 15 },
			{ key: 16, text: 'Week Seventeen', value: 16 },
		]
		const value = this.state.selectedWeek
		return (
			<Container>
				 <LeagueDashboard />
				<Segment>
					<HeaderContainer>
						<Header
							as="h1"
							content="Matchup Analysis"
						/>
						<Dropdown
							onChange={this.handleChange}
							options={options}
							placeholder='Choose an option'
							selection
							value={value}
						/>

					</HeaderContainer>

				</Segment>
				{/* {this.renderLoader()} */}

				{!this.state.pythCompleted &&

					this.renderMatchups()}
							 
					
			</Container>

		)
	}
}

const mapStateToProps = state => ({
	matchupAnalysis: state.contentReducer.matchupAnalysis,
	league: state.leagueReducer.league,

	
})
export default connect(mapStateToProps, actions)(Schedule)


const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`