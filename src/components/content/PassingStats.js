import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Container,
	Header,
	Segment,
	Table, Image
} from 'semantic-ui-react'
import { connect } from 'react-redux'

import * as actions from './../../redux/actions/actions'
import LeagueDashboard from '../league/LeagueDashboard';
import Loader from './../general/Loader'
import Filter from './../general/Filter'

import AutoAd from './../general/ads/AutoAd'

class PassingStats extends Component {
	constructor(props) {
		super(props)

		this.state = {
			selectedWeek: undefined,
			passStats: [],
			column: null,
			direction: null,

		}
	}
	componentWillMount() {
		if (this.props.passStats) {
			this.setState({ passStats: this.props.passStats })
		}
	}


	componentWillReceiveProps(newProps) {
		if (newProps.passStats) {
			this.setState({ passStats: newProps.passStats })
		}
	}

	renderStats() {
		let stats = this.props.passStats
		const filter = this.props.filter
		let index = 0

		return stats.map((player) => {
			if (!filter.positions[player.position]) {
				return null
			}
			if (player.passAtt < filter.minAttempts) {
				return null;
			}
			if (!filter.teams[player.teamName]) {
				return null;
			}
			index++

			let image;
			try {
				image = require(`./../../assets/logos/${player.teamName.toLowerCase()}.svg`)

			} catch (e) {
				image = require(`./../../assets/logos/notfound.jpg`)

			}
			player.adjCompPercentage = player.adjCompPercentage > 1 ? 1 : player.adjCompPercentage
			return (
				<Table.Row key={player._id + 'passStats' + index} >
					<Table.Cell textAlign="center">{index} </Table.Cell>
					<Table.Cell ><Image style={{ borderRadius: '0' }} src={image} avatar />   {player.firstName + ' ' + player.lastName}</Table.Cell>
					<Table.Cell textAlign="center">
						{player.passComp}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{player.passAtt}
					</Table.Cell>
					<Table.Cell textAlign="center">{player.passYds || 0}</Table.Cell>
					<Table.Cell textAlign="center">{(player.compPercentage * 100).toFixed(2) || 0}</Table.Cell>
					<Table.Cell textAlign="center">{(player.adjCompPercentage * 100).toFixed(2) || 0}</Table.Cell>
					<Table.Cell textAlign="center">{(player.assumedDrops).toFixed(1) || 0}</Table.Cell>
					<Table.Cell textAlign="center">
						{(player.ypa).toFixed(2)}
					</Table.Cell>

					<Table.Cell textAlign="center">
						{player.passTDs}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{(player.intRate * 100).toFixed(2)}% ({player.passInts})
					</Table.Cell>
					<Table.Cell textAlign="center">
						{(player.sackRate * 100).toFixed(2)}% ({player.passSacks})

					</Table.Cell>



					<Table.Cell textAlign="center">
						{(player.tdRate * 100).toFixed(2)}%
						</Table.Cell>
					<Table.Cell textAlign="center">
						{(player.qbr).toFixed(2)}

					</Table.Cell>
					<Table.Cell textAlign="center">
						{(player.ncaa).toFixed(2)}

					</Table.Cell>
				</Table.Row>
			)
		})
	}


	handleSort = clickedColumn => () => {
		const { column, passStats, direction } = this.state

		if (column !== clickedColumn) {

			passStats.sort((a, b) => {
				return b[clickedColumn] - a[clickedColumn]
			})

			this.setState({
				column: clickedColumn,
				passStats: passStats,
				direction: 'ascending',
			})

			return
		}
		if (direction === 'descending') {

			passStats.sort((a, b) => {
				return b[clickedColumn] - a[clickedColumn]
			})

		} else if (direction === 'ascending') {
			passStats.sort((a, b) => {
				return a[clickedColumn] - b[clickedColumn]
			})
		}

		this.setState({
			passStats, 
			direction: direction === 'ascending' ? 'descending' : 'ascending',
		})


	}
	handleChange = (e, { value }) => {
		this.setState({ selectedWeek: value })
	}
	render() {

		const { column, direction } = this.state
		return (
			<Container>
				<LeagueDashboard />
				<Segment>
					<HeaderContainer>
						<Header
							as="h1"
							content="Advanced Passing Statistics"
						/>
						<Filter minDescription='Pass Attempts' />

					</HeaderContainer>

				</Segment>
				{(this.props.passStats === undefined) &&
					<Loader loaderMessage="Generating Passing Stats" />}

				{this.props.passStats &&

					<Container style={{ overflowX: 'scroll' }}>
							
					
						<Table celled sortable unstackable singleline="true" compact>
							<Table.Header >
								<Table.Row>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center">#</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Player</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'passComp' ? direction : null} onClick={this.handleSort('passComp')}>
										Pass Comp.
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'passAtt' ? direction : null} onClick={this.handleSort('passAtt')}>
										Pass Att.
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'passYds' ? direction : null} onClick={this.handleSort('passYds')} > Yards</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'compPercentage' ? direction : null} onClick={this.handleSort('compPercentage')} >Completion %</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'adjCompPercentage' ? direction : null} onClick={this.handleSort('adjCompPercentage')} > Adj. Completion %</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'assumedDrops' ? direction : null} onClick={this.handleSort('assumedDrops')} >Receiver Drops</Table.HeaderCell>

									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'ypa' ? direction : null} onClick={this.handleSort('ypa')} >YPA</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'passTDs' ? direction : null} onClick={this.handleSort('passTDs')} >
										TD
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'intRate' ? direction : null} onClick={this.handleSort('intRate')} >
										Int Rate
		</Table.HeaderCell><Table.HeaderCell textAlign="center" sorted={column === 'sackRate' ? direction : null} onClick={this.handleSort('sackRate')} >
										Sack Rate
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'tdRate' ? direction : null} onClick={this.handleSort('tdRate')} >TD Rate</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'qbr' ? direction : null} onClick={this.handleSort('qbr')} >Passer Rating</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'ncaa' ? direction : null} onClick={this.handleSort('ncaa')} >NCAA Passer Rating</Table.HeaderCell>

								</Table.Row>
							</Table.Header>

							<Table.Body>{this.renderStats()}</Table.Body>
						</Table>

					</Container>


				}
							

			</Container>

		)
	}
}

const mapStateToProps = state => ({
	league: state.leagueReducer.league,
	passStats: state.contentReducer.passStats,
	filter: state.filterReducer.filter,


})
export default connect(mapStateToProps, actions)(PassingStats)


const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`