import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Container,

	Header,
	Segment,
	Table, Image
} from 'semantic-ui-react'
import { connect } from 'react-redux'

import * as actions from './../../redux/actions/actions'
import LeagueDashboard from '../league/LeagueDashboard';
import Loader from './../general/Loader'
import Filter from './../general/Filter'
import AutoAd from './../general/ads/AutoAd'

class RecStats extends Component {
	constructor(props) {
		super(props)

		this.state = {
			selectedWeek: undefined,
			recStats: [],
			teamFilter: undefined,
			column: null,
			direction: null,

		}
	}

	componentWillMount() {
		if (this.props.recStats) {
			this.setState({ recStats: this.props.recStats })
		}
	}

	componentWillReceiveProps(newProps) {


		if (newProps.recStats) {
			this.setState({ recStats: newProps.recStats })
		}
	}


	renderStats() {
		let stats = this.props.recStats
		const filter = this.props.filter

		return stats.map((player, index) => {
			if (!filter.positions[player.position]) {
				return null
			}
			if (player.recCatches < filter.minAttempts) {
				return null;
			}
			if (!filter.teams[player.teamName]) {
				return null;
			}
			let image;
			try {
				image = require(`./../../assets/logos/${player.teamName.toLowerCase()}.svg`)

			} catch (e) {
				image = require(`./../../assets/logos/notfound.jpg`)

			}
			return (
				<Table.Row key={player._id + 'recStats'}>
					<Table.Cell textAlign="center">{index + 1} </Table.Cell>
					<Table.Cell ><Image style={{ borderRadius: '0' }} src={image} avatar />   {player.firstName + ' ' + player.lastName}</Table.Cell>
					<Table.Cell textAlign="center">
						{player.recCatches}
					</Table.Cell>
					<Table.Cell textAlign="center">{player.recYds || 0}</Table.Cell>
					<Table.Cell textAlign="center">{(player.ypc).toFixed(2)}</Table.Cell>

					<Table.Cell textAlign="center">
						{player.recTDs}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{player.depthOfRec.toFixed(2)}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{player.yac.toFixed(2)}
					</Table.Cell>
					<Table.Cell textAlign="center">{(player.dropRate * 100).toFixed(2)}%</Table.Cell>

					<Table.Cell textAlign="center">
						{(player.tdRate * 100).toFixed(2)}%
					</Table.Cell>
				</Table.Row>
			)
		})
	}

	handleSort = clickedColumn => () => {
		const { column, recStats, direction } = this.state

		if (column !== clickedColumn) {

			recStats.sort((a, b) => {
				return b[clickedColumn] - a[clickedColumn]
			})

			this.setState({
				column: clickedColumn,
				recStats: recStats,
				direction: 'ascending',
			})

			return
		}
		if (direction === 'descending') {

			recStats.sort((a, b) => {
				return b[clickedColumn] - a[clickedColumn]
			})

		} else if (direction === 'ascending') {
			recStats.sort((a, b) => {
				return a[clickedColumn] - b[clickedColumn]
			})
		}

		this.setState({
			recStats: recStats,
			direction: direction === 'ascending' ? 'descending' : 'ascending',
		})


	}
	handleChange = (e, { value }) => {
		this.setState({ selectedWeek: value })
	}
	render() {

		const { column, direction } = this.state

		return (
			<Container>
				<LeagueDashboard />
				<Segment>
					<HeaderContainer>
						<Header
							as="h1"
							content="Advanced Receiving Statistics"
						/>
						<Filter minDescription='Catches' />

					</HeaderContainer>

				</Segment>
				{(this.props.recStats === undefined) &&

					<Loader loaderMessage="Generating Receiving Stats" />}

				{this.props.recStats &&

					<Container style={{ overflowX: 'scroll' }}>
							
					
						<Table celled sortable unstackable singleline="true" compact>
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell textAlign="center">#</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Player</Table.HeaderCell>
									<Table.HeaderCell textAlign="center" sorted={column === 'recCatches' ? direction : null} onClick={this.handleSort('recCatches')}>
										Catches
		</Table.HeaderCell>
									<Table.HeaderCell textAlign="center" sorted={column === 'recYds' ? direction : null} onClick={this.handleSort('recYds')} > Yards</Table.HeaderCell>
									<Table.HeaderCell textAlign="center" sorted={column === 'ypc' ? direction : null} onClick={this.handleSort('ypc')} >YPC</Table.HeaderCell>
									<Table.HeaderCell textAlign="center" sorted={column === 'recTDs' ? direction : null} onClick={this.handleSort('recTDs')} >
										TD
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'depthOfRec' ? direction : null} onClick={this.handleSort('depthOfRec')} >
										Depth of Catch
		</Table.HeaderCell><Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'yac' ? direction : null} onClick={this.handleSort('yac')} >
										Yards after Catch
		</Table.HeaderCell>
									<Table.HeaderCell textAlign="center" sorted={column === 'dropRate' ? direction : null} onClick={this.handleSort('dropRate')} >Drop Rate</Table.HeaderCell>
									<Table.HeaderCell textAlign="center" sorted={column === 'tdRate' ? direction : null} onClick={this.handleSort('tdRate')} >TD Rate</Table.HeaderCell>
								</Table.Row>
							</Table.Header>

							<Table.Body>{this.renderStats()}</Table.Body>
						</Table>

					</Container>


				}


			</Container>

		)
	}
}

const mapStateToProps = state => ({
	league: state.leagueReducer.league,
	recStats: state.contentReducer.recStats,
	filter: state.filterReducer.filter

})
export default connect(mapStateToProps, actions)(RecStats)


const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;
justify-content: space-between;`