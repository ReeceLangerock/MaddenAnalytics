import React, { Component } from 'react'
import styled from 'styled-components'

import {
    Container,
    Button,
    Modal,
    Dimmer, Loader,
    Header,
    Image,
    Segment,
    Grid,
    Dropdown,
    Table
} from 'semantic-ui-react'
import { connect } from 'react-redux'

import * as actions from './../../redux/actions/actions'
import LeagueDashboard from '../league/LeagueDashboard';
import gen from './../../content_generation/fantasyPoints.js'
import AutoAd from './../general/ads/AutoAd'


class Schedule extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedWeek: undefined,
            generating: false

        }
    }

    componentWillMount() {
        this.setState({ selectedWeek: this.props.league.currentWeek - 1 })

        this.generateFantasyRankings()
    }

    componentWillReceiveProps(newProps) {

        if (newProps.league && !this.state.selectedWeek) {
            if (newProps.league.currentWeek === 21) {
                this.setState({ selectedWeek: 19 })

            } else if (newProps.league.currentWeek === 0) {

                this.setState({ selectedWeek: newProps.league.currentWeek })
            } else {
                this.setState({ selectedWeek: newProps.league.currentWeek - 1 })

            }

        }
        if (this.props.fantasyRankings === [] || !this.props.fantasyRankings
        ) {
            this.generateFantasyRankings()
        }
    }

    async generateFantasyRankings() {

        if (!this.props.players || !this.props.passingData || !this.props.rushingData || !this.props.receivingData || !this.props.teams) {
            return
        }

        const s = this.props.league.currentSeason
        const end = this.props.league.currentWeek > 21 ? 21 : this.props.league.currentWeek

        const fantasyRankings = []
        for (let i = 0; i < end; i++) {
            if (i === 20) {
                fantasyRankings[i] = [];
                continue;
            }
            fantasyRankings[i] = await gen.calculateFantasyPoints(this.props.players, this.props.passingData[s][i], this.props.rushingData[s][i], this.props.receivingData[s][i], this.props.teams)
        }


        if (fantasyRankings.length > 0) {

            const [playerMetaData, studsAndDuds] = await gen.calculateFantasyStudsAndDuds(fantasyRankings)
            this.props.savePlayerFantasyRankings(fantasyRankings)
            this.props.saveSAD(studsAndDuds)
            this.props.saveFantasyMetaData(playerMetaData)
        }
    }



    renderQbRanks(position) {

        const week = this.state.selectedWeek
        const orderedPlayers = this.props.fantasyRankings
        let metaData = this.props.metaData

        if (!orderedPlayers || !metaData) {
            return
        }
        if (!orderedPlayers[week]) {
            return
        }
        let playerRank = 0

        return orderedPlayers[week].map((p, index) => {



            if (p.position === 'QB' && playerRank < 10) {
                let numTopTen = 0;
                for (let i = 0; i <= week; i++) {
                    let add = metaData[p._id].qbTopTen[i] ? 1 : 0
                    numTopTen += add
                }

                playerRank++
                let image;
                try {
                    image = require(`./../../assets/logos/${p.teamName.toLowerCase()}.svg`)

                } catch (e) {
                    image = require(`./../../assets/logos/notfound.jpg`)

                }
                return (<Table.Row key={p._id}>
                    <Table.Cell textAlign="center">{playerRank} </Table.Cell>
                    <Table.Cell><Image style={{ borderRadius: '0' }} src={image} avatar />  {p.firstName + ' ' + p.lastName}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.fantasyPoints.toFixed(1)}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.passYds}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.passTDs}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.passInts}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.rushYds || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.rushTDs || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.rushFum || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {numTopTen || 0}</Table.Cell>
                </Table.Row>)
            } else {
                return null;
            }
        })
    }

    renderHbRanks() {
        const week = this.state.selectedWeek
        const orderedPlayers = this.props.fantasyRankings
        let metaData = this.props.metaData
        if (!orderedPlayers || !metaData) {
            return
        }

        if (!orderedPlayers[week]) {
            return
        }
        let playerRank = 0
        return orderedPlayers[week].map((p, index) => {

            if (p.position === 'HB' && playerRank < 10) {
                let numTopTen = 0;
                for (let i = 0; i <= week; i++) {
                    let add = metaData[p._id].hbTopTen[i] ? 1 : 0
                    numTopTen += add
                }
                playerRank++
                let image;
                try {
                    image = require(`./../../assets/logos/${p.teamName.toLowerCase()}.svg`)

                } catch (e) {
                    image = require(`./../../assets/logos/notfound.jpg`)

                }
                return (<Table.Row key={p._id}>
                    <Table.Cell textAlign="center">{playerRank} </Table.Cell>
                    <Table.Cell ><Image style={{ borderRadius: '0' }} src={image} avatar />  {p.firstName + ' ' + p.lastName}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.fantasyPoints.toFixed(1)}</Table.Cell>

                    <Table.Cell textAlign="center"> {p.rushYds || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.rushTDs || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.rushFum || 0}</Table.Cell>

                    <Table.Cell textAlign="center"> {p.recYds || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.recTDs || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.recCatches || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {numTopTen || 0}</Table.Cell>

                </Table.Row>)
            } else {
                return null;
            }
        })
    }

    renderReceiverRanks(position) {
        const week = this.state.selectedWeek
        const orderedPlayers = this.props.fantasyRankings
        let metaData = this.props.metaData

        if (!orderedPlayers || !metaData) {
            return null
        }

        if (!orderedPlayers[week]) {
            return null
        }
        let playerRank = 0
        return orderedPlayers[week].map((p, index) => {

            if (p.position === position && playerRank < 10) {
                let numTopTen = 0;
                let add;
                for (let i = 0; i <= week; i++) {
                    if (position === 'WR') {
                        add = metaData[p._id].wrTopTen[i] ? 1 : 0

                    }

                    if (position === 'TE') {
                        add = metaData[p._id].teTopTen[i] ? 1 : 0

                    }

                    numTopTen += add
                }
                playerRank++
                let image;
                try {
                    image = require(`./../../assets/logos/${p.teamName.toLowerCase()}.svg`)

                } catch (e) {
                    image = require(`./../../assets/logos/notfound.jpg`)

                }
                return (<Table.Row key={p._id}>
                    <Table.Cell textAlign="center">{playerRank} </Table.Cell>
                    <Table.Cell ><Image style={{ borderRadius: '0' }} src={image} avatar />  {p.firstName + ' ' + p.lastName}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.fantasyPoints.toFixed(1)}</Table.Cell>

                    <Table.Cell textAlign="center"> {p.recYds || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.recTDs || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.recCatches || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.rushYds || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.rushTDs || 0}</Table.Cell>
                    <Table.Cell textAlign="center"> {p.rushFum || 0}</Table.Cell>


                    <Table.Cell textAlign="center"> {numTopTen || 0}</Table.Cell>

                </Table.Row>)
            } else {
                return null;
            }
        })
    }

    renderLoader() {
        if (this.props.fantasyRankings === undefined || this.props.fantasyRankings === []) {

            return (<Segment key='loader' style={{ height: '300px' }}>

                <Dimmer active style={{ height: '300px' }}>
                    <Loader size='massive' content='Generating Rankings' />
                </Dimmer>
            </Segment>)

        }

    }

    renderModal() {
        return (
            <Modal trigger={<Button circular icon='question' />}>
                <Modal.Header>Matchup</Modal.Header>
                <Modal.Content >

                    <Modal.Description>
                        <Header>The Theorem Explained:</Header>
                        <p>The principle, made famous by baseball analyst Bill James, that states that the record of a baseball team can be approximated by taking the square of team runs scored and dividing it by the square of team runs scored plus the square of team runs allowed. Statistician Daryl Morey later extended this theorem to other sports including professional football. Teams that win a game or more over what the Pythagorean theorem would project tend to regress the following year; teams that lose a game or more under what the Pythagorean theorem would project tend to win more the following year, particularly if they were 8-8 or better despite underahieving.</p>

                        <Header>How It Works:</Header>
                        <p>The formula to calculate a team's Pythagorean Expected Wins is</p>
                        <code>Expected Wins = (Points For ^ 2.37) / (Points For ^ 2.37 + Points Against ^ 2.37) * Games Played</code>

                        <Header>Why It Works:</Header>
                        <p>Because all wins aren’t created equal. Take for example Kansas City beats the Raiders 28-0 in a game in which they forced six interceptions. Pretty impressive.
						A week later, they beat the Chargers 23-20 in overtime. For each of those two performances, Kansas City got the same exact mark on their record: one win. Nobody in his right mind would think that Kansas City looked equally good in both of those games, even if they got the same result. That’s where the “All that matters is the W” argument falls apart. It’s like saying the pass/fail system is just as useful as the traditional grading scale when figuring out how well somebody did in a class.</p>

                        <Header>Understanding the Table</Header>
                        <p>The table you see <strike>(which is sortable by any field)</strike>, ranks the teams by their <strong>Expected Wins</strong>.
			The <strong>Win Differential</strong> is the difference between how many games a team actually won versus how many they are expected ot have won based on their points scored and allowed.
			A positive <strong>Win differential</strong> indicates that a team is over achieving, or winning more games than their point differential would indicate. Conversely, a negaitve <strong>Win Differential</strong> indicates they are underperforming, and losing more games than would be expected.</p>

                    </Modal.Description>
                </Modal.Content>
            </Modal>)
    }

    generateStudAndDud(position) {

        let week = this.state.selectedWeek || 0
        let sad = this.props.sad
        let stud, dud, byAirPoints, byGroundPoints, combinedDudName, dudModifier, recTdPercent, recYardsPercent, recPercent, combinedStudName, differentiatorDescription, rushYardsPercent, rushTdPercent, passTdPercent, passYardsPercent;
        const orderedPlayers = this.props.fantasyRankings
        if (!orderedPlayers || !sad) {
            return null
        }
        if (week <= 0 || week === 20) {
            return null
        }



        if (!position) {
            return (
                <Segment>

                    <Header as='h2'>
                        Nothing to see
                    <Header.Subheader>
                            Fantasy Stud and Dud not completed for this position group. It will be added soon.
                            </Header.Subheader>
                    </Header>
                </Segment>
            )
        }
        // let image = require(`./../../assets/logos/notfound.jpg`)

        switch (position) {



            case 'QB':



                stud = sad[week].qbStud || ''
                combinedStudName = stud.firstName + ' ' + stud.lastName
                dud = sad[week].qbDud
                combinedDudName = dud.firstName + ' ' + dud.lastName
                rushTdPercent = (stud.rushTDs * 6 / stud.fantasyPoints) * 100 || 0
                rushYardsPercent = (stud.rushYds * .1 / stud.fantasyPoints) * 100 || 0
                passYardsPercent = (stud.passYds * .04 / stud.fantasyPoints) * 100 || 0
                passTdPercent = (stud.passTDs * 4 / stud.fantasyPoints) * 100 || 0
                let intPercent = ((dud.passInts * 2 + dud.fantasyPoints) / dud.fantasyPoints) * 100 || 0

                //STUD BY ARM OR BY FOOT
                byAirPoints = stud.passYds * .04 + stud.passTDs * 4 - stud.passInts || 0
                byGroundPoints = stud.rushYds * .1 + stud.rushTDs * 6 - stud.rushFum || 0

                if (byAirPoints > byGroundPoints) {
                    let plural = stud.passTDs > 1 ? 's' : ''
                    if (passYardsPercent > passTdPercent) {
                        differentiatorDescription = ` ${stud.lastName} chucked the ball all around the yard, racking up ${stud.passYds} passing yards. ${passYardsPercent.toFixed(1)} percent of ${stud.firstName}'s fantasy points game from aerial yardage.`

                    }
                    else {
                        differentiatorDescription = ` ${stud.lastName} found the endzone with regularity this week, as he threw for ${stud.passTDs} touchdown${plural}. That was good for ${passTdPercent.toFixed(1)} percent of ${stud.firstName}'s fantasy production this week.`


                    }
                } else {
                    if (rushYardsPercent > rushTdPercent) {

                        differentiatorDescription = ` ${stud.lastName} made huge plays with his legs, incredibly scoring more points from his ${stud.rushYds} rush yards than anything else. 
                    ${rushYardsPercent.toFixed(1)} percent of ${stud.firstName}'s fantasy points came from scrambles and designed runs.`
                    } else {
                        let plural = stud.rushTDs > 1 ? 's' : ''

                        differentiatorDescription = ` ${stud.lastName} decided to do it himself, tucking the ball down and making his way across the endzone for ${stud.rushTDs} rushing touchdown${plural}.
                        ${stud.firstName} earned ${rushTdPercent.toFixed(1)} percent of his fantasy points by stretching the ball across the goalline.`
                    }
                }

                dudModifier = dud.fantasyPoints < 12 ? ' He played incredibly poorly' : " Althought the output is not what we expect, it could have been much worse"
                dudModifier = dud.fantasyPoints < 6 ? " He was an unmitigated disaster and played as poor of a game as you'll see" : dudModifier

                let intModifier = intPercent > 250 ? `${dud.firstName}'s ${dud.passInts} interceptions are what truly ruined his day.` : ''

                return (
                    <Segment>
                        <Grid>
                            <Grid.Row columns={2}>
                                <Grid.Column>
                                    <Header as='h2'>
                                        Stud of the Week
                    <Header.Subheader>
                                            {combinedStudName}
                                        </Header.Subheader>
                                    </Header>
                                    {stud._id === undefined &&
                                        <p>There were no Stud Quarterbacks this week. </p>

                                    }
                                    {stud._id &&

                                        <p>{combinedStudName} is the week's stud Quarterback. He exceeded his weekly average fantasy output of {(stud.fantasyPoints - Math.abs(stud.diff)).toFixed(1)} by {stud.diff.toFixed(1)} points.{differentiatorDescription}</p>
                                    }

                                </Grid.Column>
                                <Grid.Column>
                                    <Header as='h2'>
                                        Dud of the Week
            <Header.Subheader>
                                            {combinedDudName}
                                        </Header.Subheader>
                                    </Header>
                                    {dud._id === undefined &&
                                        <p>There was no dud quarterback this week. </p>

                                    }
                                    {dud._id &&

                                        <p>{combinedDudName} is the dud Quarterback of week {week + 1}. {dudModifier}. {intModifier} His output of {dud.fantasyPoints.toFixed(1) || 0} points was well below his typical {(dud.fantasyPoints + Math.abs(dud.diff)).toFixed(1) || 0} point performance, falling short by {dud.diff.toFixed(1) || 0} points.</p>
                                    }
                                </Grid.Column>

                            </Grid.Row>
                        </Grid>
                    </Segment>
                )


            case 'HB':
                stud = sad[week].hbStud
                combinedStudName = stud.firstName + ' ' + stud.lastName
                dud = sad[week].hbDud
                combinedDudName = dud.firstName + ' ' + dud.lastName
                rushTdPercent = (stud.rushTDs * 6 / stud.fantasyPoints) * 100
                rushYardsPercent = (stud.rushYds * .1 / stud.fantasyPoints) * 100
                recYardsPercent = (stud.recYds * .1 / stud.fantasyPoints) * 100
                recPercent = (stud.recCatches * 1 / stud.fantasyPoints) * 100
                recTdPercent = (stud.recTDs * 6 / stud.fantasyPoints) * 100

                byAirPoints = stud.recTDs + stud.recYds + stud.recCatches || 0
                byGroundPoints = stud.rushTDs + stud.rushYds || 0

                // if player had more rushing points
                if (byGroundPoints > byAirPoints) {
                    if (rushTdPercent > rushYardsPercent) {
                        let plural = stud.rushTDs > 1 ? 's' : ''
                        differentiatorDescription = ` Again and again ${stud.lastName} found his way into the endzone, to the tune of ${stud.rushTDs} rushing touchdown${plural}. At 6 points a pop, they add up fast and accounted for ${rushTdPercent.toFixed(1)} percent of ${stud.firstName}'s fantasy points.`



                    } else {
                        differentiatorDescription = ` ${stud.firstName} was called upon to grind out the yardage in week ${week + 1}. He was unstoppable and ended the game with ${stud.rushYds} rushing yards, or ${rushYardsPercent.toFixed(1)} percent of ${stud.firstName}'s fantasy output.`

                    }
                }
                // else player had more receiving points 
                else {


                    if (recTdPercent > recYardsPercent && recTdPercent > recPercent) {
                        let plural = stud.recTDs > 1 ? 's' : ''
                        differentiatorDescription = ` Perhaps an unconventional day for ${stud.firstName}, but productive nonetheless. He found the endzone ${stud.recTDs} time${plural}, but through the air. Receiving touchdowns count the same as rushing ones though, and these counted for  ${recTdPercent.toFixed(1)} percent of ${stud.lastName}'s points.`

                    }
                    else if (recYardsPercent > recTdPercent && recYardsPercent > recPercent) {
                        differentiatorDescription = ` ${stud.lastName} made the most of his targets this week, using the open space to pile up ${stud.recYds} yards receiving.
                         ${recYardsPercent.toFixed(1)} percent of his fantasy points came from yardage when the quarterback looked his way.`

                    }
                    else {
                        differentiatorDescription = ` Who says only wide receivers can be target monsters? ${stud.lastName} was found early and often in the game, and snagged ${stud.recCatches} balls, which accounted for ${recPercent.toFixed(1)} percent of his fantasy production for the day.`

                    }
                }
                dudModifier = dud.fantasyPoints < 10 ? 'He was a huge letdown this week' : "He didn't live up to his usual output but still put in a respectable effort"

                return (
                    <Segment>
                        <Grid>
                            <Grid.Row columns={2}>
                                <Grid.Column>
                                    <Header as='h2'>
                                        Stud of the Week
            <Header.Subheader>
                                            {combinedStudName}
                                        </Header.Subheader>
                                    </Header>

                                    <p>{combinedStudName} is the stud Halfback of week {week + 1}. He blew past his usual {(stud.fantasyPoints - stud.diff).toFixed(1)} point output, and exceeded that by {stud.diff.toFixed(1)} points this week.{differentiatorDescription}</p>


                                </Grid.Column>
                                <Grid.Column>
                                    <Header as='h2'>
                                        Dud of the Week
    <Header.Subheader>
                                            {combinedDudName}
                                        </Header.Subheader>
                                    </Header>

                                    <p>You can't be a great every week, and this week {combinedDudName} is the dud at Halfback. {dudModifier}, scoring {dud.fantasyPoints.toFixed(1)} points, which is {dud.diff.toFixed(1)} points fewer than his average output of {(Math.abs(dud.diff) + dud.fantasyPoints).toFixed(1)}.</p>

                                </Grid.Column>

                            </Grid.Row>
                        </Grid>
                    </Segment>
                )
            case 'WR':
                stud = sad[week].wrStud
                combinedStudName = stud.firstName + ' ' + stud.lastName
                dud = sad[week].wrDud
                combinedDudName = dud.firstName + ' ' + dud.lastName
                recTdPercent = (stud.recTDs * 6 / stud.fantasyPoints) * 100
                recYardsPercent = (stud.recYds * .1 / stud.fantasyPoints) * 100
                recPercent = (stud.recCatches * 1 / stud.fantasyPoints) * 100



                if (recTdPercent > recYardsPercent && recTdPercent > recPercent) {
                    let plural = stud.recTDs > 1 ? 's' : ''
                    differentiatorDescription = ` This is largely thanks to ${stud.lastName}'s touchdown production. ${stud.firstName} scored ${stud.recTDs} touchdown${plural}, accounting for ${recTdPercent.toFixed(1)} percent of his fantasy points.`

                }
                else if (recYardsPercent > recTdPercent && recYardsPercent > recPercent) {
                    differentiatorDescription = ` This is mostly due to ${stud.lastName} accounting for a huge amount of yardage. ${stud.firstName} rolled to a ${stud.recYds} yard day, which accounted for ${recYardsPercent.toFixed(1)} percent of his fantasy points.`

                }
                else {
                    differentiatorDescription = ` ${stud.lastName} was a target monster this week and that's the main reason why he is the fantasy stud. ${stud.firstName} caught  ${stud.recCatches} passes, which accounted for ${recPercent.toFixed(1)} percent of his fantasy points.`

                }


                return (
                    <Segment>
                        <Grid>
                            <Grid.Row columns={2}>
                                <Grid.Column>
                                    <Header as='h2'>
                                        Stud of the Week
            <Header.Subheader>
                                            {combinedStudName}
                                        </Header.Subheader>
                                    </Header>

                                    <p>{combinedStudName} is the Stud Wide Receiver of week {week + 1}.{stud.firstName} put on a show this week, exceeding his weekly average fantasy output by {stud.diff.toFixed(1)} points this week.{differentiatorDescription}</p>


                                </Grid.Column>
                                <Grid.Column>
                                    <Header as='h2'>
                                        Dud of the Week
    <Header.Subheader>
                                            {combinedDudName}
                                        </Header.Subheader>
                                    </Header>
                                    {dud._id === undefined &&
                                        <p>No Stud </p>

                                    }
                                    {dud._id &&
                                        <p>{combinedDudName} is the biggest disappointment at the Wide Receiver position this week. {dud.lastName} only managed to score {dud.fantasyPoints.toFixed(1)} points. This fell well short of the {(dud.fantasyPoints + Math.abs(dud.diff)).toFixed(1)} points he scores in an average week.</p>
                                    }

                                </Grid.Column>

                            </Grid.Row>
                        </Grid>
                    </Segment >
                )

            case 'TE':
                stud = sad[week].teStud
                combinedStudName = stud.firstName + ' ' + stud.lastName

                dud = sad[week].teDud
                combinedDudName = dud.firstName + ' ' + dud.lastName
                recTdPercent = (stud.recTDs * 6 / stud.fantasyPoints) * 100
                recYardsPercent = (stud.recYds * .1 / stud.fantasyPoints) * 100
                recPercent = (stud.recCatches * 1 / stud.fantasyPoints) * 100

                if (recTdPercent > recYardsPercent && recTdPercent > recPercent) {
                    let plural = stud.recTDs > 1 ? 's' : ''
                    differentiatorDescription = ` This is largely thanks to ${stud.lastName}'s touchdown production. ${stud.firstName} scored ${stud.recTDs} touchdown${plural}, accounting for ${recTdPercent.toFixed(1)} percent of his fantasy points.`

                }
                else if (recYardsPercent > recTdPercent && recYardsPercent > recPercent) {
                    differentiatorDescription = ` This is mostly due to ${stud.lastName} accounting for a huge amount of yardage. ${stud.firstName} rolled to a ${stud.recYds} yard day, accounting for ${recYardsPercent.toFixed(1)} percent of his fantasy points.`

                }
                else {
                    differentiatorDescription = ` ${stud.lastName} was a target monster this week and that's the main reason why he is the fantasy stud. ${stud.firstName} caught  ${stud.recCatches} passes, which accounted for ${recPercent.toFixed(1)} percent of his fantasy points.`

                }

                dudModifier = dud.fantasyPoints < 8 ? " Even with lower expectations, this was especially poor." : " All things considered, this isn't a terrible performace."

                return (
                    <Segment>
                        <Grid>
                            <Grid.Row columns={2}>
                                <Grid.Column>
                                    <Header as='h2'>
                                        Stud of the Week
            <Header.Subheader>
                                            {combinedStudName}
                                        </Header.Subheader>
                                    </Header>

                                    <p>{combinedStudName} is the stud Tight End of week {week + 1}.  {sad[week].teStud.lastName}'s {sad[week].teStud.fantasyPoints.toFixed(1)} points exceeded his weekly average fantasy output by {sad[week].teStud.diff.toFixed(1)} points.{differentiatorDescription}</p>


                                </Grid.Column>
                                <Grid.Column>
                                    <Header as='h2'>
                                        Dud of the Week
    <Header.Subheader>
                                            {combinedDudName}
                                        </Header.Subheader>
                                    </Header>

                                    <p>Tight End is a volatile position for all except the elite players so we can't hold {combinedDudName}'s poor performance against him, but he is this week's dud.
                                     {dudModifier} {dud.lastName} scored {dud.fantasyPoints.toFixed(1)} points, falling {dud.diff.toFixed(1)} points short of his average fantasy production.</p>

                                </Grid.Column>

                            </Grid.Row>
                        </Grid>
                    </Segment>
                )

            default:
                break;
        }

    }
    handleChange = (e, { value }) => {
        this.setState({ selectedWeek: value })

    }
    render() {
        const options = [
            { key: 0, text: 'Week One', value: 0 },
            { key: 1, text: 'Week Two', value: 1 },
            { key: 2, text: 'Week Three', value: 2 },
            { key: 3, text: 'Week Four', value: 3 },
            { key: 4, text: 'Week Five', value: 4 },
            { key: 5, text: 'Week Six', value: 5 },
            { key: 6, text: 'Week Seven', value: 6 },
            { key: 7, text: 'Week Eight', value: 7 },
            { key: 8, text: 'Week Nine', value: 8 },
            { key: 9, text: 'Week Ten', value: 9 },
            { key: 10, text: 'Week Eleven', value: 10 },
            { key: 11, text: 'Week Twelve', value: 11 },
            { key: 12, text: 'Week Thirteen', value: 12 },
            { key: 13, text: 'Week Fourteen', value: 13 },
            { key: 14, text: 'Week Fifteen', value: 14 },
            { key: 15, text: 'Week Sixteen', value: 15 },
            { key: 16, text: 'Week Seventeen', value: 16 },
            { key: 17, text: 'Wildcard', value: 17 },
            { key: 18, text: 'Divisional', value: 18 },
            { key: 19, text: 'Conference', value: 19 },
            { key: 21, text: 'Super Bowl', value: 21 },
        ]
        const value = this.state.selectedWeek
        if (this.props.fantasyRankings) {

        }
        return (
            <Container>
                <LeagueDashboard />
                <Segment>
                    <HeaderContainer>
                        <Header
                            as="h1"
                            content="Fantasy Test"
                        />
                        <Dropdown
                            onChange={this.handleChange}
                            options={options}
                            placeholder='Choose an option'
                            selection
                            value={value}
                        />
                        {/* {this.renderModal()} */}

                    </HeaderContainer>

                </Segment>
                {this.renderLoader()}
                {this.props.fantasyRankings && this.state.selectedWeek >= this.props.fantasyRankings.length &&
                    <Header as='h1'>No Stats Available for Week {this.state.selectedWeek + 1}. Please select an earlier week</Header>
                }

                {this.props.fantasyRankings && this.state.selectedWeek < this.props.fantasyRankings.length &&
                    <section>
                        <Header as='h2'>Quarterback Ranks</Header>




                        {this.generateStudAndDud('QB')}


                        <Table celled unstackable singleline="true" compact>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell textAlign="center">#</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Player</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Fantasy Points</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Pass Yds
                                     </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Pass TDs
                                      </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Int
                                    </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rush Yds
                                           </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rush TD
                                   </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Fumbles
                                   </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        # Top 10
                                   </Table.HeaderCell>

                                </Table.Row>
                            </Table.Header>

                            <Table.Body>{this.renderQbRanks()}


                            </Table.Body>
                        </Table>

                        


                        <Header as='h2'>Halfback Ranks</Header>
                        {this.generateStudAndDud('HB')}


                        <Table celled unstackable singleline="true" compact>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell textAlign="center">#</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Player</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Fantasy Points</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rush Yds
                                 </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rush TDs
                                  </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Fumbles
                                </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rec Yds
                                       </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rec TD
                               </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Catches
                               </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        # Top 10
                                   </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>

                            <Table.Body>{this.renderHbRanks()}


                            </Table.Body>
                        </Table>
                        
                        <Header as='h2'>Wide Receiver Ranks</Header>
                        {this.generateStudAndDud('WR')}



                        <Table celled unstackable singleline="true" compact>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell textAlign="center">#</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Player</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Fantasy Points</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rec Yds
                                       </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rec TD
                               </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Catches
                               </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rush Yds
                                 </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rush TDs
                                  </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Fumbles
                                </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        # Top 10
                                   </Table.HeaderCell>

                                </Table.Row>
                            </Table.Header>

                            <Table.Body>{this.renderReceiverRanks('WR')}


                            </Table.Body>
                        </Table>

                        

                        <Header as='h2'>Tight End Ranks</Header>
                        {this.generateStudAndDud('TE')}

                        <Table celled unstackable singleline="true" compact>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell textAlign="center">#</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Player</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Fantasy Points</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rec Yds
                                       </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rec TD
                               </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Catches
                               </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rush Yds
                                 </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Rush TDs
                                  </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Fumbles
                                </Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        # Top 10
                                   </Table.HeaderCell>

                                </Table.Row>
                            </Table.Header>

                            <Table.Body>{this.renderReceiverRanks('TE')}


                            </Table.Body>
                        </Table>

                        


                    </section>
                }
            </Container>

        )
    }
}

const mapStateToProps = state => ({
    teams: state.leagueReducer.teams,
    league: state.leagueReducer.league,
    metaData: state.contentReducer.fantasyMetaData,
    sad: state.contentReducer.fantasySAD,
    fantasyRankings: state.contentReducer.fantasyRankings,
    passingData: state.weeklyDataReducer.passingData,
    rushingData: state.weeklyDataReducer.rushingData,
    receivingData: state.weeklyDataReducer.receivingData,
    players: state.rosterReducer.skillPositions
})
export default connect(mapStateToProps, actions)(Schedule)


const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`