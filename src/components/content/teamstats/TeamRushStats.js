import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Container,
	Header,
	Segment,
	Table, Image
} from 'semantic-ui-react'
import { connect } from 'react-redux'

import * as actions from './../../../redux/actions/actions'
import LeagueDashboard from '../../league/LeagueDashboard';
import Loader from './../../general/Loader'

import AutoAd from './../../general/ads/AutoAd'

class RushStats extends Component {
	constructor(props) {
		super(props)

		this.state = {
			selectedWeek: undefined,
			rushStats: [],
			column: null,
			direction: null,
			filter: {
				teams: [],
				positions: [],
				minAttempts: 10
			}
		}
	}

	componentWillMount() {
		if (this.props.rushStats) {
			this.setState({ rushStats: this.props.rushStats })
		}
	}

	componentWillReceiveProps(newProps) {
		if (newProps.rushStats) {
			this.setState({ rushStats: newProps.rushStats })
		}
	}

	renderStats() {
		let stats = this.props.rushStats
		let index = 0

		return stats.map((team) => {
		index++
			let image;
			try {
				image = require(`./../../../assets/logos/${team.name.toLowerCase()}.svg`)

			} catch (e) {
				image = require(`./../../../assets/logos/notfound.jpg`)

			}
			
			return (
				<Table.Row key={team._id + 'rushStats'}>
					<Table.Cell textAlign="center">{index } </Table.Cell>
					<Table.Cell ><Image style={{ borderRadius: '0' }} src={image} avatar /> {team.name}</Table.Cell>
					<Table.Cell textAlign="center">
						{team.rushAtt}
					</Table.Cell>
					<Table.Cell textAlign="center">{team.rushYds || 0}</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.ypc).toFixed(2)}
					</Table.Cell>

					<Table.Cell textAlign="center">
						{team.rushTDs}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{team.ybc.toFixed(2)}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{team.yac.toFixed(2)} ({team.rushYdsAfterContact})
					</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.btRate * 100).toFixed(2)}% ({team.rushBrokenTackles})
					</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.bpRate * 100).toFixed(2)}% ({team.rush20PlusYds})
					</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.fumbleRate * 100).toFixed(2)}% ({team.rushFum})
					</Table.Cell>

					<Table.Cell textAlign="center">
						{(team.tdRate * 100).toFixed(2)}%
					</Table.Cell>
				</Table.Row>
			)
		})
	}


	handleSort = clickedColumn => () => {
		const { column, rushStats, direction } = this.state

		if (column !== clickedColumn) {

			rushStats.sort((a, b) => {
				return b[clickedColumn] - a[clickedColumn]
			})

			this.setState({
				column: clickedColumn,
				rushStats: rushStats,
				direction: 'ascending',
			})

			return
		}
		if (direction === 'descending') {

			rushStats.sort((a, b) => {
				return b[clickedColumn] - a[clickedColumn]
			})

		} else if (direction === 'ascending') {
			rushStats.sort((a, b) => {
				return a[clickedColumn] - b[clickedColumn]
			})
		}

		this.setState({
			rushStats: rushStats,
			direction: direction === 'ascending' ? 'descending' : 'ascending',
		})


	}
	handleChange = (e, { value }) => {
		this.setState({ selectedWeek: value })
	}
	render() {
	
		const { column, direction } = this.state

		return (
			<Container>
				<LeagueDashboard />
				<Segment>
					<HeaderContainer>
						<Header
							as="h1"
							content="Team Rushing Statistics"
						/>
						{/* <Filter minDescription='Rush Attempts' /> */}

					</HeaderContainer>

				</Segment>
				{(this.props.rushStats === undefined) &&
					<Loader loaderMessage="Generating Rushing Stats" />}


				{this.props.rushStats &&

					<Container style={{ overflowX: 'scroll' }}>
						<Table celled sortable unstackable singleline="true" compact>
							<Table.Header >
								<Table.Row>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center">#</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Player</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'rushAtt' ? direction : null} onClick={this.handleSort('rushAtt')}>
										Rush Att.
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'rushYds' ? direction : null} onClick={this.handleSort('rushYds')} > Yards</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'ypc' ? direction : null} onClick={this.handleSort('ypc')} >YPC</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'rushTDs' ? direction : null} onClick={this.handleSort('rushTDs')} >
										TD
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'ybc' ? direction : null} onClick={this.handleSort('ybc')} >
										Avg. Yards Before Contact
		</Table.HeaderCell><Table.HeaderCell textAlign="center" sorted={column === 'yac' ? direction : null} onClick={this.handleSort('yac')} >
										Avg. Yards after Contact
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'btRate' ? direction : null} onClick={this.handleSort('btRate')} >Broken Tackle Rate</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'bpRate' ? direction : null} onClick={this.handleSort('bpRate')} >20 Yard Rush Rate</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'fumbleRate' ? direction : null} onClick={this.handleSort('fumbleRate')} >Fumble Rate</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'tdRate' ? direction : null} onClick={this.handleSort('tdRate')} >TD Rate</Table.HeaderCell>
								</Table.Row>
							</Table.Header>

							<Table.Body>{this.renderStats()}</Table.Body>
						</Table>
						

					</Container>


				}

			</Container>

		)
	}
}

const mapStateToProps = state => ({
	league: state.leagueReducer.league,
	rushStats: state.contentReducer.teamRushStats,
	filter: state.filterReducer.filter,


})
export default connect(mapStateToProps, actions)(RushStats)


const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`