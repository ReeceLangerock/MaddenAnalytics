import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Container,
	Header,
	Segment,
	Table, Image
} from 'semantic-ui-react'
import { connect } from 'react-redux'

import * as actions from './../../../redux/actions/actions'
import LeagueDashboard from '../../league/LeagueDashboard';
import Loader from './../../general/Loader'

import AutoAd from './../../general/ads/AutoAd'

class TeamPassingStats extends Component {
	constructor(props) {
		super(props)

		this.state = {
			selectedWeek: undefined,
			passStats: [],
			column: null,
			direction: null,
			sum: {

			}

		}
	}
	componentWillMount() {
		if (this.props.passStats) {
			this.setState({ passStats: this.props.passStats })
		}
	}
	
	
	componentWillReceiveProps(newProps) {
		if (newProps.passStats) {
			this.setState({ passStats: newProps.passStats })
		}
	}

	renderStats() {
		let stats = this.props.passStats
		const sum = this.state.sum
		let index = 0
		sum.passAtt = 0

		return stats.map((team) => {
		
			sum.passAtt += team.passAtt
			index++

			let image;
			try {
				image = require(`./../../../assets/logos/${team.name.toLowerCase()}.svg`)

			} catch (e) {
				image = require(`./../../../assets/logos/notfound.jpg`)

			}
			team.teamStats.adjCompPercentage = team.teamStats.adjCompPercentage > 1 ? 1 : team.teamStats.adjCompPercentage
			return (
				<Table.Row key={team.teamStats._id + 'passStats' + index} >
					<Table.Cell textAlign="center">{index} </Table.Cell>
					<Table.Cell ><Image style={{ borderRadius: '0' }} src={image} avatar />   {team.name}</Table.Cell>
					<Table.Cell textAlign="center">
						{team.teamStats.passComp}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{team.teamStats.passAtt}
					</Table.Cell>
				<Table.Cell textAlign="center">{team.teamStats.passYds || 0}</Table.Cell>
				<Table.Cell textAlign="center">{(team.teamStats.yacPercent * 100).toFixed(3) || 0}</Table.Cell>
					<Table.Cell textAlign="center">{(team.teamStats.compPercentage * 100).toFixed(2) || 0}</Table.Cell>
					<Table.Cell textAlign="center">{(team.teamStats.adjCompPercentage * 100).toFixed(2) || 0}</Table.Cell>
					<Table.Cell textAlign="center">{(team.teamStats.receiverDrops).toFixed(1) || 0}</Table.Cell>
				<Table.Cell textAlign="center">
						{(team.teamStats.ypa).toFixed(2)}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.teamStats.catchDepth).toFixed(2)}
					</Table.Cell>




					<Table.Cell textAlign="center">
						{team.teamStats.passTDs}
					</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.teamStats.intRate * 100).toFixed(2)}% ({team.teamStats.passInts})
					</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.teamStats.sackRate * 100).toFixed(2)}% ({team.teamStats.passSacks})

					</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.teamStats.tdRate * 100).toFixed(2)}%
						</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.teamStats.qbr).toFixed(2)}

					</Table.Cell>
					<Table.Cell textAlign="center">
						{(team.teamStats.ncaa).toFixed(2)}

					</Table.Cell>
				</Table.Row>
			)
		})
	}


	handleSort = clickedColumn => () => {
		const { column, passStats, direction } = this.state

		if (column !== clickedColumn) {

			passStats.sort((a, b) => {
				return b.teamStats[clickedColumn] - a.teamStats[clickedColumn]
			})

			this.setState({
				column: clickedColumn,
				passStats: passStats,
				direction: 'ascending',
			})

			return
		}
		if (direction === 'descending') {

			passStats.sort((a, b) => {
				return b.teamStats[clickedColumn] - a.teamStats[clickedColumn]
			})

		} else if (direction === 'ascending') {
			passStats.sort((a, b) => {
				return a.teamStats[clickedColumn] - b.teamStats[clickedColumn]
			})
		}

		this.setState({
			passStats, 
			direction: direction === 'ascending' ? 'descending' : 'ascending',
		})


	}
	handleChange = (e, { value }) => {
		this.setState({ selectedWeek: value })
	}
	render() {

		const { column, direction} = this.state
		
		return (
			<Container>
				<LeagueDashboard />
				<Segment>
					<HeaderContainer>
						<Header
							as="h1"
							content="Team Passing Statistics"
						/>
						{/* <Filter minDescription='Pass Attempts' /> */}

					</HeaderContainer>

				</Segment>
				{(this.props.passStats === undefined) &&
					<Loader loaderMessage="Generating Passing Stats" />}

				{this.props.passStats &&

					<Container style={{ overflowX: 'scroll' }}>
						<Table celled sortable unstackable singleline="true" compact>
							<Table.Header >
								<Table.Row>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center">#</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Team</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'passComp' ? direction : null} onClick={this.handleSort('passComp')}>
										Pass Comp.
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'passAtt' ? direction : null} onClick={this.handleSort('passAtt')}>
										Pass Att.
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'passYds' ? direction : null} onClick={this.handleSort('passYds')} > Yards</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'yacPercent' ? direction : null} onClick={this.handleSort('yacPercent')} > YAC %</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'compPercentage' ? direction : null} onClick={this.handleSort('compPercentage')} >Completion %</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'adjCompPercentage' ? direction : null} onClick={this.handleSort('adjCompPercentage')} > Adj. Completion %</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'receiverDrops' ? direction : null} onClick={this.handleSort('receiverDrops')} >Receiver Drops</Table.HeaderCell>

									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'ypa' ? direction : null} onClick={this.handleSort('ypa')} >YPA</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'catchDepth' ? direction : null} onClick={this.handleSort('catchDepth')} >Depth of Catch</Table.HeaderCell>
									
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'passTDs' ? direction : null} onClick={this.handleSort('passTDs')} >
										TD
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'intRate' ? direction : null} onClick={this.handleSort('intRate')} >
										Int Rate
		</Table.HeaderCell><Table.HeaderCell textAlign="center" sorted={column === 'sackRate' ? direction : null} onClick={this.handleSort('sackRate')} >
										Sack Rate
		</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'tdRate' ? direction : null} onClick={this.handleSort('tdRate')} >TD Rate</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'qbr' ? direction : null} onClick={this.handleSort('qbr')} >Passer Rating</Table.HeaderCell>
									<Table.HeaderCell style={{ whiteSpace: 'normal' }} textAlign="center" sorted={column === 'ncaa' ? direction : null} onClick={this.handleSort('ncaa')} >NCAA Passer Rating</Table.HeaderCell>

								</Table.Row>
							</Table.Header>

							<Table.Body>{this.renderStats()}
		
							</Table.Body>
						</Table>


					</Container>
					


				}

			</Container>

		)
	}
}

const mapStateToProps = state => ({
	league: state.leagueReducer.league,
	passStats: state.contentReducer.teamPassStats,
	filter: state.filterReducer.filter,


})
export default connect(mapStateToProps, actions)(TeamPassingStats)


const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`