import React from 'react';
import styled from 'styled-components'
import { connect } from 'react-redux'

import {
	Container,
	Button,
	Modal,
	Dimmer, Loader,
	Header,
	Segment,
	Icon,
	Table
} from 'semantic-ui-react'
import LeagueDashboard from '../league/LeagueDashboard';

import * as actions from './../../redux/actions/actions'

const entries = {

}

class DraftKings extends React.Component {
    constructor(props){
        super(props)
        this.renderLineups = this.renderLineups.bind(this)
        this.calculate = this.calculate.bind(this)
        this.state= {
            dataReady: false,
            leaderboard: [],
            lineups: []
        }
    }

    calculate(){
        const {dkData, fantasyAggregation} = this.props
        if(!dkData || !fantasyAggregation){
            return null
        }
        let data = []
        
        Object.keys(dkData).map((lineup)=> {
            let sum = 0;
            if(dkData[lineup].code.includes('fun')){
                return null
            }
            const l = dkData[lineup]


            l.weeklyTotals = [0,0,0,0, 0,0,0,0, 0,0,0,0 ,0,0,0,0, 0]

            Object.keys(l.roster).map((position, index)=>{
                let weekTotal = 0;

                const pd = l.roster[position]
                for (let i =1; i <= 16; i++){
                    try {
                       
                    let posTotal = fantasyAggregation[pd._id].fpArray[i] !== undefined ? fantasyAggregation[pd._id].fpArray[i] : 0
                        l.weeklyTotals[i] = l.weeklyTotals[i] + posTotal
                        if(l.code === 'izezoh'){
                        }
                    } catch(e){
                    }
                    if(l.weeklyTotals[i] && l.weeklyTotals[i] > 0){

                    }
                }
               

               
            })
            l.sum = l.weeklyTotals.reduce((accumulator, currentValue) => {
                if(currentValue !== undefined && accumulator !== undefined){
                   return accumulator + currentValue
                }
            }
            ,0)
            data.push((l))
        })

       data = data.sort((a,b) =>{
           return b.sum - a.sum
       })
        this.setState({
            leaderboard: data
        })
    }

    componentDidMount(){

    }

    renderLeaderboard(){


        if(this.state.leaderboard.length > 0){
            return(		<Table celled unstackable singleline="true" compact>
            <Table.Header>
                          <Table.Row>
                              <Table.HeaderCell textAlign="center">Team</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk2</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk3</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk4</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk5</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk6</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk7</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk8</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk9</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk10</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk11</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk12</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk13</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk14</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk15</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk16</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Wk17</Table.HeaderCell>
                              <Table.HeaderCell textAlign="center">Total</Table.HeaderCell>
                          
                          </Table.Row>
                      </Table.Header>
                      <Table.Body>
                          {this.state.leaderboard.map((entry)=>{
                              return (<Table.Row>
                                  <Table.Cell>{entry.team}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[1].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[2].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[3].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[4].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[5].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[6].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[7].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[8].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[9].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[10].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[11].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[12].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[13].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[14].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[15].toFixed(1)}</Table.Cell>
                                  <Table.Cell>{entry.weeklyTotals[16].toFixed(1)}</Table.Cell>

                                  <Table.Cell>{entry.sum.toFixed(1)}</Table.Cell>
                                  </Table.Row>)
                          })}
                          </Table.Body>
                          </Table>)
        }

    }

    renderLineups(){
        const {dkData, fantasyAggregation} = this.props
        if(dkData && fantasyAggregation){
        const tableData =  Object.keys(dkData).map((lineup) => {
            if(dkData[lineup].code.includes('fun')){
                return null
            }
            var fullSum = 0
            const weeklyTotals = []
            const l = dkData[lineup]
            
            return (  <Lineup key = {l.code}>
                <h1>{l.team}</h1>
						<Table celled unstackable singleline="true" compact>
                  <Table.Header>
								<Table.Row>
									<Table.HeaderCell textAlign="center">Pos.</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Player</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Team</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk2</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk3</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk4</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk5</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk6</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk7</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk8</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk9</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk10</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk11</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk12</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk13</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk14</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk15</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk16</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Wk17</Table.HeaderCell>
									<Table.HeaderCell textAlign="center">Total</Table.HeaderCell>
								
								</Table.Row>
							</Table.Header>
							<Table.Body>{Object.keys(l.roster).map((position)=>{
                                const pd = l.roster[position]
                                var fp =[]
                                try{
                                    fp = fantasyAggregation[pd._id].fpArray
                                } catch(e){}
                                for(let i=0; i <= 16; i++)
{
    fp[i] = fp[i]? fp[i] : undefined
    
}                               var posSum = 0;
                                return (
				<Table.Row key={l.code + position}>
                
					<Table.Cell textAlign="center">{position}</Table.Cell>
					<Table.Cell textAlign="center">{pd.firstName} {pd.lastName}</Table.Cell>
                   
					<Table.Cell textAlign="center">{pd.teamName}</Table.Cell>
                    {fp.map((points,index)=>{
                        if(index === 0){
                            return null;
                        }
                        if(points !== undefined){
                            
                            weeklyTotals[index] = weeklyTotals[index] ? weeklyTotals[index]+ points : points;
                            posSum += points
                            
                            return (
					<Table.Cell key ={pd._id +index} textAlign="center">{points.toFixed(1) ||  0}</Table.Cell>
                            )
                        } else {
					return <Table.Cell key = {pd._id +index} textAlign="center">-</Table.Cell>
                            
                        }
                    })
                    
                    }
                    
					<Table.Cell key ={pd._id +'sum'} textAlign="center">{posSum.toFixed(1) ||  0}</Table.Cell>

                </Table.Row>)
                  
                            })}

          
                              <Table.Row>
					<Table.Cell textAlign="center">Total</Table.Cell>
					<Table.Cell textAlign="center"></Table.Cell>
					<Table.Cell textAlign="center"></Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[1].toFixed(1)}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[2] ? weeklyTotals[2].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[3] ? weeklyTotals[3].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[4] ? weeklyTotals[4].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[5] ? weeklyTotals[5].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[6] ? weeklyTotals[6].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[7] ? weeklyTotals[7].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[8] ? weeklyTotals[8].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[9] ? weeklyTotals[9].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[10] ? weeklyTotals[10].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[11] ? weeklyTotals[11].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[12] ? weeklyTotals[12].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[13] ? weeklyTotals[13].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[14] ? weeklyTotals[14].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[15] ? weeklyTotals[15].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">{weeklyTotals[16] ? weeklyTotals[16].toFixed(1) : '-'}</Table.Cell>
					<Table.Cell textAlign="center">
                    {weeklyTotals.reduce((accumulator, currentValue) => {
                if(currentValue !== undefined && accumulator !== undefined){
                   return accumulator + currentValue
                }
            }
            ,0).toFixed(1)}
                    </Table.Cell>
		

                    </Table.Row>
                            </Table.Body>

                            </Table>

                </Lineup>)
        })

      
        return tableData
    }
    }

    componentDidUpdate(prevProps){

        if(this.state.leaderboard.length === 0){

            this.calculate()
        }
        
    }


	renderLoader() {
		if (this.state.leaderboard.length === 0) {




			return (<Segment key='loader' style={{ height: '300px' }}>

				<Dimmer active style={{ height: '300px' }}>
					<Loader size='massive' content='Generating Rankings' />
				</Dimmer>
			</Segment>)

		}

	}
    
    render() {

        

        return (
            <Container>
            <LeagueDashboard />
            <Segment>
                <HeaderContainer>
                    <Header
                        as="h1"
                        content = "Draft Kings"
                    />


                </HeaderContainer>

            </Segment>
            {this.renderLoader()}


                <Container>
                    {this.renderLeaderboard()}
                {this.renderLineups()}
                  

                </Container>
        </Container>
        );
    }
}

const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`

const Lineup = styled.div`

`

const Position = styled.div`

`

const mapStateToProps = state => ({
    teams: state.leagueReducer.teams,
    league: state.leagueReducer.league,
    metaData: state.contentReducer.fantasyMetaData,
    sad: state.contentReducer.fantasySAD,
    fantasyRankings: state.contentReducer.fantasyRankings,
    passingData: state.weeklyDataReducer.passingData,
    rushingData: state.weeklyDataReducer.rushingData,
    receivingData: state.weeklyDataReducer.receivingData,
    players: state.rosterReducer.skillPositions,
    dkData: state.contentReducer.draftkings,
    fantasyAggregation: state.contentReducer.fantasyAggregation
})

export default connect(mapStateToProps, actions)(DraftKings)