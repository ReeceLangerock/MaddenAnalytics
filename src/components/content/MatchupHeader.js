import React from 'react';
import styled from 'styled-components'

import {
	Container,
	Button,
	Modal,
	Dimmer, Loader,
	Image,
	Grid,
	Dropdown,
} from 'semantic-ui-react'

export default class BannerAd extends React.Component {
    constructor(props){
        super(props)
    }

    
    render() {

        const {homeTeam, awayTeam} = this.props
        let htLogo, atLogo
		try {
			htLogo = require(`./../../assets/logos/${(homeTeam).toLowerCase()}.svg`)
			atLogo = require(`./../../assets/logos/${(awayTeam).toLowerCase()}.svg`)

		} catch (e) {
			htLogo = require(`./../../assets/logos/notfound.jpg`)
			atLogo = require(`./../../assets/logos/notfound.jpg`)

		}

        return (
            <Header>
                <StyledImage>
                
            <Image src ={htLogo} fluid alt ={this.props.homeTeam}/>
            <div>
            <h1>{this.props.homeTeam}</h1>
            <h1>{this.props.homeUser}</h1>
            </div>
            
            </StyledImage>
        


                vs.
                <StyledImage>
            <Image src ={atLogo} alt ={this.props.awayTeam} fluid/>
            <h1>{this.props.awayUser}</h1>
            </StyledImage>

            
       

            </Header>
        );
    }
}

const Header = styled.div`
justify-content: space-between;
align-items: center;
display: flex;`

const StyledImage = styled.div`
width: 150px;
display: flex;

`

