import React, { Component } from 'react'
import { Header, Segment, Container, Button, Icon } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom';

const ErrorPageWithRouter = withRouter(props => <ErrorPage {...props} />);

export class ErrorPage extends Component {

	constructor(props) {
		super(props)
		this.handleClick = this.handleClick.bind(this)

	}

	handleClick() {
		this.props.history.goBack()

	}
	render() {


		return (
			<Segment

				textAlign="center"
				style={{ minHeight: 30, padding: '2em 0em', border: 'none', boxShadow: 'none' }}

			>
				<Container text>
					<Header
						as="h1"


						style={{
							fontSize: '4em',
							fontWeight: 'normal',
							marginBottom: '2rem',
							marginTop: '2em',
							letterSpacing: '2px'
						}}
					>
						You're in the wrong neighborhood.
						</Header>
					<Button primary size="huge" onClick={this.handleClick}
					>
						<Icon name="left arrow" />
						Back To Safety
						</Button>

				</Container>
			</Segment>
		)
	}
}

export default ErrorPageWithRouter

