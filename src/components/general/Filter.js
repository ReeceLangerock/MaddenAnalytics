import React from 'react';
import {
    Modal,
    Button,
    Header,
    Checkbox,
    Input
    , Icon
} from 'semantic-ui-react'
import Teams from './../../TeamStyleSheet.js'
import styled from 'styled-components'
import { connect } from 'react-redux'

import * as actions from './../../redux/actions/actions'


class Filter extends React.Component {

    constructor(props) {
        super(props)
        this.applyAndClose = this.applyAndClose.bind(this)
        const filter = Object.assign({}, this.props.filter);
        this.state = {
            modalOpen: false,
            filter: filter
        }

    }

    renderTeamCheckboxes() {
        const filter = this.state.filter;

        const teams = Object.keys(Teams.teams)
        return teams.map((team) => {
            return <Checkbox key={`${team}filter}`} name='team' value={team} checked={filter.teams[team]} onChange={this.handleChange} label={team} />
        })
    }

    handleOpen = () => this.setState({ modalOpen: true })

    handleClose = () => {
        this.setState({
            filter: {
                teams: this.props.filter.teams,
                positions: this.props.filter.positions,
                minAttempts: this.props.filter.minAttempts
            },
            modalOpen: false
        })
    }

    handleChange = (e, { name, value }) => {
        let teams = Object.assign({}, this.state.filter.teams);

        if (name === 'position') {
            let positions = Object.assign({}, this.state.filter.positions);
            positions[value] = !positions[value]
            this.setState({
                filter: {
                    ...this.state.filter,
                    positions: positions
                }
            })
        } else if (name === 'team') {
            teams[value] = !teams[value]
            this.setState({
                filter: {
                    ...this.state.filter,
                    teams: teams
                }
            })
        }
        else if (name === 'all') {
            Object.keys(teams).forEach((key, index) => {
                teams[key] = true;
            });
            this.setState({
                filter: {
                    ...this.state.filter,
                    teams: teams
                }
            })
        }
        else if (name === 'none') {
            Object.keys(teams).forEach((key, index) => {
                teams[key] = false;
            });
            this.setState({
                filter: {
                    ...this.state.filter,
                    teams: teams
                }
            })
        } else if (name === 'min') {

            value = value > 0 ? value : 0;
            this.setState({
                filter: {
                    ...this.state.filter,
                    //eslint-disable-next-line
                    minAttempts: parseInt(value)
                }
            })
        }
    }



    applyAndClose() {

        const filters = this.state.filter
        this.props.applyFilters(filters)


        this.handleClose()


    }

    render() {
        const filter = this.state.filter;
        return (<Modal trigger={<Button onClick={this.handleOpen} circular icon='filter' />}
            open={this.state.modalOpen}
            onClose={this.handleClose}>
            <Modal.Header>Data Filters</Modal.Header>
            <Modal.Content >

                <Modal.Description>
                    <Header>Position</Header>
                    <CheckboxContainer>

                        <Checkbox label='Quarterback' name='position' value='QB' checked={filter.positions.QB} onChange={this.handleChange} />
                        <Checkbox label='Wide Receiver' name='position' value='WR' checked={filter.positions.WR} onChange={this.handleChange} />
                        <Checkbox label='Tight End' name='position' value='TE' checked={filter.positions.TE} onChange={this.handleChange} />
                        <Checkbox label='Running Back' name='position' value='HB' checked={filter.positions.HB} onChange={this.handleChange} />
                        <Checkbox label='Full Back' name='position' value='FB' checked={filter.positions.FB} onChange={this.handleChange} />
                    </CheckboxContainer>



                </Modal.Description>
                <br />
                <Modal.Description>
                    <Header>Team</Header>
                    <TeamsContainer>

                        {this.renderTeamCheckboxes()}

                    </TeamsContainer>
                    <br />

                    <Modal.Actions>

                        <Button content='Select All' name='all' onClick={this.handleChange} />
                        <Button content='Select None' name='none' onClick={this.handleChange} />
                    </Modal.Actions>
                </Modal.Description>
                <br />
                <Modal.Description>
                    <Header>Minimum {this.props.minDescription}</Header>
                    <Input value={filter.minAttempts} placeholder={`Minimum ${this.props.minDescription}`} name='min' onChange={this.handleChange} />


                </Modal.Description>

            </Modal.Content>
            <Modal.Actions>
                <Button color='red' inverted onClick={this.handleClose}>
                    <Icon name='remove' /> Cancel
      </Button>
                <Button color='green' inverted onClick={this.applyAndClose}>
                    <Icon name='checkmark' /> Apply Filters
      </Button>
            </Modal.Actions>
        </Modal>)

    }
}

const mapStateToProps = state => ({
    filter: state.filterReducer.filter


})
export default connect(mapStateToProps, actions)(Filter)


const CheckboxContainer = styled.div`
display: grid;
  grid-template-areas: "col1 col2 col3 col4";
  grid-column-gap: 5px;
  grid-row-gap: 5px;

`

const TeamsContainer = styled.div`
display: grid;
grid-template-rows: repeat(8, auto);
grid-gap: 5px;
grid-auto-flow: column;
grid-auto-columns: auto auto auto auto;

`