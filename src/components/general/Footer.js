import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import { Container, Grid, Header,  List, Segment } from 'semantic-ui-react'

export default class Contact extends Component {
	render() {
		return (
			<Segment
				inverted
				vertical
				style={{ padding: '5em 0em', marginTop: '3em'}}
			>
				<Container>
					<Grid divided inverted stackable columns={3}>
						<Grid.Row>
						<Grid.Column width={3}>

								<Header inverted as="h4" content="About" />
								<List link inverted>
									<List.Item as={Link} to="/about">
										About
									</List.Item>

									<List.Item as={Link} to="/blog">
										Blog
									</List.Item>
									<List.Item as={Link} to="/contact">
									Placeholder
									</List.Item>
								</List>
							</Grid.Column>

							<Grid.Column width={3}>

								<Header inverted as="h4" content="Portfolio" />
								<List link inverted>
									<List.Item as={Link} to="/portfolio">
										Placeholder
									</List.Item>
								</List>
							</Grid.Column>
							<Grid.Column width={7}>
							
								<Header as="h4" inverted>
									Madden Analytics
								</Header>
								<p>Take your league to the next level.</p>
								<List horizontal inverted>
									{/* <List.Item
										href="https://twitter.com/Grickle_Dev"
										target="_blank"
									>
										<Icon size="big" link name="twitter" />
										<List.Content />
									</List.Item>
									<List.Item
										href="https://www.instagram.com/grickle_dev/"
										target="_blank"
									>
										<Icon size="big" link name="instagram" />
										<List.Content />
									</List.Item>
								
									<List.Item
										href="https://www.linkedin.com/company/grickle/"
										target="_blank"
									>
										<Icon size="big" link name="linkedin" />
										<List.Content />
									</List.Item>
									<List.Item
										href="https://www.facebook.com/grickle.dev/"
										target="_blank"
									>
										<Icon size="big" link name="facebook" />
										<List.Content />
									</List.Item> */}
								</List>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Container>
			</Segment>
		)
	}
}
