import React from 'react';
import {
    Loader,
    Dimmer,

    Segment,

} from 'semantic-ui-react'

export default class ShowLoading extends React.Component {

    render() {
        return (<Segment key='loader' style={{ height: '300px' }}>

            <Dimmer active style={{ height: '300px' }}>
                <Loader size='massive' content={this.props.loaderMessage || 'Content Generation in Process'} />
            </Dimmer>
        </Segment>)

    }
}
