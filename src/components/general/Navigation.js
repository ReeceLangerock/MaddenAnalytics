import React, { Component } from 'react'
import styled from 'styled-components'
import { Menu } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
export class Navigation extends Component {
	state = { activeItem: 'home' }

	handleItemClick = (e, { name }) => this.setState({ activeItem: name })

	render() {
		const { activeItem } = this.state

		return (
			<StyledMenu
				inverted
				stackable
				style={{
					marginBottom: '3em',
					borderRadius: '0px'
				}}
			>
				<Menu.Item
					name="DeepDive.ML"
					as={Link}
					to="/"

					style={{

						fontFamily: 'Coyote',
						fontSize: '1.75rem',
						padding: '.25rem .75rem'
					}}
				>DeepDive.ML</Menu.Item>

				<Menu.Menu position="right">
				<Menu.Item
						name="contact"
						active={activeItem === 'find'}
						onClick={this.handleItemClick}
						as={Link}
						to="/browse"
					>
						Find Your League
					</Menu.Item>
					<Menu.Item
						name="contact"
						active={activeItem === 'add'}
						onClick={this.handleItemClick}
						as={Link}
						to="/add_league"
					>
						Add Your League
					</Menu.Item>
					{/* <Menu.Item
						name="about us"
						disabled
						active={activeItem === 'about'}
						onClick={this.handleItemClick}
						as={Link}
						to="/about"
					>
						About
					</Menu.Item>{' '}

					<Menu.Item
						name="blog"
						disabled
						active={activeItem === 'blog'}
						onClick={this.handleItemClick}
						as={Link}
						to="/blog"
					>
						Blog
					</Menu.Item>
					<Menu.Item
						name="contact"
						disabled
						active={activeItem === 'contact'}
						onClick={this.handleItemClick}
						as={Link}
						to="/contact"
					>
						Contact Us
					</Menu.Item> */}
				</Menu.Menu>
			</StyledMenu>
		)
	}
}

export default Navigation

const StyledMenu = styled(Menu) `;`
