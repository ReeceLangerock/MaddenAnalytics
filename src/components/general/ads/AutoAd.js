import React from 'react';
import styled from 'styled-components'

export default class BannerAd extends React.Component {
    componentDidMount() {
        (window.adsbygoogle = window.adsbygoogle || []).push({});
    }

    render() {
        return (
            <Ad className='ad' height={this.props.height}>
               <ins className="adsbygoogle"
     style={{display: "block"}}
     data-ad-client="ca-pub-1482869089838229"
     data-ad-slot="2950438843"
     data-ad-format="auto"></ins>

            </Ad>
        );
    }
}


const Ad = styled.div`
width: 100%;
height: ${props => props.height};
border: 1px solid;
margin: 10px auto;
`

