import React, { Component } from 'react'
import styled from 'styled-components'

import {
    Container,
    Dimmer, Loader,
    Header,
    Segment,
    Table
} from 'semantic-ui-react'
import { connect } from 'react-redux'

import * as actions from './../../redux/actions/actions'
import LeagueDashboard from '../league/LeagueDashboard';

class UserLeaderboard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            usersReady: false,
            userLeaderboard: undefined,
			column: null,
			direction: null,

        }

    }

    componentWillMount() {
        if(this.props.userLeaderboard){
            this.setState({userLeaderboard: this.props.userLeaderboard})
        }

    }

    componentWillReceiveProps(newProps) {
       if(newProps.userLeaderboard){
           this.setState({userLeaderboard: newProps.userLeaderboard})
       }


    }




    renderRankingsTable() {
        let data = this.props.userLeaderboard
        if (!this.props.userLeaderboard) {

            return
        }

        
        return (data).map((key, index) => {

            let teams = Object.keys(key)
            teams.shift()
            teams.shift()
            let numTeams = teams.length
            const rows = teams.map((t, teamIndex) => {
                let gamesPlayed = (key[t].wins || 0) + (key[t].losses || 0) + (key[t].ties || 0)
                if (teamIndex === 0) {


                    return (

                        <Table.Row key={key.user + teamIndex}>
                            <Table.Cell textAlign="center" rowSpan={numTeams}>{index + 1} </Table.Cell>
                            <Table.Cell textAlign="center" > {key.user} </Table.Cell>
                            <Table.Cell textAlign="center">{key.weeksInLeague || 0}</Table.Cell>
                            <Table.Cell textAlign="center">{t}</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].weeksAsOwner}</Table.Cell>
                            <Table.Cell textAlign="center">{gamesPlayed || 0}</Table.Cell>
                            <Table.Cell textAlign="center">{((key[t].pointsScored / gamesPlayed).toFixed(2)) || 0}</Table.Cell>
                            <Table.Cell textAlign="center">{(key[t].pointsAllowed / gamesPlayed).toFixed(2) || 0}</Table.Cell>
                            <Table.Cell textAlign="center">{(key[t].pointsScored - key[t].pointsAllowed)  || 0}</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].wins}</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].losses}</Table.Cell>
                            <Table.Cell textAlign="center">{(((key[t].wins || 0) / gamesPlayed)* 100).toFixed(2)  || 0}%</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].playoffWins}</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].playoffLosses}</Table.Cell>



                        </Table.Row>
                    )
                } else {
                    return (
                        <Table.Row key={key.user + teamIndex}>
                            <Table.Cell textAlign="center"></Table.Cell>
                            <Table.Cell textAlign="center"></Table.Cell>

                            <Table.Cell textAlign="center">{t}</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].weeksAsOwner}</Table.Cell>
                            <Table.Cell textAlign="center">{gamesPlayed || 0}</Table.Cell>
                            
                            <Table.Cell textAlign="center">{((key[t].pointsScored / key[t].weeksAsOwner).toFixed(2)) || 0}</Table.Cell>
                         
                            <Table.Cell textAlign="center">{(key[t].pointsAllowed / key[t].weeksAsOwner).toFixed(2) || 0}</Table.Cell>
                            <Table.Cell textAlign="center">{(key[t].pointsScored - key[t].pointsAllowed)  || 0}</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].wins}</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].losses}</Table.Cell>
                            <Table.Cell textAlign="center">{(((key[t].wins || 0) / gamesPlayed)* 100).toFixed(2)  || 0}%</Table.Cell>
                         
                 
                            <Table.Cell textAlign="center">{key[t].playoffWins}</Table.Cell>
                            <Table.Cell textAlign="center">{key[t].playoffLosses}</Table.Cell>
                          

                        </Table.Row>
                    )

                }
            })

            return (
                rows


            )
        })
    }

    renderLoader() {
        if (!this.props.userLeaderboard) {




            return (<Segment key='loader' style={{ height: '300px' }}>

                <Dimmer active style={{ height: '300px' }}>
                    <Loader size='massive' content='Generating Rankings' />
                </Dimmer>
            </Segment>)

        }

    }





    render() {
		const { column, direction } = this.state

        return (
            <Container>
                <LeagueDashboard />
                <Segment>
                    <HeaderContainer>
                        <Header
                            as="h1"
                            content="User Rankings"
                        />



                    </HeaderContainer>

                </Segment>
                {this.renderLoader()}

                {this.props.userLeaderboard &&

                    <Container style={{ overflowX: 'scroll' }}>
                        <Table celled unstackable singleline="true" compact>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell textAlign="center" >#</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center" >User</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Weeks In League</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">
                                        Teams Owned
								</Table.HeaderCell>
                                
                                    <Table.HeaderCell textAlign="center">Weeks With Team</Table.HeaderCell>
                                <Table.HeaderCell textAlign="center">Games Played</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Avg. Points Scored</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Avg. Points Allowed</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Point Differential</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center" >Wins</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center" >Losses</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Win %</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Playoff Wins</Table.HeaderCell>
                                    <Table.HeaderCell textAlign="center">Playoff Losses</Table.HeaderCell>


                                </Table.Row>
                            </Table.Header>

                            <Table.Body>{this.renderRankingsTable()}</Table.Body>
                        </Table>

                    </Container>
                }
            </Container>
        )
    }
}

const HeaderContainer = styled.div`
display: flex;
align-items: flex-start;

justify-content: space-between;`

const mapStateToProps = state => ({
    userLeaderboard: state.contentReducer.userLeaderboard
});

export default connect(mapStateToProps, actions)(UserLeaderboard);