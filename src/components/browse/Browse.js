import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import Moment from 'react-moment'
import AutoAd from './../general/ads/AutoAd'

import xboxLogo from './../../assets/xboxLogo.svg'
import psLogo from './../../assets/psLogo.svg'
import question from './../../assets/question.svg'

import * as actions from './../../redux/actions/actions'
import {
	Button,
	Container,
	Input,
	Header,
	Image,
	Segment,
	Table,
} from 'semantic-ui-react'

class Browse extends Component {
	constructor(props) {
		super(props)
		this.handleChange = this.handleChange.bind(this)

		this.state = {
			searchText: ''
		}
	}

	componentWillMount() {
		this.props.getLeagues({ query: '' })
	}

	handleChange(e, { query }) {
		this.setState({ searchText: e.target.value },
			() => this.props.getLeagues({ query: this.state.searchText })
		)

	}

	renderLeagues() {
		return this.props.leagues.map(league => {
			let logo
			if (league.console === 'xbox') {
				logo = xboxLogo
			} else if (league.console === 'ps3' || league.console === 'ps4') {
				logo = psLogo
			} else {
				logo = question
			}
			const status = league.leagueId === 'pending' ? 'Pending' : 'Active'
			return (
				<Table.Row negative={league.leagueId === 'pending'} key={league.abbreviation}>
					<Table.Cell textAlign="center">
						<Image avatar src={logo} />
					</Table.Cell>
					<Table.Cell textAlign="center">{league.name}</Table.Cell>
					<Table.Cell textAlign="center">{league.abbreviation}</Table.Cell>
					<Table.Cell textAlign="center">
						{' '}
						<Moment format="MM/DD/YYYY">{league.createdAt}</Moment>
					</Table.Cell>
					<Table.Cell textAlign="center">
					<Moment fromNow>{league.updatedAt}</Moment>
					</Table.Cell>
					<Table.Cell textAlign="center">{status}</Table.Cell>

					<Table.Cell textAlign="center">
						<Button as={Link} to={`/${league.abbreviation}`}>
							Go To League
						</Button>
					</Table.Cell>
				</Table.Row>
			)
		})
	}
	render() {
		return (
			<div>
				<Segment
					textAlign="center"
					vertical
					style={{ borderBottom: 'none', marginBottom: '2em' }}
				>
					<Container text>
						<Header
							as="h1"
							content="League Finder"
							style={{
								fontSize: '4em',
								fontWeight: 'normal',
								marginBottom: 0,
								marginTop: '0em'
							}}
						/>
						<Header
							as="h2"
							content="Search for your league by name or abbreviation"
							style={{
								fontSize: '1.7em',
								fontWeight: 'normal',
								marginBottom: '1em'
							}}
						/>
						<Input fluid icon='search' placeholder='Search...' onChange={this.handleChange} />
					</Container>
				</Segment>
				<Container>
					<Table celled singleline='true'>
						<Table.Header>
							<Table.Row>
								<Table.HeaderCell textAlign="center">Console</Table.HeaderCell>
								<Table.HeaderCell textAlign="center">Name</Table.HeaderCell>
								<Table.HeaderCell textAlign="center">
									Abbreviation
								</Table.HeaderCell>
								<Table.HeaderCell textAlign="center">
									Date Added
								</Table.HeaderCell>
								<Table.HeaderCell textAlign="center">
									Last Updated
								</Table.HeaderCell>
								<Table.HeaderCell textAlign="center">Status</Table.HeaderCell>
								<Table.HeaderCell textAlign="center">
									League Page
								</Table.HeaderCell>
							</Table.Row>
						</Table.Header>

						<Table.Body>{this.renderLeagues()}</Table.Body>
					</Table>
					
				</Container>

			</div>
		)
	}
}

const mapStateToProps = state => ({
	leagues: state.browseLeaguesReducer.leagues
})
export default connect(mapStateToProps, actions)(Browse)
