import React, { Component } from 'react'
import styled from 'styled-components'

import {
    Container,
    Divider,
    Header,
    Segment,
    Grid,
} from 'semantic-ui-react'
import { connect } from 'react-redux'
import TeamStyleSheet from './../../TeamStyleSheet.js'
import AutoAd from './../general/ads/AutoAd'
import LeagueDashboard from '../league/LeagueDashboard';

import * as actions from './../../redux/actions/actions'

class League extends Component {
    constructor(props) {
        super(props)
        this.renderLeagueHeader = this.renderLeagueHeader.bind(this)

        this.state = {
            selectedTeam: undefined,
            owners: []
        }
    }

    componentDidMount() {
        const team = this.props.location.pathname.split('/')[3]

        this.setState({
            selectedTeam: team
        })

    }


    componentWillReceiveProps(newProps) {
        if (newProps.teams) {
            this.findTeam(newProps.teams)
        }
    }

    // findTeam(teams) {
    //     let selectedTeam = this.state.selectedTeam
    //     let teamInfo
    //     teams.forEach((team) => {
    //         if (team.name === selectedTeam) {
    //             teamInfo = team
    //         }
    //     })

    // }

    renderLeagueHeader() {
        // const league = this.props.league
        const team = this.state.selectedTeam
        const teamStyles = TeamStyleSheet.teams[team] || { primary: '255,255,255', secondary: '0,0,0' }


        return (

            <LeagueHeader>
                <div>
                    <Header as="h1" style={{

                        color: `rgba(${teamStyles.secondary},1)`
                    }}>{this.state.selectedTeam}</Header>
                </div>


            </LeagueHeader>

        )

    }



    render() {
        const team = this.state.selectedTeam
        const teamStyles = TeamStyleSheet.teams[team] || { primary: '255,255,255', secondary: '0,0,0' }
        return (
            <Container


            >
                <LeagueDashboard />


                <Segment style={{
                    background: `rgba(${teamStyles.primary},1)`,
                    color: `rgba(${teamStyles.secondary},1) `

                }}>{this.renderLeagueHeader()}</Segment>

                {
                    this.props.league.leagueActivated &&
                    <div>
                        <Header as="h1" style={{
                            color: 'green !important'
                        }}></Header>

                        <Grid stackable columns={5}>



                        </Grid>


                        


                        <Divider style={{ margin: '2rem 0' }} />

                        <Header as="h1">Team Content</Header>
                        <Grid stackable columns={8}>

                        </	Grid>
                    </div>
                }
            </Container >
        )
    }
}

const mapStateToProps = state => ({
    league: state.leagueReducer.league,
    teams: state.leagueReducer.teams
})
export default connect(mapStateToProps, actions)(League)


const LeagueHeader = styled.div`
display: flex;
justify-content: space-between;
align-items: flex-start;
align-self: flex-start;
align-content: flex-start;
color: green;
`
