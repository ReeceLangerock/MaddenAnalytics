import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

import * as actions from './../../redux/actions/actions';
import AutoAd from './../general/ads/AutoAd'

import {
	Button,
	Container,
	Divider,
	Grid,
	Header,
	Icon,
	Image,
	List,
	Segment,
} from 'semantic-ui-react'

class Index extends Component {

	render() {
		return (
			<div>
				<Segment

					textAlign="center"
					style={{ minHeight: 600, padding: '2em 0em', }}
					vertical
				>
					<Container text>
						<Header
							as="h1"


							style={{
								fontSize: '4em',
								fontWeight: 'normal',
								marginBottom: 0,
								marginTop: '2em',
								letterSpacing: '2px'
							}}
						>
							DeepDive.M<span style={{ fontSize: '1.5rem', fontStyle: 'italic' }}>adden</span>L<span style={{ fontSize: '1.5rem', fontStyle: 'italic' }}>eague</span>
						</Header>
						<Header
							as="h2"
							content="Take your league to the next level with advanced stats and analysis."

							style={{ fontSize: '1.7em', fontWeight: 'normal' }}
						/>

						<Button primary size="huge" as={Link}
							to="/browse">
							Find Your League
							<Icon name="right arrow" />
						</Button>
						<Divider horizontal style={{ color: 'white' }}>Or</Divider>
						<Button primary size="huge" as={Link}
							to="/add_league">
							Add It For Free
							<Icon name="right arrow" />
						</Button>
					</Container>
				</Segment>

				<Segment style={{ padding: '8em 0em' }} vertical>
					<Grid container stackable verticalAlign="middle">
						<Grid.Row style={{ marginBottom: '2em' }}>
							<Grid.Column width={8}>
								<Header as="h3" style={{ fontSize: '2em' }}>
									Weekly Insights
								</Header>
								<p style={{ fontSize: '1.33em' }}>
									Automated articles custom generated for the league and for
									each team, letting you see your league detailed like never
									before.
								</p>
								<List bulleted>
									<List.Item>Player Performance Analysis</List.Item>
									<List.Item>Change in Team Rankings</List.Item>
									<List.Item>Matchup Breakdown</List.Item>
								</List>
							</Grid.Column>
							<Grid.Column floated="right" width={6}>
								<Image
									bordered
									rounded
									size="large"
									src="/assets/images/wireframe/white-image.png"
								/>
							</Grid.Column>
						</Grid.Row>
						<Grid.Row style={{ marginBottom: '2em' }}>
							<Grid.Column floated="left" width={6}>
								<Image
									bordered
									rounded
									size="large"
									src="/assets/images/wireframe/white-image.png"
								/>
							</Grid.Column>
							<Grid.Column width={8}>
								<Header as="h3" style={{ fontSize: '2em' }}>
									Fantasy Breakdown
								</Header>
								<p style={{ fontSize: '1.33em' }}>
									View the league-wide top players at each position or for an
									individual team, both week and season long stats are
									available.
								</p>
								<List bulleted>
									<List.Item>Customizable Fantasy Scoring</List.Item>
									<List.Item>View season long positional high scores</List.Item>
									<List.Item>
										See how each team fares against different position and
										player archetypes
									</List.Item>
									<List.Item>Fantasy Studs and Duds</List.Item>
								</List>
							</Grid.Column>
						</Grid.Row>
						<Grid.Row style={{ marginBottom: '2em' }}>
							<Grid.Column width={8}>
								<Header as="h3" style={{ fontSize: '2em' }}>
									Advanced Rankings
								</Header>
								<p style={{ fontSize: '1.33em' }}>
									See how each team stacks up compared to others in a number of
									different formats
								</p>
								<List bulleted>
									<List.Item>Pythagorean Power Rankings</List.Item>
									<List.Item>ELO Ratings System</List.Item>
								</List>
							</Grid.Column>
							<Grid.Column floated="right" width={6}>
								<Image
									bordered
									rounded
									size="large"
									src="/assets/images/wireframe/white-image.png"
								/>
							</Grid.Column>
						</Grid.Row>
						<Grid.Row style={{ marginBottom: '2em' }}>
							<Grid.Column floated="left" width={6}>
								<Image
									bordered
									rounded
									size="large"
									src="/assets/images/wireframe/white-image.png"
								/>
							</Grid.Column>
							<Grid.Column width={8}>
								<Header as="h3" style={{ fontSize: '2em' }}>
									Draft Recap
								</Header>
								<p style={{ fontSize: '1.33em' }}>
									Analysis on each teams draft, the draft class and individual
									rookies
								</p>
								<List bulleted>
									<List.Item>Positional Breakdown by Round</List.Item>
									<List.Item>Best and worst value picks</List.Item>
									<List.Item>
										Player Comparison - See what current player each rookie most
										closely resembles
									</List.Item>
									<List.Item>
										Attribute breakdown for each teams rookies
									</List.Item>
								</List>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
				<Segment style={{ padding: '0em' }} vertical>
					<Grid celled="internally" columns="equal" stackable>
						<Grid.Row textAlign="center">
							<Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
								<Header as="h3" style={{ fontSize: '2em' }}>
									"There are lies, damned lies and statistics."
								</Header>
								<p style={{ fontSize: '1.33em' }}>
									<b> - Mark Twain</b>
								</p>
							</Grid.Column>
							<Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
								<Header as="h3" style={{ fontSize: '2em' }}>
									"Self-praise is for losers. Be a winner. Stand for something.
									Always have class, and be humble."
								</Header>
								<p style={{ fontSize: '1.33em' }}>
									<b> - John Madden</b>
								</p>
							</Grid.Column>
						</Grid.Row>
					</Grid>
					

				</Segment>
				<Segment style={{ padding: '8em 0em' }} vertical>
					<Container text>
						<Header as="h3" style={{ fontSize: '2em' }}>
							Deep Dive.ml
						</Header>
						<p style={{ fontSize: '1.33em' }}>
							Content isn't necessary for a Madden League to be a success but it
							makes the experience invariably better. Having interesting stats
							and articles to build interest, provoke trash-talk and backup
							banter makes a league that much better. Let us generate the
							content for you.
						</p>
						<Button as="a" size="large">
							Find Out More
						</Button>
						<Divider />

						<Header as="h3" style={{ fontSize: '2em' }}>
							What more do you need to hear?
						</Header>
						<p style={{ fontSize: '1.33em' }}>It's free! Sign up now!</p>
						<Button as="a" size="massive">
							Sign Up
						</Button>
					</Container>
					

				</Segment>
			</div>
		)
	}
}

const mapStateToProps = state => ({
});
export default connect(mapStateToProps, actions)(Index);