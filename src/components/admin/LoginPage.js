import React, { Component } from 'react'
// import styled from 'styled-components'

import { Button, Form, Grid, Header, Image, Segment } from 'semantic-ui-react'
import { connect } from 'react-redux'

import * as actions from './../../redux/actions/actions'

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

        this.state = {
            leagues: [],
            form: {
                username: '',

                password: '',

            }
        }
    }


    handleSubmit() {
        let form = this.state.form
        this.props.authenticateAdmin(form)

        this.setState({
            form
        })
    }

    handleChange(e, { field, value }) {
        let form = this.state.form
        form[field] = value
        this.setState({ form })
    }


    render() {
        return (
            <div className='login-form'>

                <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style>
                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Header as='h2' color='teal' textAlign='center'>
                            <Image src='/logo.png' />
                            {' '}Log-in to your admin account
        </Header>
                        <Form size='large' onSubmit={this.handleSubmit}>
                            <Segment stacked>
                                <Form.Input
                                    fluid
                                    icon='user'
                                    iconPosition='left'
                                    field='username'
                                    placeholder='Username'
                                    value={this.state.form.username}
                                    onChange={this.handleChange}
                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    field='password'
                                    type='password'
                                    value={this.state.form.password}
                                    onChange={this.handleChange}
                                />

                                <Button color='teal' fluid size='large'>Login</Button>
                            </Segment>
                        </Form>

                    </Grid.Column>
                </Grid>
            </div>
        )
    }
}

export default connect(null, actions)(LoginForm)


