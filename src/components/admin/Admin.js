import React, { Component } from 'react'
// import { Link } from 'react-router-dom'
// import Moment from 'react-moment'

// import xboxLogo from './../../assets/xboxLogo.svg'
// import psLogo from './../../assets/psLogo.svg'
import { Button, Header, Container } from 'semantic-ui-react'
import { connect } from 'react-redux'
import LoginPage from './LoginPage';

import * as actions from './../../redux/actions/actions'



class Admin extends Component {
    render() {
        return (
            <div >

                {!this.props.authenticated &&

                    <LoginPage />}

                {this.props.authenticated

                    &&
                    <Container>
                        <Header
                            as="h1"
                            content="Welcome Back Commander"
                        />
                        <Button />
                    </Container>

                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    authenticated: state.adminReducer.authenticated
})

export default connect(mapStateToProps, actions)(Admin)


