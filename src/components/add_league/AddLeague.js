import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import Moment from 'react-moment'
import BannerAd from './../general/ads/BannerAd'

import xboxLogo from './../../assets/xboxLogo.svg'
import psLogo from './../../assets/psLogo.svg'
import {
	Container,
	Message,
	Button,
	Form,
	Header,
	List,
	Image
} from 'semantic-ui-react'
import { connect } from 'react-redux'

import * as actions from './../../redux/actions/actions'
import io from 'socket.io-client'
const baseApiUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:3001' : 'https://ddbl.herokuapp.com'; //hard coded for now - use env var later

let socket = io(baseApiUrl)

class AddLeague extends Component {
	constructor(props) {
		super(props)
		this.renderLeagues = this.renderLeagues.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)

		this.state = {
			leagues: [],
			form: {
				name: '',
				nameError: false,
				abbreviation: '',
				abbreviationError: false,
				errorMessage: '',
				submissionAttempt: false
			}
		}
	}

	componentDidMount() {
		socket = io(baseApiUrl)

		// this.props.getIfLeagueExists()
		socket.on('leagueFoundStatus', leagueData => {
			let leagues = this.state.leagues
			leagues.push(leagueData)
			this.setState({ leagues })
		})
	}

	componentWillUnmount() {
		socket.close()
	}

	componentWillReceiveProps(newProps) {
		if (newProps.addition.success) {
			this.props.history.push(
				`/${newProps.addition.league.abbreviation}`
			)
		}
	}

	handleSubmit() {
		let form = this.state.form
		if (form.name.length >= 2 && form.abbreviation.length >= 2) {
			form.nameError = false
			form.abbreviationError = false
			form.submissionAttempt = true
			this.props.addLeague(form)
		} else {
			form.nameError = form.name.length >= 2 ? false : true
			form.abbreviationError = form.abbreviation.length >= 2 ? false : true
			form.submissionAttempt = false

			if (!form.nameError && form.abbreviationError) {
				form.errorMessage =
					'The league abbreviation must be at least 2 characters long'
			} else if (form.nameError && !form.abbreviationError) {
				form.errorMessage = 'The league name must be at least 2 characters long'
			} else {
				form.errorMessage =
					'The league name and abbreviation must both be at least 2 characters long'
			}
		}

		this.setState({
			form
		})
	}

	handleChange(e, { field, value }) {
		let form = this.state.form
		form[field] = value
		this.setState({ form })
	}
	renderLeagues() {
		return this.state.leagues.map(league => {
			const logo = league.console === 'xbox' ? xboxLogo : psLogo
			if (league.abbreviation) {
				return (
					<List.Item>
						<Image avatar src={logo} />
						<List.Content>
							<List.Header as="a">
								{league.name} - {league.abbreviation}
							</List.Header>
							<List.Description>
								{league.name} has already been added. It was added on{<Moment format="YY/MM/DD">
									{league.createdAt}
								</Moment>}
								{league.dateAdded}.
							</List.Description>
						</List.Content>
						<List.Content floated="right">
							<Button as={Link} to={`/${league.abbreviation}`}>
								Go To League
							</Button>
						</List.Content>
					</List.Item>
				)
			} else {
				return (
					<List.Item>
						<Image avatar src={logo} />

						<List.Content>
							<List.Header as="a">League Not Found</List.Header>
							<List.Description>Go Ahead and Add it</List.Description>
						</List.Content>
					</List.Item>
				)
			}
		})
	}

	renderError() {
		if (
			this.state.form.submissionAttempt &&
			this.props.addition.success === false
		) {
			return (
				<StyledMessage error color="green">
					{this.props.addition.error || 'message'}
					<Button
						as={Link}
						to={`/${this.props.addition.league.abbreviation}`}
					>
						Go To League
					</Button>
				</StyledMessage>
			)
		}
		// render generic form error
	}

	render() {
		return (
			<Container>
				<Header
					as="h2"
					content="Take Your League to the Next Level"
					subheader="Enter in your league name and abbreviation. The abbreviation needs to be at least three characters, but the shorter the better. You'll be using that to import data from the Madden Companion App. If the league name and abbreviation aren't taken you'll be sent to the league homepage. If it's unsuccessful, we'll let you know why right here."
				/>
				<Form
					onSubmit={this.handleSubmit}
					error={this.state.form.nameError || this.state.form.abbreviationError}
				>
					<Form.Field>
						<label>League Name</label>
						<Form.Input
							error={this.state.form.nameError}
							placeholder="League Name"
							field="name"
							value={this.state.form.name}
							onChange={this.handleChange}
						/>
					</Form.Field>
					<Form.Field>
						<label>League Abbreviation</label>
						<Form.Input
							error={this.state.form.abbreviationError}
							placeholder="League Abbreviation"
							field="abbreviation"
							value={this.state.form.abbreviation}
							onChange={this.handleChange}
						/>
					</Form.Field>
					<Message error>{this.state.form.errorMessage}</Message>
					{this.renderError()}
					<Button type="submit">Join!</Button>
				</Form>
				<BannerAd height='90px' />

				<Header as="h2">
					Think Your League Might Already be Active?
					<Header.Subheader>
						Using the Madden Companion App, export the League Info data to{' '}
						<code>http://www.deepdive.ml/lg</code>. In a few seconds you'll see a
						notification pop up below letting you know.
					</Header.Subheader>
				</Header>

				<Container style={{ marginTop: '2em' }}>
					<List divided relaxed>
						{this.renderLeagues()}
					</List>
				</Container>
				<BannerAd height='90px' />

			</Container>
		)
	}
}

const mapStateToProps = state => ({
	addition: state.leagueAdditionReducer.addition
})
export default connect(mapStateToProps, actions)(AddLeague)

const StyledMessage = styled(Message) `
	display: flex !important;
	justify-content: space-between;
`
