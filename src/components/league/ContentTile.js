import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { Grid } from 'semantic-ui-react'

export default class ContentTile extends Component {
	
	render() {
			const toLink = this.props.disabled ? '#':`/${this.props.league}/${this.props.contentUrl}`;
		return (
			<Grid.Column key={this.props.contentUrl + 'Tile'}>

				<StyledLink to={toLink}>

					<Tile disabled={this.props.disabled}>

						<h3>{this.props.tileName}</h3>


					</Tile >
				</StyledLink>



			</Grid.Column>
		)
	}
}

const Tile = styled.div`
    position: relative;
    -webkit-box-shadow: 0 1px 2px 0 rgba(34,36,38,.15);
    box-shadow: 0 1px 2px 0 rgba(34,36,38,.15);
    padding: .5rem .5rem;
    text-align: center;
    border-radius: .28571429rem;
    border: 1px solid rgba(34,36,38,.15);
    cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
    height: 100px;
:hover {

	box-shadow: 0 2px 4px 0 rgba(34,36,38,.12), 0 2px 10px 0 rgba(34,36,38,.15)

}

background: ${props => props.disabled ? '#e0e1e2' : '#fff'};

`
const StyledLink = styled(Link) `
color: black;
:hover{color: black;}
`
