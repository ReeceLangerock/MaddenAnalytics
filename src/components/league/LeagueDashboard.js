import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import {
	Header,
	Menu,
	Dropdown,
} from 'semantic-ui-react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import * as actions from './../../redux/actions/actions'


const LeagueDashboardWithRouter = withRouter(props => <LeagueDashboard {...props} />);

class LeagueDashboard extends Component {

	componentDidMount() {

		const abbr = this.props.location.pathname.split('/')[1]
		if (this.props.league.abbreviation !== abbr) {
			this.props.getLeagueData(abbr, 'info')
		}


	}



	render() {
		const league = this.props.league
		const weekNames = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 'Wildcard', 'Divisional', 'Conference', 'Pro Bowl', 'Superbowl', "Offseason"]
		let loc = this.props.location.pathname.split('/')[2]
		let locCat = 		this.props.location.pathname.split('/')[2]
		if (loc === 'stats') {
			loc = this.props.location.pathname.split('/')[3]
			
		} else if (loc === 'team-stats'){
			loc = 'team-'+this.props.location.pathname.split('/')[3]
		}

		let week;
		if (league.currentWeek < 17) {
			week = 'Week ' + weekNames[league.currentWeek]
		} else if (league.currentWeek > 21) {
			week = 'Offseason';
		}

		else {
			week = weekNames[league.currentWeek]

		}

		return (
			<DashboardContainer key={`${league.abbreviation}dashboard`}>
				<DashboardHeader>
					<Header as="h1">{`${league.name}`}</Header>
					<StatusSpan>Season {league.currentSeason + 1} - {week} </StatusSpan>
				</DashboardHeader>

				<Menu size='small' attached="bottom">
					<Menu.Item name='Dashboard' as={Link} to={`/${league.abbreviation}`} />
					<Dropdown item text='Rankings'>
						<Dropdown.Menu>
							<Dropdown.Item active={loc === 'pythagorean'} as={Link} to={`/${league.abbreviation}/pythagorean`}>Pythagorean</Dropdown.Item>
							<Dropdown.Item active={loc === 'elo'} as={Link} to={`/${league.abbreviation}/elo`}>ELO </Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>
					<Dropdown item text='Player Stats'>
						<Dropdown.Menu>
							<Dropdown.Item active={loc === 'passing'} as={Link} to={`/${league.abbreviation}/stats/passing`}>Passing</Dropdown.Item>
							<Dropdown.Item active={loc === 'receiving'} as={Link} to={`/${league.abbreviation}/stats/receiving`}>Receiving </Dropdown.Item>
							<Dropdown.Item active={loc === 'rushing'} as={Link} to={`/${league.abbreviation}/stats/rushing`}>Rushing </Dropdown.Item>
							<Dropdown.Item active={loc === 'defensive'} as={Link} to={`/${league.abbreviation}/stats/defense`}>Defense </Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>

					<Dropdown item text='Team Stats' >
						<Dropdown.Menu > 
							<Dropdown.Item active={loc === 'team-passing'} as={Link} to={`/${league.abbreviation}/team-stats/passing`}>Passing</Dropdown.Item>
							<Dropdown.Item active={loc === 'team-receiving'} as={Link} to={`/${league.abbreviation}/team-stats/receiving`}>Receiving </Dropdown.Item>
							<Dropdown.Item active={loc === 'team-rushing'} as={Link} to={`/${league.abbreviation}/team-stats/rushing`}>Rushing </Dropdown.Item>
							<Dropdown.Item active={loc === 'team-defensive'} as={Link} to={`/${league.abbreviation}/team-stats/defense`}>Defense </Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>
					<Menu.Item name='Fantasy' as={Link} to={`/${league.abbreviation}/fantasy`} active={locCat === 'fantasy'}/>




				</Menu>
			</DashboardContainer>
		)
	}
}

const mapStateToProps = state => ({
	league: state.leagueReducer.league
})
export default connect(mapStateToProps, actions)(LeagueDashboardWithRouter)


const DashboardHeader = styled.div`
	display: flex;
	justify-content: space-between;
`

const DashboardContainer = styled.div`
`

const StatusSpan = styled.span`
	font-size: 1.35em;
	font-weight: bold;
	
`
