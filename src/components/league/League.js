import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Container,
	Label,
	Icon,
	Header,
	Segment,
	Grid,
} from 'semantic-ui-react'
import { connect } from 'react-redux'
import ContentTile from './ContentTile';
import TeamTile from './TeamTile';
import TeamStyleSheet from './../../TeamStyleSheet.js'
import Loader from './../general/Loader'


import * as actions from './../../redux/actions/actions'


class League extends Component {
	constructor(props) {
		super(props)
		this.renderLeagueHeader = this.renderLeagueHeader.bind(this)
	}

	renderLeagueHeader() {
		const league = this.props.league
		if (league.status === "noleague") {
			return (<LeagueHeader>
				<Header as="h1">League Not Found</Header>
			</LeagueHeader>)
		} else if (league.status === "Awaiting Data") {
			return (<Loader loaderMessage="Loading League Data" />)
		}
		else if (!league.leagueActivated) {
			return (
				<div>
					<LeagueHeader>
						<Header as="h1">{`${league.name}`}<Abbreviation>{` (${league.abbreviation})`}</Abbreviation></Header>
						<StatusSpan>{`PENDING`}</StatusSpan>
					</LeagueHeader>

					<p>
						Congratulations! You've completed the first step towards setting up
						your league. To complete the process, using the Madden Companion
						App, please send league data to{' '}
						<code>http://www.deepdive.ml/im/{league.abbreviation}</code>
					</p>
					<p>
						You can send just the League Data or all of the export options, it's
						up to you. If we receive your data successfully you'll see the
						the page will refresh and we'll handle whatever you sent at us and
						start generating your league content right away.
					</p>

					<p>
						<strong>Please note</strong>, if we haven't received any league data
						starting 24 hours from when the league was initialized your league
						name and abbreviation will be erased and you'll need to start the
						process over from the start.
					</p>
				</div>
			)
		} else {
			return (
					<LeagueHeader>
						<div>
							<Header as="h1" style={{ marginBottom: '0' }}>{`${league.name}`}<Abbreviation>{` (${league.abbreviation})`}</Abbreviation></Header>
							<LeagueInfo>Season {league.currentSeason + 1} - Week {league.currentWeek + 1} </LeagueInfo>
						</div>

						<LeagueOptionsContainer>
							<StyledLabel as='a'>
								<Icon name='setting' />
								Settings
					</StyledLabel>
							<StyledLabel as='a' style={{ marginLeft: 0 }}>
								<Icon name='check' />
								Data Checklist
  </StyledLabel>
							<StyledLabel as='a' style={{ marginLeft: 0 }}>
								<Icon name='info' />
								Info
  </StyledLabel>

						</LeagueOptionsContainer>
					</LeagueHeader>


			)
		}
	}


	renderTeamTiles() {
		const teams = TeamStyleSheet.teams
		return Object.keys(teams).map((team) => {
			return <TeamTile key={`${team}tile`} league={this.props.league.abbreviation} teamName={team} primaryColor={teams[team].primary} />
		})

	}
	render() {
		const abbr = this.props.league.abbreviation

		return (
			<div>

				<Container

				>

					<Segment>{this.renderLeagueHeader()}</Segment>

					{
						this.props.league.leagueActivated &&
						<div>
							<br/>
							<Header as="h1">League Content</Header>
							<Grid stackable columns={6}>
								<ContentTile league={abbr} tileName='Pythagorean Power Rankings' contentUrl='pythagorean' />
								<ContentTile league={abbr} tileName='ELO Rating System' contentUrl='elo' />
								<ContentTile league={abbr} tileName='Matchup Analysis' contentUrl='matchup' />
								<ContentTile league={abbr} tileName='Fantasy Breakdown' contentUrl='fantasy' />
								{/* <ContentTile league={abbr} tileName='Weekly Insights' disabled /> */}
								<ContentTile league={abbr} tileName='User Leaderboards' contentUrl='user/leaderboard' />
								<ContentTile league={abbr} tileName='Passing Statistics' contentUrl='stats/passing' />
								<ContentTile league={abbr} tileName='Receiving Statistics' contentUrl='stats/receiving' />
								<ContentTile league={abbr} tileName='Rushing Statistics' contentUrl='stats/rushing' />
								<ContentTile league={abbr} tileName='Team Passing Statistics'  contentUrl='team-stats/passing' />
								<ContentTile league={abbr} tileName='Team Rushing Statistics'  contentUrl='team-stats/rushing' />
								<ContentTile league={abbr} tileName='Team Receiving Statistics'  contentUrl='team-stats/receiving' />
								{/* <ContentTile league={abbr} tileName='Defensive Statistics' disabled />
								<ContentTile league={abbr} tileName='Playoff Odds' disabled />
								<ContentTile league={abbr} tileName='Weekly Insights' disabled /> */}

							</Grid>


							{/*  */}
							{/* <Divider style={{ margin: '2rem 0' }} /> */}
							{/* <Header as="h1">Team Content</Header>
							<Grid stackable columns={8}>
								{this.renderTeamTiles()}
							</	Grid>
							 */}

						</div>
					}

				</Container >
			</div>
		)
	}
}

const mapStateToProps = state => ({
	league: state.leagueReducer.league
})
export default connect(mapStateToProps, actions)(League)


const LeagueHeader = styled.div`
display: flex;
justify-content: space-between;
align-items: flex-start;
align-self: flex-start;
align-content: flex-start;
margin-bottom: 1rem;

`

const LeagueInfo = styled.span`
font-size: 1.1rem;

`

const Abbreviation = styled.span`
font-size: 1.1rem;
`

const StyledLabel = styled(Label) `
margin-bottom: 5px !important;`

const LeagueOptionsContainer = styled.div`
display: flex;

flex-direction: column;`

const StatusSpan = styled.span`
	font-size: 1.35em;
	font-weight: bold;
	animation: pulse 3s infinite;
	animation-direction: alternate;

	@keyframes pulse {
		0% {
			color: rgba(0, 0, 0, 0.87);
		}
		100% {
			color: #db2828;
		}
	}
`
