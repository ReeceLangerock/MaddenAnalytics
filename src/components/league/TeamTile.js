import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { Grid } from 'semantic-ui-react'


export default class TeamTile extends Component {


	render() {
		const teamName = this.props.teamName || 'bills'
		let image
		try {
			image = require(`./../../assets/logos/${(teamName).toLowerCase()}.svg`)

		} catch (e) {
			image = require(`./../../assets/logos/notfound.jpg`)

		}
		return (
			<Grid.Column >
				<StyledLink to={`/${this.props.league}/team/${this.props.teamName}/`}>
					<Tile image={image} primary={this.props.primaryColor} >

						{/* <h4>{this.props.teamName}</h4> */}


					</Tile >
				</StyledLink>


			</Grid.Column>
		)
	}
}

const Tile = styled.div`
    position: relative;
    box-shadow: 0 1px 2px 0 rgba(34,36,38,.15);
    padding: .5em .5em;
    text-align: center;
    border-radius: .28571429rem;
	border: 2px solid; 
	border-color: ${props => `rgb(${props.primary})` || 'rgba(34,36,38,.15)'};
	background-image: url(${props => props.image});
	height: 100px;
	width: 100%;
	background-size: contain;
	background-repeat: no-repeat;
	cursor: not-allowed;
	background-position: center; 
:hover {

	box-shadow: 0 2px 4px 0 rgba(34,36,38,.12), 0 2px 10px 0 rgba(34,36,38,.15)

}`

const StyledLink = styled(Link) `
color: black;
:hover{color: black;}
`


