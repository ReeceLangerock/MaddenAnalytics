import React, { Component } from 'react'
import styled from 'styled-components'

import {
	Progress
} from 'semantic-ui-react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import * as actions from './../../redux/actions/actions'
import lsManager from './../../redux/actions/localStorageManager'
import contentManager from './../../content_generation/contentManager'


const DataDownloaderWithRouter = withRouter(props => <DataDownloader {...props} />);

class DataDownloader extends Component {


	async componentWillMount() {


		if (!this.props.initiationStarted && !this.props.initiationCompleted) {

			await this.gatherData()
		}

	}

	async componentWillReceiveProps(newProps) {

		const abbr = newProps.location.pathname.split('/')[1]
		const league = newProps.league
		if(abbr === 'browse' || abbr === 'add_league' || abbr ===''){
			return;
		}

		if (league === undefined || league.abbreviation !== abbr) {
			await this.gatherData()
			
		}

		else if (!newProps.initiationStarted && !newProps.initiationCompleted) {
			await this.gatherData()
		}

	}

	async gatherData() {

		const league = this.props.league
		let stepsCompleted = 0;
		const abbr = this.props.location.pathname.split('/')[1]

		// IF LEAGUE DATA IS NOT PRESENT OR LEAGUE ABBREVIATION HAS CHANGED
		// THEN PHONE HOME TO GET BASIC LEAGUE INFO
		if (league === undefined || league.abbreviation !== abbr) {
		await this.props.getDKData()

			this.props.clearContent()
			contentManager.resetChecklist();
			stepsCompleted++
			this.props.updateInitiationStatus('Retreiving League Data', stepsCompleted)
			this.props.getLeagueData(abbr, 'info')
			return;

		}
		this.props.startInitiation()



		// CHECK LOCALSTORAGE FOR DATA
		stepsCompleted++
		this.props.updateInitiationStatus('Checking Local Storage for Data', stepsCompleted)


		const [missingData, storedData] = await lsManager.checkLocalStorage(league.abbreviation, league.currentWeek, league.currentSeason)

		for (let data of storedData) {
			let dataKey = Object.keys(data)[0]

			stepsCompleted++
			this.props.updateInitiationStatus(`Pulling ${dataKey} into memory`, stepsCompleted)
			await this.props.saveLocalStorageDataToStore(dataKey, data)
		}
		for (let data of missingData) {

			stepsCompleted++
			this.props.updateInitiationStatus(`Retreiving ${data} from server`, stepsCompleted)

			if (data === 'weeklyData') {
				await this.props.getWeeklyData(abbr, data)

			} else {
				await this.props.getLeagueData(abbr, data)
			}

		}

		stepsCompleted++
		this.props.updateInitiationStatus(`Removing old data from storage'`, stepsCompleted)
		await lsManager.clearLeagueData(league.abbreviation, league.currentWeek, league.currentSeason)



		stepsCompleted++
		this.props.updateInitiationStatus(`Data Download Completed'`, stepsCompleted)



		this.generateContent()

	}

	generateContent() {
		contentManager.initiate()
		setTimeout(() => {
			this.props.endInitiation()

		}, 1000)



	}


	render() {
		const abbr = this.props.location.pathname.split('/')[1]
		const leagueAbbr = this.props.league.abbreviation
		let completed = false;
		if (this.props.initiationCompleted) {
			completed = true
		}

		if (abbr === leagueAbbr && this.props.league.leagueActivated) {
			let percent = (this.props.stepsCompleted / this.props.stepsTotal) * 100
			return (
				<ProgressContainer completed={completed}>
					<Progress size='medium' style={{ border: '1px solid', background: 'rgba(255,255,255,.5)' }}
						percent={percent.toFixed(2)} color='green' progress	>{this.props.statusMessage}</Progress>
				</ProgressContainer>
			)
		} else {
			return null
		}
	}



}

const mapStateToProps = state => ({
	league: state.leagueReducer.league,
	loc: state.routing,
	stepsCompleted: state.contentReducer.stepsCompleted,
	stepsTotal: state.contentReducer.stepsTotal,
	statusMessage: state.contentReducer.statusMessage,
	initiationCompleted: state.contentReducer.initiationCompleted,
	initiationStarted: state.contentReducer.initiationStarted
})
export default connect(mapStateToProps, actions)(DataDownloaderWithRouter)


const ProgressContainer = styled.div`
opacity: ${props => props.completed ? 0 : 100};
width: 80%;
height: 50px;
	position: fixed;
	z-index: 1000;
	padding: 2px;
top: 90vh;
left: 10%;
margin: 0 auto;
transition: opacity 1s linear;
`
