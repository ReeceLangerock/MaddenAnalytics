export default {
 

    '49ers': ['NFC', 'WEST'],
    Bears: ['NFC', 'NORTH'],
    Bengals: ['AFC', 'NORTH'],
    Bills: ['AFC', 'EAST'],
    Broncos: ['AFC', 'WEST'],
    Browns: ['AFC', 'NORTH'],
    Buccaneers: ['NFC', 'SOUTH'],
    Cardinals: ['NFC', 'WEST'],
    Chargers: ['AFC', 'WEST'],
    Chiefs: ['AFC', 'WEST'],
    Colts:['AFC', 'SOUTH'],
    Cowboys: ['NFC', 'EAST'],
    Dolphins: ['AFC', 'EAST'],
    Eagles: ['NFC', 'EAST'],
    Falcons: ['NFC', 'SOUTH'],
    Giants: ['NFC', 'EAST'],
    Jaguars: ['AFC', 'SOUTH'],
    Jets: ['AFC', 'EAST'],
    Lions: ['NFC', 'NORTH'],
    Packers: ['NFC', 'NORTH'],
    Panthers:['NFC', 'SOUTH'],
    Patriots: ['AFC', 'EAST'],
    Raiders:['AFC', 'WEST'],
    Rams: ['NFC', 'WEST'],
    Ravens: ['AFC', 'NORTH'],
    Redskins:['NFC', 'EAST'],
    Saints: ['NFC', 'SOUTH'],
    Seahawks: ['NFC', 'WEST'],
    Steelers:['AFC', 'NORTH'],
    Texans: ['AFC', 'SOUTH'],
    Titans:['AFC', 'SOUTH'],
    Vikings: ['NFC', 'NORTH']
  

};
