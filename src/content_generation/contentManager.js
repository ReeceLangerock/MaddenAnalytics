import s from './../redux/store/store'
import * as actions from './../redux/actions/actions'
import elo from './eloRankings'
import pyth from './pythRankings'
import recAnalytics from './recAnalytics'
import passAnalytics from './passingAnalytics'
import rushAnalytics from './rushAnalytics'
import userAnalytics from './userAnalytics'
import matchup from './matchupAnalysis'
import fp from './fantasyPoints'

var cm = (function () {

    var store;

    const contentChecklist = {
        fantasy: {
            fantasyRankings: undefined,
            fantasyMetaData: undefined,
            fantasySAD: undefined
        },

        rankings: {
            elo: undefined,
            pyth: undefined
        },

        playerStats: {
            playerPassing: undefined,
            playerRushing: undefined,
            playerReceiving: undefined,
            playerDefense: undefined,
        },
        teamStats: {
            teamPassing: undefined,
            teamRushing: undefined,
            teamReceiving: undefined,
            teamDefense: undefined
        },
        user: {
            leaderboard: undefined,
        },

        matchup: {
            basic: undefined

        },

        teamContent: {

        }
    }


    return {

        initiate: async function () {
            var t0 = performance.now();
            store = s.getState();
            await this.checkLocalStorage()
            try {

                await this.generate()
            } catch (e) {
                console.log(e)
            }
            var t1 = performance.now();
            console.log("Generation took " + (t1 - t0) + " milliseconds.")
            return true;
        },

        checkLocalStorage: async function () {
            Object.keys(contentChecklist).forEach((category) => {
                Object.keys(contentChecklist[category]).forEach((subcat) => {

                    // placeholder for check local storage

                    if (false) {

                    }
                    else {

                    }

                    // if (!contentChecklist[category][subcat]) {
                    //     //IF CONTENT IS IN LOCAL STORAGE, SAVE TO STORE, UPDATE CONTENT GENERATION STATUS
                    // }
                })
            })
        },

        resetChecklist: () => {
            Object.keys(contentChecklist).forEach((category) => {
                Object.keys(contentChecklist[category]).forEach((subcat) => {

                    contentChecklist[category][subcat] = undefined
                })
            })

        },

        generate: async () => {
            // GET COMMONLY USED DATA FROM STORE
            const TEAMS = store.leagueReducer.teams
            const WEEKLY_DATA = store.leagueReducer.weeklyData
            const cc = contentChecklist;
            const CW = store.leagueReducer.league.currentWeek
            const CS = store.leagueReducer.league.currentSeason
            const STAGE = store.leagueReducer.league.currentStage
            const LEAGUE_NAME = store.leagueReducer.league.name
            let stepsCompleted = store.contentReducer.stepsCompleted
            var h2hStats, usersWithData, eloData;

            // FIRST GENERATE CONTENT THAT DOESN'T RELY ON ANY OTHER GENERATED DATA
            if (!cc.rankings.elo) {
                stepsCompleted++
                s.dispatch(actions.updateInitiationStatus('Generating ELO Ratings', stepsCompleted))
                eloData = await elo.calculateEloRankings(WEEKLY_DATA, TEAMS)
                cc.rankings.elo = true;
                s.dispatch(actions.saveEloRankings(eloData))
            }

            if (!cc.rankings.pyth) {
                s.dispatch(actions.updateInitiationStatus('Generating Pythagorean Rankings', stepsCompleted))
                const pythData = await pyth.calculatePythagoreanRankings(WEEKLY_DATA, TEAMS, CW)
                cc.rankings.pyth = true;
                s.dispatch(actions.savePythRankings(pythData))
            }
            // RANKINGS GENERATION COMPLETED 
            stepsCompleted++

            if (!cc.user.leaderboard) {
                s.dispatch(actions.updateInitiationStatus('Creating User Leaderboard', stepsCompleted))
                const [userLeaderboard, teamsWithData, users] = await userAnalytics.generateLeaderboard(TEAMS, WEEKLY_DATA, CS, CW)
                h2hStats = teamsWithData
                usersWithData = users;
                cc.user.leaderboard = true;
                s.dispatch(actions.saveUserLeaderboard(userLeaderboard))
                s.dispatch(actions.saveH2HStats(h2hStats))
                stepsCompleted++
            }

            // IF LEAGUE IS IN PRESEASON, NONE OF THE DATA NEEDED TO CALCULATE THE BELOW INFO WILL EXIST
            if (STAGE === 0) {
                return
            }
            //RECEIVING NEEDS TO BE GENERATED BEFORE PASSING DATA
            if (!cc.playerStats.playerReceiving) {
                s.dispatch(actions.updateInitiationStatus('Generating Player Receiving Statistics', stepsCompleted))
                // teamRecStats is just data for calculating passing info, formatted is to save in redux store
                // could use refactoring
                var [recStats, teamRecStats, formatedTeamRecStats] = await recAnalytics.calculateRecAnalytics(store.rosterReducer.skillPositions, store.weeklyDataReducer.receivingData[CS], TEAMS)
                cc.playerStats.playerReceiving = true;
                s.dispatch(actions.saveRecStats(recStats))
                s.dispatch(actions.saveTeamRecStats(formatedTeamRecStats))
            }


            if (!cc.playerStats.playerPassing) {
                s.dispatch(actions.updateInitiationStatus('Generating Passing Statistics', stepsCompleted))
                var [passStats, teamPassStats] = await passAnalytics.calculatePassAnalytics(store.rosterReducer.skillPositions, store.weeklyDataReducer.passingData[CS], TEAMS, teamRecStats)
                cc.playerStats.playerPassing = true;
                s.dispatch(actions.savePassStats(passStats))
                s.dispatch(actions.saveTeamPassStats(teamPassStats))
            }
            if (!cc.playerStats.playerRushing) {
                s.dispatch(actions.updateInitiationStatus('Generating Player Rushing Statistics', stepsCompleted))
                var [rushStats, teamRushStats] = await rushAnalytics.calculateRushAnalytics(store.rosterReducer.skillPositions, store.weeklyDataReducer.rushingData[CS], TEAMS)
                cc.playerStats.playerRushing = true;
                s.dispatch(actions.saveRushStats(rushStats))
                s.dispatch(actions.saveTeamRushStats(teamRushStats))

            }

            const allPlayers = {}
            recStats.forEach((player) => {
                allPlayers[player._id] = player
            });

            rushStats.forEach((player) => {
                if (allPlayers[player._id] === undefined) {

                    allPlayers[player._id] = player
                }
                else {
                    allPlayers[player._id].fantasyPoints += player.fantasyPoints
                    allPlayers[player._id].rushYds = player.rushYds
                    allPlayers[player._id].rushTDs = player.rushTDs
                    allPlayers[player._id].rushFum = player.rushFum

                    for (let i = 0; i <= 16; i++) {
                        let prev = typeof allPlayers[player._id].fpArray[i] === 'number' ? allPlayers[player._id].fpArray[i] : 0
                        let next = typeof player.fpArray[i] === 'number' ? player.fpArray[i] : 0
                        allPlayers[player._id].fpArray[i] = prev + next
                    }

                }
            });
            passStats.forEach((player) => {
                if (allPlayers[player._id] === undefined) {

                    allPlayers[player._id] = player
                }
                else {
                    allPlayers[player._id].fantasyPoints += player.fantasyPoints
                    allPlayers[player._id].passYds = player.passYds
                    allPlayers[player._id].passTDs = player.passTDs
                    allPlayers[player._id].passInts = player.passInts
                    for (let i = 0; i <= 16; i++) {
                        let prev = typeof allPlayers[player._id].fpArray[i] === 'number' ? allPlayers[player._id].fpArray[i] : 0
                        let next = typeof player.fpArray[i] === 'number' ? player.fpArray[i] : 0
                        allPlayers[player._id].fpArray[i] = prev + next
                    }
                }
            });

            const allPlayersOrdered = []
            s.dispatch(actions.saveAggregatedFantasyData(allPlayers))
            for (let player in allPlayers) {
                let GP = 0;
                if (allPlayers[player].firstName === "Dion" && allPlayers[player].lastName === "Ricard") {
                    console.log(allPlayers[player])
                }
                allPlayers[player].fpArray.forEach((num) => {
                    if (num && num !== 0 && typeof num !== 'undefined') {
                        GP += 1;
                    }
                })
                GP = GP > 0 ? GP : 1
                GP = GP < 4 ? 4 : GP

                switch (allPlayers[player].position) {
                    case 'QB':
                        allPlayers[player].price = 6000;
                        allPlayers[player].price += 4000 * (allPlayers[player].fantasyPoints / GP) / 25;
                        break;
                    case 'HB':
                        allPlayers[player].price = 4500;
                        allPlayers[player].price += 5000 * (allPlayers[player].fantasyPoints / GP) / 22;
                        break;
                    case 'WR':
                        allPlayers[player].price = 4500;
                        allPlayers[player].price += 5000 * (allPlayers[player].fantasyPoints / GP) / 22;
                        break;
                    case 'TE':
                        allPlayers[player].price = 4000;
                        allPlayers[player].price += 6000 * (allPlayers[player].fantasyPoints / GP) / 24;
                        break;
                }
                if(allPlayers[player].rookie){
                    GP = 'R'
                }
                allPlayers[player].price = Math.round(allPlayers[player].price / 100) * 100
                allPlayersOrdered.push(allPlayers[player])
            }

            allPlayersOrdered.sort((a, b) => {
                return b.price - a.price
            })


            // PLAYER STATISTICS GENERATION COMPLETED 
            stepsCompleted++

            //RECEIVING NEEDS TO BE GENERATED BEFORE PASSING DATA
            if (!cc.teamStats.teamReceiving) {
                // const recStats = await recAnalytics.calculateRecAnalytics(store.leagueReducer.weeklyData, store.leagueReducer.teams)
                cc.teamStats.teamReceiving = true;
                s.dispatch(actions.updateInitiationStatus('Generating Team Receiving Statistics', stepsCompleted))
            }

            if (!cc.teamStats.teamPassing) {
                // const eloData = await elo.calculateEloRankings(store.leagueReducer.weeklyData, store.leagueReducer.teams)
                cc.teamStats.teamPassing = true;
                s.dispatch(actions.updateInitiationStatus('Generating Team Passing Statistics', stepsCompleted))
            }
            if (!cc.teamStats.teamRushing) {
                // const eloData = await elo.calculateEloRankings(store.leagueReducer.weeklyData, store.leagueReducer.teams)
                cc.teamStats.teamRushing = true;
                s.dispatch(actions.updateInitiationStatus('Generating Team Rushing Statistics', stepsCompleted))
            }
            // TEAM STATISTICS GENERATION COMPLETED 
            stepsCompleted++

            if (!cc.fantasy.fantasyRankings) {
                // const eloData = await elo.calculateEloRankings(store.leagueReducer.weeklyData, store.leagueReducer.teams)
                cc.fantasy.fantasyRankings = true;
                s.dispatch(actions.updateInitiationStatus('Calculating Player Fantasy Points', stepsCompleted))
            }

            if (!cc.fantasy.fantasyMetaData) {
                // const eloData = await elo.calculateEloRankings(store.leagueReducer.weeklyData, store.leagueReducer.teams)
                cc.fantasy.fantasyMetaData = true;
                s.dispatch(actions.updateInitiationStatus('Generating Fantasy Metadata', stepsCompleted))
            }

            if (!cc.fantasy.fantasySAD) {
                // const eloData = await elo.calculateEloRankings(store.leagueReducer.weeklyData, store.leagueReducer.teams)
                cc.fantasy.fantasySAD = true;
                s.dispatch(actions.updateInitiationStatus('Finding Fantasy Studs and Duds', stepsCompleted))
            }

            // FANTASY CONTENT GENERATION COMPLETED 
            stepsCompleted++

            // FINALLY, GENERATE CONTENT DEPENDENT ON OTHER GENERATED CONTENT
            if (!cc.matchup.basic) {
                const mt = await matchup.analyzeMatchup(usersWithData, WEEKLY_DATA, h2hStats, eloData, LEAGUE_NAME, CS, CW)
                cc.matchup.basic = true;
                s.dispatch(actions.saveMatchupAnalysis(mt))
                s.dispatch(actions.updateInitiationStatus('Analyzing Matchups...', stepsCompleted))
            }

            // MATCHUP ANALYSIS CONTENT GENERATION COMPLETED 
            stepsCompleted++
            s.dispatch(actions.updateInitiationStatus('Completed!', stepsCompleted))

            /* --------------------------

            NOTHING BELOW HERE COMPLETED YET

            ----------------------------- */



        }




    }
})();

export default cm

