import util from './contentUtilities'

export default {

    async calculateRushAnalytics(players, rushingData, teams, start, end, filter) {

        if (!players || !rushingData) {

            return
        }
        teams = await util.convertTeamsToObject(teams)
        players = await util.convertPlayersToObject(players, teams)
        start = start || 0
        end = end || rushingData.length
        for (let i = start; i < end; i++) {
            if (i === 20) {
                continue;
            }
            if (rushingData[i] === null) {
                continue;
            }
            rushingData[i].playerStats.forEach((stat) => {
                let player = players[stat.rosterId]

                if (!players[stat.rosterId]) {
                    return
                }
                if (player.fpArray === undefined) {
                    player.fpArray = []
                }

                // GENERAL STATS

                player.rushYds = player.rushYds ? player.rushYds + stat.rushYds : stat.rushYds
                player.rushAtt = player.rushAtt ? player.rushAtt + stat.rushAtt : stat.rushAtt
                player.rushTDs = player.rushTDs ? player.rushTDs + stat.rushTDs : stat.rushTDs
                player.rushFum = player.rushFum ? player.rushFum + stat.rushFum : stat.rushFum
                player.rushYdsAfterContact = player.rushYdsAfterContact ? player.rushYdsAfterContact + stat.rushYdsAfterContact : stat.rushYdsAfterContact
                player.rushBrokenTackles = player.rushBrokenTackles ? player.rushBrokenTackles + stat.rushBrokenTackles : stat.rushBrokenTackles
                player.rush20PlusYds = player.rush20PlusYds ? player.rush20PlusYds + stat.rush20PlusYds : stat.rush20PlusYds
                if (i <= 16) {

                    let FP = (stat.rushYds * .1) + (stat.rushTDs * 6) + (stat.rushFum * -2)
                    let otherFP = player.fpArray[i] || 0
                    player.fpArray[i] = otherFP + FP
                    player.fantasyPoints = player.fantasyPoints ? player.fantasyPoints + FP : FP
                }


                // TEAM STATS
                if (!teams[stat.teamId].players) {
                    teams[stat.teamId].players = {}
                }

                if (teams[stat.teamId].players[stat.rosterId]) {
                    let teamPlayer = teams[stat.teamId].players[stat.rosterId]
                    teamPlayer.rushYds = teamPlayer.rushYds ? teamPlayer.rushYds + stat.rushYds : stat.rushYds
                    teamPlayer.rushTDs = teamPlayer.rushTDs ? teamPlayer.rushTDs + stat.rushTDs : stat.rushTDs
                    teamPlayer.rushAtt = teamPlayer.rushAtt ? teamPlayer.rushAtt + stat.rushAtt : stat.rushAtt
                    teamPlayer.rushFum = teamPlayer.rushFum ? teamPlayer.rushFum + stat.rushFum : stat.rushFum
                    teamPlayer.rushYdsAfterContact = teamPlayer.rushYdsAfterContact ? teamPlayer.rushYdsAfterContact + stat.rushYdsAfterContact : stat.rushYdsAfterContact
                    teamPlayer.rushBrokenTackles = teamPlayer.rushBrokenTackles ? teamPlayer.rushBrokenTackles + stat.rushBrokenTackles : stat.rushBrokenTackles
                    teamPlayer.rush20PlusYds = teamPlayer.rush20PlusYds ? teamPlayer.rush20PlusYds + stat.rush20PlusYds : stat.rush20PlusYds


                } else if (!teams[stat.teamId].players[stat.rosterId]) {
                    teams[stat.teamId].players[stat.rosterId] = {
                        firstName: player.firstName,
                        lastName: player.lastName,
                        position: player.position,
                        teamId: stat.teamId,
                        rushYds: stat.rushYds,
                        rushTDs: stat.rushTDs,


                    }
                }



            })
        }

        for (let player in players) {
            let p = players[player]
            // ADVANCED STATS

            p.fumbleRate = (p.rushFum) / p.rushAtt
            p.yac = (p.rushYdsAfterContact) / p.rushAtt
            p.btRate = (p.rushBrokenTackles) / p.rushAtt
            p.ybc = (p.rushYds - p.rushYdsAfterContact) / p.rushAtt
            p.ypc = (p.rushYds) / p.rushAtt
            p.tdRate = (p.rushTDs) / p.rushAtt
            p.bpRate = (p.rush20PlusYds) / p.rushAtt

        }

        for (let team in teams) {
            if (!teams[team].teamStats) {
                teams[team].teamStats = {}
            }
            let ts = teams[team].teamStats
            for (let player in teams[team].players) {
                let p = teams[team].players[player]
                if (p.rushAtt === 0) {
                    continue;
                }

                ts.rushYds = ts.rushYds ? (p.rushYds || 0) + ts.rushYds : p.rushYds
                ts.rushTDs = ts.rushTDs ? (p.rushTDs || 0) + ts.rushTDs : p.rushTDs


                ts.rushAtt = ts.rushAtt ? (p.rushAtt || 0) + ts.rushAtt : p.rushAtt
                ts.rushFum = ts.rushFum ? (p.rushFum || 0) + ts.rushFum : p.rushFum
                ts.rush20PlusYds = ts.rush20PlusYds ? (p.rush20PlusYds || 0) + ts.rush20PlusYds : p.rush20PlusYds
                ts.rushBrokenTackles = ts.rushBrokenTackles ? (p.rushBrokenTackles || 0) + ts.rushBrokenTackles : p.rushBrokenTackles
                ts.rushYdsAfterContact = ts.rushYdsAfterContact ? (p.rushYdsAfterContact || 0) + ts.rushYdsAfterContact : p.rushYdsAfterContact

            }
            ts._id = team
            ts.name = teams[team].name
            ts.fumbleRate = (ts.rushFum) / ts.rushAtt
            ts.yac = (ts.rushYdsAfterContact) / ts.rushAtt
            ts.btRate = (ts.rushBrokenTackles) / ts.rushAtt
            ts.ybc = (ts.rushYds - ts.rushYdsAfterContact) / ts.rushAtt
            ts.ypc = (ts.rushYds) / ts.rushAtt
            ts.tdRate = (ts.rushTDs) / ts.rushAtt
            ts.bpRate = (ts.rush20PlusYds) / ts.rushAtt

        }

        const orderedPlayers = []
        for (let player in players) {

            if (players[player].rushAtt > 0) {
                orderedPlayers.push(players[player])
            }
        }

        orderedPlayers.sort((a, b) => {
            return b.rushYds - a.rushYds
        })

        const orderedTeams = []
        for (let team in teams) {
            orderedTeams.push(teams[team].teamStats)
        }


        orderedTeams.sort((a, b) => {
            return b.rushYds - a.rushYds
        })


        return [orderedPlayers, orderedTeams, players]


    },


}


