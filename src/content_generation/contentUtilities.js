export default {


    convertPlayersToObject(players, teams) {
        const playerObject = {}
        players.forEach(player => {
            playerObject[player._id] = {
                firstName: player.firstName,
                lastName: player.lastName,
                position: player.position,
                fantasyPoints: 0,
                teamName: teams[player.teamId].name,
                teamId: player.teamId,
                _id: player._id

            }
        })
        return playerObject
    },

    convertTeamsToObject(teams) {
        const teamObject = {}
        teams.forEach((team) => {

            teamObject[team.teamId] = {
                name: team.name

            }
        })


        return teamObject
    },
     getOrdinal(n) {
        var s=["th","st","nd","rd"],
        v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
     },

     getPlayoffWeekName(weekNumber){
         switch(weekNumber){

            case 17: return 'Wild Card Round'
            case 18: return 'Divisional Round'
            case 19: return 'Conference Chanpionship'
            case 20: return 'Pro Bowl'
            case 21: return 'Super Bowl'
         }
     }



}


