import util from './contentUtilities'
import teams from './../TeamBreakdown'

export default {

    async analyzeMatchup(USERS, WEEKLY_DATA, h2hStats, elo, LEAGUE_NAME, CS, CW) {

        const SEASON_MATCHUPS = []
WEEKLY_DATA[CS].forEach((week, weekIndex) => {
    if(!week){
        return;
    }
        let WEEKLY_MATCHUP_DATA = week.teamData.map((matchup) => {
            if(matchup.teamID == '0' ){
                return 
            }


           
            const matchupData = {}
            matchupData.teamMatchups ={}
            
            const homeTeam = h2hStats[matchup.teamID]
            matchupData.homeTeamCSWins = h2hStats[matchup.teamID].csWins
            matchupData.homeTeamCSLosses = h2hStats[matchup.teamID].csLosses
            matchupData.homeTeamCSTies = h2hStats[matchup.teamID].csTies
            matchupData.awayTeamCSWins = h2hStats[matchup.opponentTeamID].csWins
            matchupData.awayTeamCSLosses = h2hStats[matchup.opponentTeamID].csLosses
            matchupData.awayTeamCSTies = h2hStats[matchup.opponentTeamID].csTies
            const homeUserName = homeTeam.users[homeTeam.users.length-1].username
            const homeUser = USERS[homeUserName]
            const homeElo = elo[matchup.teamID].elo
            matchupData.homeElo = homeElo[homeElo.length-1]

            const awayTeam = h2hStats[matchup.opponentTeamID]
            const awayUserName = awayTeam.users[awayTeam.users.length-1].username
            const awayUser = USERS[awayUserName]
            const awayElo = elo[matchup.opponentTeamID].elo
            matchupData.awayElo = awayElo[awayElo.length-1]


            if(!homeUser || !awayUser){
                return;
            }

            const lastMatchup = homeUser[homeTeam.name] && homeUser[homeTeam.name].usersPlayedAgainst[awayUserName]
            const teamMatchups = homeTeam.teamsPlayedAgainst[awayTeam.name]


            matchupData.homeUserName = homeUserName
            matchupData.homeTeamName = homeTeam.name

            matchupData.awayUserName = awayUserName
            matchupData.awayTeamName = awayTeam.name
            

            matchupData.homeTeam = USERS[homeUserName][homeTeam.name]

            matchupData.awayTeam = USERS[awayUserName][awayTeam.name]

            // IF USERS HAVE PLAYED BEFORE
            if(lastMatchup){
                const lm = lastMatchup[lastMatchup.length -1]
                matchupData.userMatchups = lastMatchup
                matchupData.userMatchupCount = lastMatchup.length

                

                matchupData.avgHomeUserScore = (lastMatchup.reduce((total, a) => {
                    return total + a.pointsScored
                }, 0) / lastMatchup.length)
         
                matchupData.avgAwayUserScore = (lastMatchup.reduce((total, a) => {
                    return total + a.pointsAllowed
                }, 0) / lastMatchup.length)
                matchupData.firstMatchup = false
                    
            } else {
                matchupData.firstMatchup = true
            }

            if(teamMatchups){

                matchupData.avgHomeTeamScore = (teamMatchups.reduce((total, a) => {
                    return total + a.pointsScored
                }, 0) / teamMatchups.length)
                
                matchupData.avgAwayTeamScore = (teamMatchups.reduce((total, a) => {
                    return total + a.pointsAllowed
                }, 0) / teamMatchups.length)
                
            }
            matchupData.teamMatchups = teamMatchups



            
            
            return matchupData

        })
        
        WEEKLY_MATCHUP_DATA = this.formatMatchupAnalysis(WEEKLY_MATCHUP_DATA, LEAGUE_NAME, CS, weekIndex)

        SEASON_MATCHUPS.push (WEEKLY_MATCHUP_DATA)
})
        return SEASON_MATCHUPS
    },

    formatMatchupAnalysis(MATCHUP_DATA, LEAGUE_NAME, CS, CW){
        

        MATCHUP_DATA.forEach((m) => {
           
            if(!m){
                return
            }
            
            // IF SAME DIVISION
            const homeLoc = teams[m.homeTeamName]
            const awayLoc = teams[m.awayTeamName]
try{
            if(homeLoc[0] === awayLoc[0] && homeLoc[1] === awayLoc[1]){
                m.matchupType = 'divisional'
            } else if (homeLoc[0] === awayLoc[0]){
                m.matchupType = 'conference'
                
            } else {
                m.matchupType = 'cross'
                
            }
         } catch(e){}

            let FIRST_OVERALL_MATCHUP = false;
            if(m.teamMatchups === undefined || m.teamMatchups.length === 0){
                FIRST_OVERALL_MATCHUP = true
            }

            if(m.firstMatchup && FIRST_OVERALL_MATCHUP){
                m.gamesPlayed = languageElements.firstEverMatchup(m.homeUserName, m.homeTeamName, m.awayUserName, m.awayTeamName, LEAGUE_NAME, m, CS)
            } else if(m.firstMatchup && !FIRST_OVERALL_MATCHUP){
                m.gamesPlayed = languageElements.firstUserMatchup(m.homeUserName, m.homeTeamName, m.awayUserName, m.awayTeamName, LEAGUE_NAME, m, CS)
            
                
            } else {
                m.gamesPlayed = languageElements.multiMatchup(m.homeUserName, m.homeTeamName, m.awayUserName, m.awayTeamName, LEAGUE_NAME, m.userMatchupCount, CS, m.userMatchups)
            }

            m.recordText = languageElements.teamRecord(m.homeUserName, m.homeTeamName, m.awayUserName, m.awayTeamName, LEAGUE_NAME, m, CS)
            

            m.spread = this.calculatePredictedWinner(m)            
        })
        

        return MATCHUP_DATA


    }   ,

    calculatePredictedWinner(matchup){

        let HOMESPREAD = 2 // START AT TWO TO ACCOUNT FOR HOMEFIELD

        const ELOSPREAD = (matchup.homeElo - matchup.awayElo) / 25
        const TOTAL_GAMES_PLAYED = matchup.homeTeam.gamesPlayed + matchup.awayTeam.gamesPlayed
        const HOME_GAMES_PLAYED_BONUS = 3 * (matchup.homeTeam.gamesPlayed / TOTAL_GAMES_PLAYED)
        const AWAY_GAMES_PLAYED_BONUS = 3 * (matchup.awayTeam.gamesPlayed / TOTAL_GAMES_PLAYED)
        const AVG_HOME_SPREAD = ((matchup.homeTeam.pointsScored / matchup.homeTeam.gamesPlayed) - (matchup.homeTeam.pointsAllowed  / matchup.homeTeam.gamesPlayed)) /2
        const AVG_AWAY_SPREAD = ((matchup.awayTeam.pointsScored / matchup.awayTeam.gamesPlayed) - (matchup.awayTeam.pointsAllowed  / matchup.awayTeam.gamesPlayed)) /2
        
        const HOME_USER_RECORD_BONUS = 10 * (((matchup.homeTeam.wins || 0) / ((matchup.homeTeam.wins || 0) + (matchup.homeTeam.losses || 0) + (matchup.homeTeam.ties || 0))) - ((matchup.awayTeam.wins || 0) / ((matchup.awayTeam.wins || 0) + (matchup.awayTeam.losses || 0) + (matchup.awayTeam.ties || 0))))
        let TEAMSPREAD
        
        if(matchup.firstMatchup && matchup.teamMatchups){
            TEAMSPREAD = matchup.avgHomeTeamScore - matchup.avgAwayTeamScore
            
            HOMESPREAD = HOMESPREAD + (.21 *ELOSPREAD) + (.21 * TEAMSPREAD) + (.16 * (HOME_GAMES_PLAYED_BONUS - AWAY_GAMES_PLAYED_BONUS)) + (.21 * AVG_HOME_SPREAD - AVG_AWAY_SPREAD) + (HOME_USER_RECORD_BONUS * .21)
            
            
        } else if (matchup.teamMatchups){
            
            const spreadFactor  =  matchup.userMatchupCount / matchup.teamMatchups.length 
            TEAMSPREAD = matchup.avgHomeTeamScore - matchup.avgAwayTeamScore
            
            let USERSPREAD = matchup.avgHomeUserScore - matchup.avgAwayUserScore
            if(spreadFactor === 0){

                HOMESPREAD = HOMESPREAD + (.2 *ELOSPREAD) +  (.2*(TEAMSPREAD * (1-spreadFactor))) + (.2*(HOME_GAMES_PLAYED_BONUS - AWAY_GAMES_PLAYED_BONUS)) + (.2 * AVG_HOME_SPREAD - AVG_AWAY_SPREAD) + (HOME_USER_RECORD_BONUS * .2)
            } else if (spreadFactor === 1){
                HOMESPREAD = HOMESPREAD + (.2 *ELOSPREAD) + (.2*(USERSPREAD * spreadFactor)) +  (.2*(HOME_GAMES_PLAYED_BONUS - AWAY_GAMES_PLAYED_BONUS)) + (.2 * AVG_HOME_SPREAD - AVG_AWAY_SPREAD) + (HOME_USER_RECORD_BONUS * .2)

            }else {
                HOMESPREAD = HOMESPREAD + (.2 *ELOSPREAD) + (.1*(USERSPREAD * spreadFactor)) + (.1*(TEAMSPREAD * (1-spreadFactor))) + (.2*(HOME_GAMES_PLAYED_BONUS - AWAY_GAMES_PLAYED_BONUS)) + (.2 * AVG_HOME_SPREAD - AVG_AWAY_SPREAD) + (HOME_USER_RECORD_BONUS * .2)

            }

        } else {
            HOMESPREAD = HOMESPREAD + (.26*ELOSPREAD) + (.22 *(HOME_GAMES_PLAYED_BONUS - AWAY_GAMES_PLAYED_BONUS)) + (.26 * AVG_HOME_SPREAD - AVG_AWAY_SPREAD) + (HOME_USER_RECORD_BONUS * .26)

        }

        return HOMESPREAD
        


    }

}




const languageElements = {

    firstEverMatchup: ((hUser, hTeam, aUser, aTeam, LN, matchup)  => {
        const text = 
        [`This is the first matchup between both ${hUser} and ${aUser}, and the ${hTeam} and the ${aTeam}. `, 
        `${hUser} and the ${hTeam} have yet to face off against the ${aUser} led ${aTeam} in ${LN}. `,
         `Not only is this the first meeting of ${hUser} and ${aUser}, it is also the first of the ${hTeam} and the ${aTeam}. `]
        let random = Math.floor(Math.random() * text.length)
        return text[random]
    }),

    firstUserMatchup: ((hUser, hTeam, aUser, aTeam, LN, matchup, CS)  => {
        const mCount = matchup.teamMatchups.length
        const lm = matchup.teamMatchups[matchup.teamMatchups.length-1]
        const lmMargin = Math.abs(lm.pointsScored - lm.pointsAllowed)
        const winner = lm.pointsScored > lm.pointsAllowed ? hTeam : aTeam
        const loser = lm.pointsScored < lm.pointsAllowed ? hTeam : aTeam

        const ordinal = util.getOrdinal(mCount +1)
        const plural = mCount > 1 ? 's' : '' 
        const text = 
        [`Although ${hUser} and ${aUser} have yet to play each other, this is the ${ordinal} matchup between the ${hTeam} and the ${aTeam}. In that last matchup the ${winner} beat the ${loser} by ${lmMargin} points.
        `, `The ${hTeam} and the ${aTeam} have faced each other ${ordinal} times in ${LN}, but this will be the first ever game between ${hUser} and ${aUser}.`]
        let random = Math.floor(Math.random() * text.length)

       let marginText = '';
        if(lmMargin <= 5) {
         marginText = [`${lmMargin} points is a razor thin margin, so this game could go either way. `,
        `The ${loser} are hoping they catch a few breaks this game and can come away with a win. `]
        } else if(lmMargin <= 14){
        marginText = [`${lmMargin} points is a pretty respectable margin of victory for the ${winner}. The ${loser} are going to have a challenge ahead of themselves if they want to pull out a victory. `,
        ]
        }else {
         marginText = [`In that game, the ${winner} blew out the ${loser} by ${lmMargin} points. The ${loser} are going to need to pull out all the stops if they expect to come away with a victory. `,
        ]
        }
      

        let random3 = Math.floor(Math.random() * marginText.length)
         
        return text[random] +  marginText[random3]
    }),

    multiMatchup: ((hUser, hTeam, aUser, aTeam, LN, mCount, CS, userMatchups)  => {
        const lm = userMatchups[userMatchups.length-1]
        const lmMargin = Math.abs(lm.pointsScored - lm.pointsAllowed)
        const winner = lm.pointsScored > lm.pointsAllowed ? hTeam : aTeam
        const loser = lm.pointsScored < lm.pointsAllowed ? hTeam : aTeam

        const ordinal = util.getOrdinal(mCount +1)
        const plural = mCount > 1 ? 's' : '' 
        const text = 
        [`This is the ${ordinal} matchup between ${hUser} and ${aUser}. `, 
        `${hUser} and ${aUser} have already played each other ${mCount} time${plural} in ${LN}. `,
         `${CS} seasons in and this is the ${ordinal} meeting between ${hUser} and ${aUser}. `]
        let random = Math.floor(Math.random() * text.length)

        let text2 = [`The last time these teams met the ${winner} beat the ${loser} by ${lmMargin} points. `,
        `In the last matchup the ${winner} walked away with a ${lmMargin} point victory. `
      ]
        let random2 = Math.floor(Math.random() * text2.length)

        let smallMarginText = [`${lmMargin} points is a razor thin margin, so this game could go either way. `,
        `The ${loser} are hoping they catch a few breaks this game and can come away with a win. `]

      

        let random3 = Math.floor(Math.random() * smallMarginText.length)
         
        return text[random] + text2[random2] + smallMarginText[random3]
    }),

    teamRecord: ((hUser, hTeam, aUser, aTeam, LN, matchup) => {

        const hTeamGamesPlayed = (matchup.homeTeamCSWins ||0) + (matchup.homeTeamCSLosses || 0)+ (matchup.homeTeamCSTies ||0)
        const hTeamGoodRecord = matchup.homeTeamCSWins / hTeamGamesPlayed >= .666
        const hTeamRecord = `${matchup.homeTeamCSWins || 0}-${matchup.homeTeamCSLosses || 0}`
        if(matchup.homeTeamCSTies){
            hTeamRecord + `-${matchup.homeTeamCSTies || 0}`
        }

        const aTeamGamesPlayed = (matchup.awayTeamCSWins ||0) + (matchup.awayTeamCSLosses || 0)+ (matchup.awayTeamCSTies ||0)
        const aTeamGoodRecord = matchup.awayTeamCSWins / aTeamGamesPlayed >= .666
        const aTeamRecord = `${matchup.awayTeamCSWins || 0}-${matchup.awayTeamCSLosses || 0}`
        if(matchup.awayTeamCSTies){
            aTeamRecord + `-${matchup.awayTeamCSTies || 0}`
        }
        


        let text1, text2;
        if(hTeamGoodRecord && aTeamGoodRecord){
             text1 = [`The ${hTeam} and the ${aTeam} both have solid records, sitting at ${hTeamRecord} and ${aTeamRecord} respectively. This is an important matchup nevertheless because it could have some impact on playoff seeding. `]
        }
        else if(hTeamGoodRecord && !aTeamGoodRecord){
            text1 = [`The ${hTeam} are happy with their ${hTeamRecord} record but will look to capitalize on this favorable matchup, the ${aTeamRecord} ${aTeam} are having a rough season and are vulnerable. `]
       }
       else if(!hTeamGoodRecord && aTeamGoodRecord){
        text1 = [`The ${aTeam} are sitting pretty with a ${aTeamRecord} record, whereas the ${hTeam} are starting to get desperate for a win . `]
   } else{
    text1 = [`The ${hTeam} are having a rough season, sitting at ${hTeamRecord}, but the ${aTeam} and aren't doing much better with their ${aTeamRecord} record. Both of these squads could really use a win. `]
       
   }
   let random = Math.floor(Math.random() * text1.length)
   return text1[random]


    }),

    divisionalMatchup: ((hUser = '', aUser= '', LN, mCount, CS)  => {
        const text = 
        [`This is the DIVISIONAL matchup between ${hUser} and ${aUser}.`, 
       ]
        let random = Math.floor(Math.random() * text.length)
        return text[random]
    }),
        
}
