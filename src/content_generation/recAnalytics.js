import util from './contentUtilities'

export default {

    async calculateRecAnalytics(players, receivingData, teams, start, end) {
        if (!players || !receivingData) {

            return
        }


        teams = await util.convertTeamsToObject(teams)

        players = await util.convertPlayersToObject(players, teams)
        start = start || 0
        end = end || receivingData.length
        for (let i = start; i < end; i++) {
            if (i === 20) {
                continue;
            }

            if (receivingData[i] === null) {
                continue;
            }
            receivingData[i].playerStats.forEach((stat) => {
                let player = players[stat.rosterId]
                if (!players[stat.rosterId]) {
                    return
                }
                if (player.fpArray === undefined) {
                    player.fpArray = []
                }


                // GENERAL STATS
                player.recYds = player.recYds ? player.recYds + stat.recYds : stat.recYds
                player.recTDs = player.recTDs ? player.recTDs + stat.recTDs : stat.recTDs
                player.recCatches = player.recCatches ? player.recCatches + stat.recCatches : stat.recCatches
                player.recYdsAfterCatch = player.recYdsAfterCatch ? player.recYdsAfterCatch + stat.recYdsAfterCatch : stat.recYdsAfterCatch
                player.recDrops = player.recDrops ? player.recDrops + stat.recDrops : stat.recDrops
                if (i <= 16) {

                    let FP = (stat.recYds * .1) + (stat.recTDs * 6) + stat.recCatches
                    let otherFP = player.fpArray[i] || 0
                    player.fpArray[i] = otherFP + FP
                    player.fantasyPoints = player.fantasyPoints ? player.fantasyPoints + FP : FP
                }


                // TEAM STATS
                if (!teams[stat.teamId].players) {
                    teams[stat.teamId].players = {}
                    teams[stat.teamId].dropArray = []
                }

                teams[stat.teamId].dropArray[i] = teams[stat.teamId].dropArray[i] ? teams[stat.teamId].dropArray[i] + stat.recDrops : stat.recDrops

                if (teams[stat.teamId].players[stat.rosterId]) {
                    let teamPlayer = teams[stat.teamId].players[stat.rosterId]
                    teamPlayer.recYds = teamPlayer.recYds ? teamPlayer.recYds + stat.recYds : stat.recYds
                    teamPlayer.recTDs = teamPlayer.recTDs ? teamPlayer.recTDs + stat.recTDs : stat.recTDs
                    teamPlayer.recCatches = teamPlayer.recCatches ? teamPlayer.recCatches + stat.recCatches : stat.recCatches
                    teamPlayer.recYdsAfterCatch = teamPlayer.recYdsAfterCatch ? teamPlayer.recYdsAfterCatch + stat.recYdsAfterCatch : stat.recYdsAfterCatch
                    teamPlayer.recDrops = teamPlayer.recDrops ? teamPlayer.recDrops + stat.recDrops : stat.recDrops

                } else if (!teams[stat.teamId].players[stat.rosterId]) {
                    teams[stat.teamId].players[stat.rosterId] = {
                        firstName: player.firstName,
                        lastName: player.lastName,
                        position: player.position,
                        teamId: stat.teamId,
                        recYds: stat.recYds,
                        recTDs: stat.recTDs,
                        recCatches: stat.recCatches,
                        recYdsAfterCatch: stat.recYdsAfterCatch,
                        recDrops: stat.recDrops,

                    }
                }

            })
        }

        for (let player in players) {
            let p = players[player]
            // ADVANCED STATS

            p.dropRate = p.recDrops / (p.recCatches + p.recDrops)
            p.depthOfRec = (p.recYds - p.recYdsAfterCatch) / p.recCatches
            p.yac = (p.recYdsAfterCatch) / p.recCatches
            p.ypc = (p.recYds) / p.recCatches
            p.tdRate = (p.recTDs) / p.recCatches

        }

        for (let team in teams) {
            if (!teams[team].teamStats) {
                teams[team].teamStats = {}
            }
            let ts = teams[team].teamStats
            for (let player in teams[team].players) {
                let p = teams[team].players[player]

                ts.recDrops = ts.recDrops ? p.recDrops + ts.recDrops : p.recDrops
                ts.recYds = ts.recYds ? p.recYds + ts.recYds : p.recYds
                ts.recTDs = ts.recTDs ? p.recTDs + ts.recTDs : p.recTDs
                ts.recCatches = ts.recCatches ? p.recCatches + ts.recCatches : p.recCatches
                ts.recYdsAfterCatch = ts.recYdsAfterCatch ? p.recYdsAfterCatch + ts.recYdsAfterCatch : p.recYdsAfterCatch

            }
            ts._id = team
            ts.name = teams[team].name
            ts.dropRate = ts.recDrops / (ts.recCatches + ts.recDrops)
            ts.depthOfRec = (ts.recYds - ts.recYdsAfterCatch) / ts.recCatches
            ts.yac = (ts.recYdsAfterCatch) / ts.recCatches
            ts.ypc = (ts.recYds) / ts.recCatches
            ts.tdRate = (ts.recTDs) / ts.recCatches

        }


        const orderedPlayers = []
        for (let player in players) {

            if (players[player].recCatches > 0) {
                orderedPlayers.push(players[player])
            }
        }

        orderedPlayers.sort((a, b) => {
            return b.recYds - a.recYds
        })


        const orderedTeams = []
        for (let team in teams) {
            orderedTeams.push(teams[team].teamStats)
        }


        orderedTeams.sort((a, b) => {
            return b.recYds - a.recYds
        })


        return [orderedPlayers, teams, orderedTeams, players]

    }


}


