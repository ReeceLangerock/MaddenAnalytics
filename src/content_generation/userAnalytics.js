
export default {

    async generateLeaderboard(teams, weeklyData, CS, CW) {
        let [leaderboard, teamsWithData, usersWithData] = this.getUsers(teams, weeklyData, CS, CW)
        return [leaderboard, teamsWithData, usersWithData];
    },


    getUsers(teams, weeklyData, CS, CW) {

        const userObject = {

        }
        teams.forEach(team => {
            team.users.forEach(user => {
                if (user.username === '') {
                    return null
                }
                const weeksAsOwner = ((user.lastSeen[0] * 21) + user.lastSeen[1]) - ((user.firstSeen[0] * 21) + user.firstSeen[1])
                let weeksInLeague = 0;
                let gamesPlayed = 0;
                if (userObject[user.username]) {
                    if (userObject[user.username].weeksInLeague) {
                        weeksInLeague = weeksAsOwner + userObject[user.username].weeksInLeague

                    } else {
                        weeksInLeague = weeksAsOwner

                    }


                } else {
                    weeksInLeague = weeksAsOwner
                }

                userObject[user.username] = {
                    ...userObject[user.username],
                    weeksInLeague: weeksInLeague,
                    user: user.username,
                  

                    [team.name]: {

                        ...user,
                        weeksAsOwner: weeksAsOwner,
                        usersPlayedAgainst: {}
                    }

                }

            })


        })
        if (Object.keys(userObject).length === 0 && userObject.constructor === Object) {
            return
        }
        const orderedUsers = []
        for (let user in userObject) {


            orderedUsers.push(userObject[user])

        }
        orderedUsers.sort((a, b) => {

            return b.weeksInLeague - a.weeksInLeague
        })

        let teamsWithData = this.calculateRecord(userObject, weeklyData, teams, CS, CW)
        return [orderedUsers, teamsWithData, userObject]


    },
    calculateRecord(users, weeklyData, teams, CS, CW) {
        teams = this.convertTeamsToObject(teams)
        let seasonIndex = 0
        let weekIndex = 0
        if(!weeklyData){
            return
        }
        weeklyData.forEach((season) => {
            weekIndex = 0;
            if(!season){
                return;
            }
            season.forEach((week) => {
                if (week === null) {
                    return
                }
                week.teamData.forEach((game) => {


                    if (game.teamID === '0' || !game.teamID) {
                        return
                    }
                    if (CS === seasonIndex && weekIndex >= CW) {
                        return null;
                    }


                    var awayTeam = teams[game.opponentTeamID]
                    var homeTeam = teams[game.teamID]

                    var awayUser = this.findUser(awayTeam.users, seasonIndex, weekIndex)
                    var homeUser = this.findUser(homeTeam.users, seasonIndex, weekIndex)
                  
                    var htWeekInfo = {
                        ...game,
                        season: seasonIndex,
                        week: weekIndex,
                        opponent: awayUser,
                    }

                    var atWeekInfo = {
                       
                        teamID: game.opponentTeamID,
                        opponentTeamID: game.teamID,
                        pointsScored: game.pointsAllowed,
                        pointsAllowed: game.pointsScored,
                        season: seasonIndex,
                        week: weekIndex,
                        opponent: homeUser,
                    }

                  
           
                    if(teams[game.teamID].teamsPlayedAgainst[awayTeam.name]) {
                        teams[game.teamID].teamsPlayedAgainst[awayTeam.name].push(htWeekInfo)
                    } else {
                        teams[game.teamID].teamsPlayedAgainst[awayTeam.name] = [htWeekInfo]
                        
                    }

                    if(teams[game.teamID].usersPlayedAgainst[awayUser]) {
                        teams[game.teamID].usersPlayedAgainst[awayUser].push(htWeekInfo)
                    } else {
                        teams[game.teamID].usersPlayedAgainst[awayUser] = [htWeekInfo]
                        
                    }

                    if(teams[game.opponentTeamID].teamsPlayedAgainst[homeTeam.name]) {
                        teams[game.opponentTeamID].teamsPlayedAgainst[homeTeam.name].push(atWeekInfo)
                    } else {
                        teams[game.opponentTeamID].teamsPlayedAgainst[homeTeam.name] = [atWeekInfo]
                        
                    }     
                    
                    if(teams[game.opponentTeamID].usersPlayedAgainst[homeUser]) {
                        teams[game.opponentTeamID].usersPlayedAgainst[homeUser].push(atWeekInfo)
                    } else {
                        teams[game.opponentTeamID].usersPlayedAgainst[homeUser] = [atWeekInfo]
                        
                    }     
                    
                    
                    if (game.pointsScored > game.pointsAllowed) {
                        teams[game.teamID].wins++
                        teams[game.opponentTeamID].losses++
                        if(CS === seasonIndex){
                        teams[game.teamID].csWins++
                        teams[game.opponentTeamID].csLosses++
                            
                        }

                    } else if (game.pointsScored < game.pointsAllowed) {
                        teams[game.teamID].losses++
                        teams[game.opponentTeamID].wins++
                        if(CS === seasonIndex){
                            teams[game.opponentTeamID].csWins++
                            teams[game.teamID].csLosses++
                                
                            }

                    } else {

                        teams[game.teamID].ties++
                        teams[game.opponentTeamID].ties++
                        if(CS === seasonIndex){
                            teams[game.opponentTeamID].csTies++
                            teams[game.teamID].csTies++
                                
                            }
                    }
                    
                    

                    teams[game.teamID].pointsScored += game.pointsScored
                    teams[game.teamID].pointsAllowed += game.pointsAllowed
                    teams[game.opponentTeamID].pointsScored += game.pointsAllowed
                    teams[game.opponentTeamID].pointsAllowed += game.pointsScored


                 

                    
                    if(users[homeUser]){
                        if(users[homeUser][homeTeam.name]){
                            
                            const user = users[homeUser][homeTeam.name]
                           
                            user.pointsScored = user.pointsScored ? user.pointsScored + game.pointsScored : game.pointsScored
                            user.pointsAllowed = user.pointsAllowed ? user.pointsAllowed + game.pointsAllowed : game.pointsAllowed
                            if(!user.usersPlayedAgainst){
                                user.usersPlayedAgainst = {}
                            }
                            if(user.usersPlayedAgainst[awayUser]) {
                                user.usersPlayedAgainst[awayUser].push(htWeekInfo)
                            } else {
                                user.usersPlayedAgainst[awayUser] = [htWeekInfo]
                                
                            }


                            if (game.pointsScored > game.pointsAllowed) {
                                user.wins = user.wins ? user.wins +1 : 1
                                if(CS === seasonIndex){
                                    user.csWins = user.csWins ? user.csWins +1 : 1
                                }
                                if(weekIndex >= 17){
                                user.playoffWins = user.playoffWins ? user.playoffWins +1 : 1
                                    
                                }
        
                            } else if (game.pointsScored < game.pointsAllowed) {
                                user.losses = user.losses ? user.losses +1 : 1
                                if(CS === seasonIndex){
                                    user.csLosses = user.csLosses ? user.csLosses +1 : 1
                                }
                                if(weekIndex >= 17){
                                    user.playoffLosses = user.playoffLosses ? user.playoffLosses +1 : 1
                                        
                                    }
        
                            } else {
                                user.ties = user.ties ? user.ties +1 : 1
                                if(CS === seasonIndex){
                                    user.csTies = user.csTies ? user.csTies +1 : 1
                                }
                                
                            
                            }

                            user.gamesPlayed = user.gamesPlayed ? user.gamesPlayed +1 : 1
                           

                        }
                    }

                    if(users[awayUser]){
                        if(users[awayUser][awayTeam.name]){
                            let user = users[awayUser][awayTeam.name]

                           
                           
                            user.pointsScored = user.pointsScored ? user.pointsScored + game.pointsAllowed : game.pointsAllowed
                            user.pointsAllowed = user.pointsAllowed ? user.pointsAllowed + game.pointsScored : game.pointsScored
                            if(!user.usersPlayedAgainst){
                                user.usersPlayedAgainst = {}
                            }
                            if(user.usersPlayedAgainst[homeUser]) {
                                user.usersPlayedAgainst[homeUser].push(atWeekInfo)
                            } else {
                                user.usersPlayedAgainst[homeUser] = [atWeekInfo]
                                
                                
                            }


                            if (game.pointsScored > game.pointsAllowed) {
                                user.losses = user.losses ? user.losses + 1 : 1
                                
                                if(CS === seasonIndex){
                                    user.csLosses = user.csLosses ? user.csLosses +1 : 1
                                }
                                    if(weekIndex >= 17){
                                        user.playoffLosses = user.playoffLosses ? user.playoffLosses +1 : 1
                                            
                                        }
        
                            } else if (game.pointsScored < game.pointsAllowed) {
                                user.wins = user.wins ? user.wins +1 : 1
                                if(CS === seasonIndex){
                                    user.csWins = user.csWins ? user.csWins +1 : 1
                                }
                                if(weekIndex >= 17){
                                    user.playoffWins = user.playoffWins ? user.playoffWins +1 : 1
                                        
                                    }
                            } else {
                                user.ties = user.ties ? user.ties +1 : 1
                                if(CS === seasonIndex){
                                    user.csTies = user.csTies ? user.csTies +1 : 1
                                }
                            }

                            user.gamesPlayed = user.gamesPlayed ? user.gamesPlayed +1 : 1
                            

                            

                        }
                    }




                })
                weekIndex++

            })
            seasonIndex = seasonIndex + 1
        })
        return  teams



    }
    , findUser(users, season, week){

        let foundUser = undefined
        
        users.forEach((user) => {
            const start = (user.firstSeen[0] * 21) + user.firstSeen[1]
            const end =  ((user.lastSeen[0] * 21) + user.lastSeen[1])
            const gametime = (season * 21) + week

            if(gametime >= start && gametime <= end){
                
                foundUser = user;
            } 
        })
        if(foundUser){
            return foundUser.username
            
        } else {
            return 'CPU'
        }
    }, convertTeamsToObject(teams) {
        const teamObject = {}
        teams.forEach(team => {
            teamObject[team.teamId] = {
                name: team.name,
                abbreviation: team.abbreviation,
                wins: 0,
                csWins: 0,
                losses: 0,
                csLosses: 0,
                ties: 0,
                csTies: 0,
                pointsAllowed: 0,
                pointsScored: 0,
                users: team.users,
                teamsPlayedAgainst: {

                },
                usersPlayedAgainst: {}
            }
        })

        return teamObject
    }
}