// ADD YARDS AT RECEPTION FROM WR STATS FOR QB

import util from './contentUtilities'

export default {

    async calculatePassAnalytics(players, passingData, teams, teamRecStats, start, end) {
        if (!players || !passingData) {

            return
        }


        teams = await util.convertTeamsToObject(teams)
        players = await util.convertPlayersToObject(players, teams)

        start = start || 0
        end = end || passingData.length
        for (let i = start; i < end; i++) {
            if (i === 20) {
                continue;
            }
            if (passingData[i] === null) {
                continue;
            }
            passingData[i].playerStats.forEach((stat) => {
                let player = players[stat.rosterId]

                if (!players[stat.rosterId]) {
                    return
                }
                if (!player.attArray) {
                    player.attArray = []
                }
                if (!player.fpArray) {
                    player.fpArray = []
                }

                //GENERAL STATS
                player.passYds = player.passYds ? player.passYds + stat.passYds : stat.passYds
                player.passTDs = player.passTDs ? player.passTDs + stat.passTDs : stat.passTDs
                player.passAtt = player.passAtt ? player.passAtt + stat.passAtt : stat.passAtt
                player.passComp = player.passComp ? player.passComp + stat.passComp : stat.passComp
                player.passInts = player.passInts ? player.passInts + stat.passInts : stat.passInts
                player.passSacks = player.passSacks ? player.passSacks + stat.passSacks : stat.passSacks

                player.attArray[i] = player.attArray[i] ? player.attArray[i] + stat.passAtt : stat.passAtt
                if (i <= 16) {

                    let FP = (stat.passYds * .04) + (stat.passTDs * 4) + (stat.passInts * -2)
                    let otherFP = player.fpArray[i] || 0
                    player.fpArray[i] = otherFP + FP
                    player.fantasyPoints = player.fantasyPoints ? player.fantasyPoints + FP : FP
                }
                // TEAM STATS
                if (!teams[stat.teamId].players) {
                    teams[stat.teamId].players = {}
                    teams[stat.teamId].compArray = []
                    teams[stat.teamId].attArray = []

                }
                teams[stat.teamId].compArray[i] = teams[stat.teamId].compArray[i] ? teams[stat.teamId].compArray[i] + stat.passComp : stat.passComp
                teams[stat.teamId].attArray[i] = teams[stat.teamId].attArray[i] ? teams[stat.teamId].attArray[i] + stat.passAtt : stat.passAtt
                if (teams[stat.teamId].players[stat.rosterId]) {

                    let teamPlayer = teams[stat.teamId].players[stat.rosterId]
                    teamPlayer.passYds = teamPlayer.passYds ? teamPlayer.passYds + stat.passYds : stat.passYds
                    teamPlayer.passTDs = teamPlayer.passTDs ? teamPlayer.passTDs + stat.passTDs : stat.passTDs
                    teamPlayer.passAtt = teamPlayer.passAtt ? teamPlayer.passAtt + stat.passAtt : stat.passAtt
                    teamPlayer.passAttArray[i] = stat.passAtt
                    teamPlayer.passComp = teamPlayer.passComp ? teamPlayer.passComp + stat.passComp : stat.passComp

                    teamPlayer.passInts = teamPlayer.passInts ? teamPlayer.passInts + stat.passInts : stat.passInts
                    teamPlayer.passSacks = teamPlayer.passSacks ? teamPlayer.passSacks + stat.passSacks : stat.passSacks

                } else if (!teams[stat.teamId].players[stat.rosterId]) {
                    teams[stat.teamId].players[stat.rosterId] = {
                        firstName: player.firstName,
                        lastName: player.lastName,
                        position: player.position,
                        teamId: stat.teamId,
                        passYds: stat.passYds,
                        passAtt: stat.passAtt,
                        passInts: stat.passInts,
                        passTDs: stat.passTDs,
                        passSacks: stat.passSacks,
                        passAttArray: [stat.passAtt],

                        passComp: stat.passComp,


                    }
                }


            })

        }


        for (let player in players) {

            let p = players[player]

            if (p.passAtt === 0 || !p.passAtt) {
                delete players[player]
                continue
            }

            // ADVANCED STATS
            p.compPercentage = p.passComp / p.passAtt
            p.intRate = p.passInts / p.passAtt
            p.sackRate = p.passSacks / (p.passSacks + p.passAtt)
            p.tdRate = p.passTDs / p.passAtt
            p.ypa = p.passYds / p.passAtt
            p.ypc = p.passYds / p.passComp
            let a = ((p.passComp / p.passAtt) - .3) * 5
            let b = ((p.passYds / p.passAtt) - 3) * .25
            let c = ((p.passTDs / p.passAtt)) * 20
            let d = 2.375 - ((p.passInts / p.passAtt) * 25)

            a = a > 2.375 ? 2.375 : a
            b = b > 2.375 ? 2.375 : b
            c = c > 2.375 ? 2.375 : c
            d = d > 2.375 ? 2.375 : d

            a = a < 0 ? 0 : a
            b = b < 0 ? 0 : b
            c = c < 0 ? 0 : c
            d = d < 0 ? 0 : d

            p.qbr = ((a + b + c + d) / 6) * 100

            p.ncaa = ((8.4 * p.passYds) + (330 * p.passTDs) + (100 * p.passComp) - (200 * p.passInts)) / p.passAtt


            let assumedDrops = 0;
            for (let i = start; i < end; i++) {
                if (i === 20) {
                    continue;
                }
                if (teams[p.teamId].attArray && p.attArray[i]) {
                    let teamAtt = teams[p.teamId].attArray[i] || 0
                    let playerAtt = p.attArray[i] || 0

                    let weeklyPassPercent = playerAtt / teamAtt
                    assumedDrops += weeklyPassPercent * teamRecStats[p.teamId].dropArray[i]

                }


            }

            p.assumedDrops = assumedDrops

            p.adjCompPercentage = (p.passComp + assumedDrops) / p.passAtt
        }



        const sum = {}
        for (let team in teams) {
            if (!teams[team].teamStats) {
                teams[team].teamStats = {}
            }
            let ts = teams[team].teamStats
            for (let player in teams[team].players) {
                let p = teams[team].players[player]

                ts.passYds = ts.passYds ? p.passYds + ts.passYds : p.passYds
                ts.passAtt = ts.passAtt ? p.passAtt + ts.passAtt : p.passAtt
                ts.passInts = ts.passInts ? p.passInts + ts.passInts : p.passInts
                ts.passSacks = ts.passSacks ? p.passSacks + ts.passSacks : p.passSacks
                ts.passComp = ts.passComp ? p.passComp + ts.passComp : p.passComp
                ts.passTDs = ts.passTDs ? p.passTDs + ts.passTDs : p.passTDs


                sum.passAtt = sum.passAtt ? p.passAtt + sum.passAtt : p.passAtt
                sum.passInts = sum.passInts ? p.passInts + sum.passInts : p.passInts
                sum.passSacks = sum.passSacks ? p.passSacks + sum.passSacks : p.passSacks
                sum.passComp = sum.passComp ? p.passComp + sum.passComp : p.passComp
                sum.passTDs = sum.passTDs ? p.passTDs + sum.passTDs : p.passTDs
                sum.passYds = sum.passYds ? p.passYds + sum.passYds : p.passYds
            }
            // ts.depthOfCompletion - 
            ts.intRate = ts.passInts / ts.passAtt
            ts.compPercentage = ts.passComp / ts.passAtt
            ts.sackRate = ts.passSacks / (ts.passSacks + ts.passAtt)
            ts.tdRate = ts.passTDs / ts.passAtt
            ts.ypa = ts.passYds / ts.passAtt
            ts.ypc = ts.passYds / ts.passComp

            let a = ((ts.passComp / ts.passAtt) - .3) * 5
            let b = ((ts.passYds / ts.passAtt) - 3) * .25
            let c = ((ts.passTDs / ts.passAtt)) * 20
            let d = 2.375 - ((ts.passInts / ts.passAtt) * 25)

            a = a > 2.375 ? 2.375 : a
            b = b > 2.375 ? 2.375 : b
            c = c > 2.375 ? 2.375 : c
            d = d > 2.375 ? 2.375 : d

            a = a < 0 ? 0 : a
            b = b < 0 ? 0 : b
            c = c < 0 ? 0 : c
            d = d < 0 ? 0 : d

            ts.qbr = ((a + b + c + d) / 6) * 100

            ts.ncaa = ((8.4 * ts.passYds) + (330 * ts.passTDs) + (100 * ts.passComp) - (200 * ts.passInts)) / ts.passAtt

            let drops = teamRecStats[team].dropArray.reduce((a, b) => {
                return a + b
            })
            ts.yac = teamRecStats[team].teamStats.recYdsAfterCatch
            ts.catchDepth = (ts.passYds - ts.yac) / ts.passComp
            ts.adjCompPercentage = (ts.passComp + drops) / ts.passAtt
            ts.receiverDrops = drops
            ts.name = teams[team].name
            ts.yacPercent = ts.yac / ts.passYds


            delete teams[team].attArray
            delete teams[team].compArray
        }

        const orderedPlayers = []
        for (let player in players) {
            if (players[player].passAtt > 0) {
                orderedPlayers.push(players[player])
            }
        }


        orderedPlayers.sort((a, b) => {
            return b.passYds - a.passYds
        })

        const orderedTeams = []
        for (let team in teams) {
            orderedTeams.push(teams[team])
        }


        orderedTeams.sort((a, b) => {
            return b.teamStats.passYds - a.teamStats.passYds
        })


        // remove irrelavant data from teams


        return [orderedPlayers, orderedTeams, players]

    },


}


