export default {


    convertPlayersToObject(players, teams) {
        const playerObject = {}
        players.forEach(player => {
            playerObject[player._id] = {
                firstName: player.firstName,
                lastName: player.lastName,
                position: player.position,
                fantasyPoints: 0,
                _id: player._id

            }

        })
        return playerObject
    },

    convertTeamsToObject(teams) {
        const teamObject = {}
        teams.forEach((team) => {

            teamObject[team.teamId] = {
                name: team.name

            }
        })


        return teamObject
    },


    async calculateFantasyPoints(players, passingData, rushingData, receivingData, teams) {

        if (!players || !passingData || !rushingData || !receivingData) {

            return
        }


        teams = await this.convertTeamsToObject(teams)
        players = await this.convertPlayersToObject(players, teams)
        passingData.playerStats.forEach((stat) => {
            if (!players[stat.rosterId]) {
                return
            }

            players[stat.rosterId].teamName = teams[stat.teamId].name
            players[stat.rosterId].passYds = stat.passYds
            players[stat.rosterId].passTDs = stat.passTDs
            players[stat.rosterId].passInts = stat.passInts

            players[stat.rosterId].fantasyPoints += stat.passYds * .04
            players[stat.rosterId].fantasyPoints += stat.passTDs * 4
            players[stat.rosterId].fantasyPoints += stat.passInts * -2
        })
        rushingData.playerStats.forEach((stat) => {
            if (!players[stat.rosterId]) {
                return
            }
            players[stat.rosterId].teamName = teams[stat.teamId].name

            players[stat.rosterId].rushYds = stat.rushYds
            players[stat.rosterId].rushTDs = stat.rushTDs
            players[stat.rosterId].rushFum = stat.rushFum

            players[stat.rosterId].fantasyPoints += stat.rushYds * .1
            players[stat.rosterId].fantasyPoints += stat.rushTDs * 6
            players[stat.rosterId].fantasyPoints += stat.rushFum * -2
        })

        receivingData.playerStats.forEach((stat) => {
            if (!players[stat.rosterId]) {
                return
            }
            players[stat.rosterId].teamName = teams[stat.teamId].name

            players[stat.rosterId].recYds = stat.recYds
            players[stat.rosterId].recTDs = stat.recTDs
            players[stat.rosterId].recCatches = stat.recCatches

            players[stat.rosterId].fantasyPoints += stat.recYds * .1
            players[stat.rosterId].fantasyPoints += stat.recTDs * 6
            players[stat.rosterId].fantasyPoints += stat.recCatches * 1
        })

        const orderedPlayers = []
        for (let player in players) {
            if (players[player].fantasyPoints > 0)
                orderedPlayers.push(players[player])
        }

        orderedPlayers.sort((a, b) => {
            return b.fantasyPoints - a.fantasyPoints
        })




        return orderedPlayers

    },

    async calculateFantasyStudsAndDuds(weeklyStats) {

        //INCOMING WEEKLY STAT DATA IS SORTED BY PLAYERS FANTASY POINTS FOR THE WEEK

        //STORE PLAYERS AVERAGES
        const playerMetaData = {}
        const studsAndDuds = []


        //LOOP THROUGH EACH WEEK
        weeklyStats.forEach((week, weekIndex) => {

            if (!week) {
                return
            }




            let qbTopTen = 0
            let hbTopTen = 0
            let wrTopTen = 0
            let teTopTen = 0
            //LOOP THROUGH EACH PLAYER



            let qbStud = {
                diff: 0,
                _id: undefined
            }
            let qbDud = {
                diff: 0,
                _id: undefined
            }
            let hbStud = {
                diff: 0,
                _id: undefined
            }
            let hbDud = {
                diff: 0,
                _id: undefined
            }
            let wrStud = {
                diff: 0,
                _id: undefined
            }
            let wrDud = {
                diff: 0,
                _id: undefined
            }
            let teStud = {
                diff: 0,
                _id: undefined
            }
            let teDud = {
                diff: 0,
                _id: undefined
            }

            week.forEach((player) => {



                //INITIALIZE PLAYER IF NOT SEEN UET
                if (!playerMetaData[player._id]) {
                    playerMetaData[player._id] = {
                        weeklyFantasyPoints: [],
                        dud: [],
                        stud: []
                    }
                }


                //STORE NUMBER OF TOP TEN FINISHES

                if (player.position === 'QB' && qbTopTen < 10) {
                    if (playerMetaData[player._id].qbTopTen) {
                        playerMetaData[player._id].qbTopTen[weekIndex] = 1
                    } else {
                        playerMetaData[player._id].qbTopTen = []
                        playerMetaData[player._id].qbTopTen[weekIndex] = 1

                    }
                    qbTopTen++

                }

                if (player.position === 'HB' && hbTopTen < 10) {
                    if (playerMetaData[player._id].hbTopTen) {
                        playerMetaData[player._id].hbTopTen[weekIndex] = 1
                    } else {
                        playerMetaData[player._id].hbTopTen = []
                        playerMetaData[player._id].hbTopTen[weekIndex] = 1

                    }
                    hbTopTen++
                }

                if (player.position === 'WR' && wrTopTen < 10) {
                    if (playerMetaData[player._id].wrTopTen) {
                        playerMetaData[player._id].wrTopTen[weekIndex] = 1
                    } else {
                        playerMetaData[player._id].wrTopTen = []
                        playerMetaData[player._id].wrTopTen[weekIndex] = 1

                    }
                    wrTopTen++
                }

                if (player.position === 'TE' && teTopTen < 10) {
                    if (playerMetaData[player._id].teTopTen) {
                        playerMetaData[player._id].teTopTen[weekIndex] = 1
                    } else {
                        playerMetaData[player._id].teTopTen = []
                        playerMetaData[player._id].teTopTen[weekIndex] = 1

                    }
                    teTopTen++
                }






                if (weekIndex > 0) {


                    const previousAvg = playerMetaData[player._id].weeklyFantasyPoints.reduce((p, c) => p + c, 0) / playerMetaData[player._id].weeksPlayed;
                    const scoreDiff = player.fantasyPoints - previousAvg

                    if (player.position === 'QB') {

                        if (scoreDiff > qbStud.diff) {
                            qbStud = player
                            qbStud.diff = scoreDiff
                        }

                        if (scoreDiff < qbDud.diff) {
                            qbDud = player
                            qbDud.diff = scoreDiff

                        }
                    }

                    if (player.position === 'HB') {
                        if (scoreDiff > hbStud.diff) {
                            hbStud = player
                            hbStud.diff = scoreDiff
                        }

                        if (scoreDiff < hbDud.diff) {
                            hbDud = player
                            hbDud.diff = scoreDiff

                        }
                    }
                    if (player.position === 'WR') {
                        if (scoreDiff > wrStud.diff) {
                            wrStud = player
                            wrStud.diff = scoreDiff
                        }

                        if (scoreDiff < wrDud.diff) {
                            wrDud = player
                            wrDud.diff = scoreDiff


                        }
                    }
                    if (player.position === 'TE') {
                        if (scoreDiff > teStud.diff) {

                            teStud = player;
                            teStud.diff = scoreDiff
                        }

                        if (scoreDiff < teDud.diff) {
                            teDud = player;
                            teDud.diff = scoreDiff


                        }
                    }


                }
                // STORE PLAYER WEEKLY FANTASY POINTS


                playerMetaData[player._id].weeklyFantasyPoints[weekIndex] = (player.fantasyPoints)
                playerMetaData[player._id].weeksPlayed = playerMetaData[player._id].weeksPlayed ? playerMetaData[player._id].weeksPlayed + 1 : 1

            })

            if (weekIndex > 0 && weekIndex !== 20) {

                studsAndDuds[weekIndex] = (
                    {
                        qbStud: qbStud,
                        qbDud: qbDud,
                        hbStud: hbStud,
                        hbDud: hbDud,
                        wrStud: wrStud,
                        wrDud: wrDud,
                        teStud: teStud,
                        teDud: teDud
                    }

                )


            } else {
                studsAndDuds[weekIndex] = ({
                    week: weekIndex

                }
                )
            }




        })



        return [playerMetaData, studsAndDuds]
    }

}


