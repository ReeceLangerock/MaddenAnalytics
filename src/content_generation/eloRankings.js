export default {


    initializeElo(teams) {
        const elo = {}
        teams.forEach((team) => {

            elo[team.teamId] = {
                name: team.name,
                elo: [1500],
                users: team.users

            }


        })


        return elo
    },


    async calculateEloRankings(scores, teams, currentSeasonOnly) {

        if (!teams || !scores) {
            return
        }
        const elo = await this.initializeElo(teams)
        let reversionList = []
        let currentSeason = null
        // only generate the ELO for the current season
        if (currentSeasonOnly) {
            currentSeason = scores.length - 1
            scores = [scores[scores.length - 1]]

        }

        // loop through all seasons

        for (let s = 0; s < scores.length; s++) {

            if (!scores[s]) {
                continue
            }
            // loop through each week of the current season

            for (let w = 0; w < scores[s].length; w++) {

                if (scores[s][w]) {
                    // if week has data, loop through each game

                    for (let g = 0; g < scores[s][w].teamData.length; g++) {


                        let weekData = scores[s][w].teamData[g]
                        if (weekData.pointsScored === 0 && weekData.pointsAllowed === 0) {
                            continue;
                        }
                        const [homeElo, awayElo, reversions] = this.calculateEloDelta(elo[weekData.teamID], elo[weekData.opponentTeamID], w, weekData.pointsScored, weekData.pointsAllowed, s, currentSeason)
                        reversionList = [...reversionList, ...reversions]
                        elo[weekData.teamID].elo.push(homeElo);
                        elo[weekData.opponentTeamID].elo.push(awayElo);

                    }
                }
            }
        }
        return elo;

    },

    calculateEloDelta(homeTeam, awayTeam, week, pointsScored, pointsAllowed, season, currentSeason) {
        season = currentSeason === null ? season : currentSeason
        if (!homeTeam || !awayTeam) {
            return
        }

        const HFA = 13.0     // Home field advantage is worth 65 Elo points
        const K = 22.0       		// The speed at which Elo ratings change
        const REVERT = 1 / 4 		// Between seasons, a team retains 3/4 of its previous season's rating
        const NEW_USER_REVERT = 1 / 3 		// Between seasons, a team retains 2/3 of its previous season's rating
        const baseELO = 1500;
        const reversions = []
        let homeElo = homeTeam.elo[homeTeam.elo.length - 1]
        let awayElo = awayTeam.elo[awayTeam.elo.length - 1]

        if (week === 0) {
            homeElo = baseELO * REVERT + homeElo * (1 - REVERT)
            awayElo = baseELO * REVERT + awayElo * (1 - REVERT)
        }

        //revert for new user

        //NEED TO REMOVE +1 FROM SEASON EVENTUALLY, NOW IN PLACE TO HAVE ELO SCORES ONLY COUNT CURRENT SEASON FOR REVO

        homeTeam.users.forEach((user) => {
            if (user.username !== '' && user.firstSeen[0] === season && user.firstSeen[1] === week) {
                let eloAfterReversion = baseELO * NEW_USER_REVERT + homeElo * (1 - NEW_USER_REVERT)
                if (homeElo !== eloAfterReversion) {
                    reversions.push(`Season ${season + 1}, Week ${week + 1}:  ${homeTeam.name} ELO rating reverted from ${homeElo} to ${eloAfterReversion}`)

                }
                homeElo = eloAfterReversion;



            }
        })
        awayTeam.users.forEach((user) => {
            if (user.username !== '' && user.firstSeen[0] === season && user.firstSeen[1] === week) {
                let eloAfterReversion = baseELO * NEW_USER_REVERT + awayElo * (1 - NEW_USER_REVERT)
                if (awayElo !== eloAfterReversion) {
                    reversions.push(`Season ${season + 1}, Week ${week + 1}:  ${awayTeam.name} ELO rating reverted from ${awayElo} to ${eloAfterReversion}`)

                }
                awayElo = eloAfterReversion;

            }
        })

        const eloDiff = (homeElo + HFA) - awayElo

        const eloProb = 1.0 / Math.pow(10, ((-eloDiff / 400) + 1))

        const pointDifferential = Math.abs(pointsScored - pointsAllowed);


        let gameResult
        let winner
        // hometeam win
        if (pointsScored > pointsAllowed) {
            winner = 1
            gameResult = 2.2 / (eloDiff * .001 + 2.2)
        } // awayteam win 
        else if (pointsScored < pointsAllowed) {
            winner = -1
            gameResult = 2.2 / (-eloDiff * .001 + 2.2)

        } //tie	
        else {
            winner = .5
            gameResult = (2.2 / 1)
        }
        const multiplier = Math.log(Math.max(pointDifferential, 1) + 1) * gameResult

        const eloShift = (K * multiplier) * (winner * eloProb)

        homeElo += eloShift
        awayElo -= eloShift
        return [homeElo, awayElo, reversions]

    }

}


