import util from './contentUtilities'

export default {

    async calculatePythagoreanRankings(weeklyData, initTeams, CW) {
        let weeks = weeklyData

        const teams = await util.convertTeamsToObject(initTeams)


        if (!teams || !weeks) {

            return
        }

        weeks = weeks[weeks.length - 1]
        CW = CW > 17 ? 17 : CW
        const previousWeek = CW - 1



        //first loop through each week, max will be 17
        for (let i = 0; i < CW; i++) {
            if (weeks[i] !== null) {

                weeks[i].teamData.forEach(team => {

                    if (i === previousWeek) {
                        teams[team.teamID].previousPointsAllowed = teams[team.teamID].pointsAllowed
                        teams[team.teamID].previousPointsScored = teams[team.teamID].pointsScored

                        teams[team.opponentTeamID].previousPointsAllowed = teams[team.opponentTeamID].pointsAllowed
                        teams[team.opponentTeamID].previousPointsScored = teams[team.opponentTeamID].pointsScored
                    }

                    teams[team.teamID].pointsAllowed = teams[team.teamID].pointsAllowed
                        ? (teams[team.teamID].pointsAllowed += team.pointsAllowed)
                        : team.pointsAllowed
                    teams[team.teamID].pointsScored = teams[team.teamID].pointsScored
                        ? (teams[team.teamID].pointsScored += team.pointsScored)
                        : team.pointsScored

                    if (team.pointsScored > team.pointsAllowed) {
                        teams[team.teamID].wins = teams[team.teamID].wins
                            ? teams[team.teamID].wins + 1
                            : 1
                        teams[team.opponentTeamID].losses = teams[team.opponentTeamID]
                            .losses
                            ? teams[team.opponentTeamID].losses + 1
                            : 1
                    } else if (team.pointsScored < team.pointsAllowed) {
                        teams[team.teamID].losses = teams[team.teamID].losses
                            ? teams[team.teamID].losses + 1
                            : 1
                        teams[team.opponentTeamID].wins = teams[team.opponentTeamID].wins
                            ? teams[team.opponentTeamID].wins + 1
                            : 1
                    }

                    teams[team.opponentTeamID].pointsAllowed = teams[team.opponentTeamID]
                        .pointsAllowed
                        ? (teams[team.opponentTeamID].pointsAllowed += team.pointsScored)
                        : team.pointsScored

                    teams[team.opponentTeamID].pointsScored = teams[team.opponentTeamID]
                        .pointsScored
                        ? (teams[team.opponentTeamID].pointsScored += team.pointsAllowed)
                        : team.pointsAllowed
                })
            }
        }

        const teamsWithPyth = Object.keys(teams).map(team => {
            const t = teams[team]
            t.pythagoreanScore =
                Math.pow(t.pointsScored, 2.37) /
                (Math.pow(t.pointsScored, 2.37) + Math.pow(t.pointsAllowed, 2.37)) *
                ((t.wins || 0) + (t.losses || 0))

            t.previousPythagoreanScore =
                Math.pow(t.previousPointsScored, 2.37) /
                (Math.pow(t.previousPointsScored, 2.37) +
                    Math.pow(t.previousPointsAllowed, 2.37)) *
                    ((t.wins || 0) + (t.losses || 0)) - 1

            delete t.previousPointsAllowed
            delete t.previousPointsScored
            return t
        })

        const sortedTeams = teamsWithPyth.sort((a, b) => {
            return b.previousPythagoreanScore - a.previousPythagoreanScore
        })
        const teamsWithPreviousRank = sortedTeams.map((team, index) => {
            team.previousRank = index
            return team
        })

        const finalSortedTeams = teamsWithPreviousRank.sort((a, b) => {
            return b.pythagoreanScore - a.pythagoreanScore
        })

        return finalSortedTeams

    }

}


