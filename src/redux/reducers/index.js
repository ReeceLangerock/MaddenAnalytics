import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import {
	leagueAdditionReducer,
	browseLeaguesReducer,
	teamsReducer,
	weeklyDataReducer,
	rosterReducer,
	leagueReducer, adminReducer,
	filterReducer
} from './reducers'

import { contentReducer } from './contentReducers'

export default combineReducers({
	routing: routerReducer,
	leagueAdditionReducer,
	contentReducer,
	teamsReducer,
	rosterReducer,
	weeklyDataReducer,
	browseLeaguesReducer,
	leagueReducer, adminReducer,
	filterReducer
})
