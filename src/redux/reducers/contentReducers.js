export const contentReducer = (
	state = {

		statusMessage: 'Connecting To DeepDive Servers',
		stepsCompleted: 0,
		stepsTotal: 1,
		initiationStarted: false,
		initiationCompleted: false,
		eloRankings: undefined,
		pythRankings: undefined,
		fantasyRankings: undefined,
		fantasyMetaData: undefined,
		fantasySAD: undefined,
		recStats: undefined,
		passStats: undefined,
		rushStats: undefined,
		teamPassStats: undefined,
		teamRushStats: undefined,
		teamRecStats: undefined,
		userLeaderboard: undefined,
		h2hStats: undefined,
		matchupAnalysis: undefined,
		draftkings: undefined,
		fantasyAggregation: undefined
		
	},
	action
) => {
	switch (action.type) {

		case 'SAVE_ELO_RANKINGS':
			return {
				...state,
				eloRankings: action.data
			}

			case 'SAVE_H2H_STATS':
			return {
				...state,
				h2hStats: action.data
			}
			case 'SAVE_MATCHUP_ANALYSIS':
			return {
				...state,
				matchupAnalysis: action.data
			}

			

		case 'SAVE_PYTH_RANKINGS':
			return {
				...state,
				pythRankings: action.data
			}

		case 'SAVE_REC_STATS':
			return {
				...state,
				recStats: action.data
			}
		case 'SAVE_PASS_STATS':
			return {
				...state,
				passStats: action.data
			}

			case 'SAVE_TEAM_PASS_STATS':
			return {
				...state,
				teamPassStats: action.data
			}
			case 'SAVE_TEAM_RUSH_STATS':
			return {
				...state,
				teamRushStats: action.data
			}
			case 'SAVE_TEAM_REC_STATS':
			return {
				...state,
				teamRecStats: action.data
			}
		case 'SAVE_RUSH_STATS':
			return {
				...state,
				rushStats: action.data
			}

		case 'SAVE_PLAYER_FANTASY_RANKS':
			return {
				...state,
				fantasyRankings: action.data
			}

		case 'SAVE_FANTASY_META_DATA':
			return {
				...state,
				fantasyMetaData: action.data
			}

			case 'SAVE_USER_LEADERBOARD':
			return {
				...state,
				userLeaderboard: action.data
			}

			case 'SAVE_DRAFTKINGS_DATA':
			return {
				...state,
				draftkings: action.data
			}
			case 'SAVE_FANTASY_AGGREGATION':
			return {
				...state,
				fantasyAggregation: action.data
			}

			

		case 'SAVE_SAD':
			return {
				...state,
				fantasySAD: action.data
			}

		case 'CLEAR_FANTASY':
			return {
				...state,
				fantasyRankings: undefined
			}

		case 'CLEAR_CONTENT':
			return {
				...state,
				initiationStarted: false,
		initiationCompleted: false,
		eloRankings: undefined,
		pythRankings: undefined,
		fantasyRankings: undefined,
		fantasyMetaData: undefined,
		fantasySAD: undefined,
		recStats: undefined,
		passStats: undefined,
		rushStats: undefined,
		teamPassStats: undefined,
		teamRushStats: undefined,
		teamRecStats: undefined,
		userLeaderboard: undefined
			}
		case 'UPDATE_INITIATION_STATUS': {

			return {
				...state,
				statusMessage: action.statusMessage,
				stepsCompleted: action.stepsCompleted
			}
		}
		case 'START_INITIATION': {

			return {
				...state,
				initiationStarted: true,
				initiationCompleted: false
			}
		}

		case 'END_INITIATION': {
			return {
				...state,
				initiationStarted: false,
				initiationCompleted: true
			}
		}


		default:
			return state
	}
}
