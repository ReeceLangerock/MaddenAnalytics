export const leagueAdditionReducer = (
	state = {
		addition: {
			success: undefined, // true for success, false for failure
			redirectUrl: undefined, // will contain url of existing site, if applicable, or newly created league
			error: '' // will be undefined unless failure
		}
	},
	action
) => {
	switch (action.type) {
		case 'HANDLE_LEAGUE_ADDITION_ATTEMPT':
			return {
				...state,
				addition: {
					...state.addition,
					...action.data
				}
			}

		default:
			return state
	}
}

export const rosterReducer = (
	state = {
		skillPositions: undefined
	},
	action
) => {
	switch (action.type) {

		case 'SAVE_SKILL_POSITION_PLAYERS':
			return {
				...state,
				skillPositions: action.data
			}

		default:
			return state
	}
}

export const adminReducer = (
	state = {
		authenticated: undefined
	},
	action
) => {
	switch (action.type) {

		case 'SAVE_ADMIN_TOKEN':
			return {
				...state,
				authenticated: action.data
			}

		default:
			return state
	}
}

export const weeklyDataReducer = (
	state = {
		passingData: undefined,
		rushingData: undefined,
		receivingData: undefined,
	},
	action
) => {
	switch (action.type) {

		case 'SAVE_PASSING_DATA':
			return {
				...state,
				passingData: action.data
			}
		case 'SAVE_RUSHING_DATA':
			return {
				...state,
				rushingData: action.data
			}
		case 'SAVE_RECEIVING_DATA':
			return {
				...state,
				receivingData: action.data
			}

		default:
			return state
	}
}




export const leagueReducer = (
	state = {
		teams: [],
		league: {
			status: 'Awaiting Data'
		},
		weeklyData: null
	},
	action
) => {
	switch (action.type) {
		case 'SAVE_LEAGUE_INFO':
			return {
				...state,
				league: action.data
			}
		case 'SAVE_WEEKLY_DATA':
			return {
				...state,
				weeklyData: action.data
			}
		case 'SAVE_TEAM_DATA':
			return {
				...state,
				teams: action.data
			}

		default:
			return state
	}
}

export const browseLeaguesReducer = (
	state = {
		leagues: []
	},
	action
) => {
	switch (action.type) {
		case 'SAVE_LEAGUE_SEARCH_RESULTS':
			return {
				...state,
				leagues: action.data
			}

		default:
			return state
	}
}

export const teamsReducer = (state = {}, action) => {
	switch (action.type) {
		case 'SAVE_LEAGUE_SEARCH_RESULTS':
			return {
				...state,
				leagues: action.data
			}

		default:
			return state
	}
}


export const filterReducer = (state = {
	filter: {
		teams: {

			'49ers': true,
			Bears: true,
			Bengals: true,
			Bills: true,
			Broncos: true,
			Browns: true,
			Buccaneers: true,
			Cardinals: true,
			Chargers: true,
			Chiefs: true,
			Colts: true,
			Cowboys: true,
			Dolphins: true,
			Eagles: true,
			Falcons: true,
			Giants: true,
			Jaguars: true,
			Jets: true,
			Lions: true,
			Packers: true,
			Panthers: true,
			Patriots: true,
			Raiders: true,
			Rams: true,
			Ravens: true,
			Redskins: true,
			Saints: true,
			Seahawks: true,
			Steelers: true,
			Texans: true,
			Titans: true,
			Vikings: true
		},
		positions: {
			QB: true,
			WR: true,
			TE: true,
			HB: true,
			FB: true
		},
		minAttempts: 1


	}
}, action) => {
	switch (action.type) {
		case 'APPLY_FILTERS':
			return {
				...state,
				filter: {
					teams: action.data.teams,
					positions: action.data.positions,
					minAttempts: action.data.minAttempts

				}
			}


		default:
			return state
	}
}

