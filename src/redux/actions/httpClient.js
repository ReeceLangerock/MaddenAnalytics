import axios from 'axios';
const baseApiUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:3001/api' : 'https://ddbl.herokuapp.com/api'; //hard coded for now - use env var later
console.log(process.env.NODE_ENV)
// store token globally - updated on login/logout
let token = null;
const setToken = _token => {
  token = _token;
};

const instance = axios.create();
// add token to request if it exists
instance.interceptors.request.use(
  config => {
    if (token) {
      config.headers = { Authorization: `Token ${token}` };
    }
    return config;
  },
  err => Promise.reject(err)
);

const requests = {
  getIfLeagueExists: () => {
    return instance.get(`${baseApiUrl}/add`);
  },
  getLeagues: (queryString) => {
    return instance.post(`${baseApiUrl}/browse`, queryString);
  },

  getLeagueData: (league, data, week) => {
    return instance.get(`${baseApiUrl}/league/${league}/${data}/${week}`);
  },
  getDKData: () => {
    return instance.get(`${baseApiUrl}/dk`);
  },
  postNewLeague: (league) => {
    return instance.post(`${baseApiUrl}/add`, league);

  },
  authenticateAdmin: (loginAttempt) => {
    return instance.post(`${baseApiUrl}/admin`, loginAttempt);

  }


};

export { requests, setToken };
