import { requests } from './httpClient'
import localStorageManager from './localStorageManager'


export const toggleLoginModal = () => {
	return {
		type: 'TOGGLE_LOGIN_MODAL'
	}
}

export const getIfLeagueExists = () => async dispatch => {
	try {
		const res = await requests.getIfLeagueExists()
		dispatch({ type: 'GET_INDIVIDUAL_RESOURCE', data: res.data })
	} catch (err) {
		// throw error
	}
}

export const addLeague = league => async dispatch => {
	try {
		const res = await requests.postNewLeague(league)
		dispatch({ type: 'HANDLE_LEAGUE_ADDITION_ATTEMPT', data: res.data })
	} catch (err) {
		// throw error
	}
}

export const saveEloRankings = (data) => {
	return {
		type: 'SAVE_ELO_RANKINGS',
		data
	}
}

export const saveUserLeaderboard = (data) => {
	return {
		type: 'SAVE_USER_LEADERBOARD',
		data
	}
}
export const savePythRankings = (data) => {
	return {
		type: 'SAVE_PYTH_RANKINGS',
		data
	}
}

export const saveRecStats = (data) => {
	return {
		type: 'SAVE_REC_STATS',
		data
	}
}
export const savePassStats = (data) => {
	return {
		type: 'SAVE_PASS_STATS',
		data
	}
}
export const saveTeamPassStats = (data) => {
	return {
		type: 'SAVE_TEAM_PASS_STATS',
		data
	}
}

export const saveTeamRecStats = (data) => {
	return {
		type: 'SAVE_TEAM_REC_STATS',
		data
	}
}

export const saveTeamRushStats = (data) => {
	return {
		type: 'SAVE_TEAM_RUSH_STATS',
		data
	}
}
export const saveRushStats = (data) => {
	return {
		type: 'SAVE_RUSH_STATS',
		data
	}
}


export const savePlayerFantasyRankings = (data) => {
	return {
		type: 'SAVE_PLAYER_FANTASY_RANKS',
		data
	}
}

export const saveFantasyMetaData = (data) => {
	return {
		type: 'SAVE_FANTASY_META_DATA',
		data
	}
}

export const saveAggregatedFantasyData = (data) => {
	return {
		type: 'SAVE_FANTASY_AGGREGATION',
		data
	}
}

export const saveSAD = (data) => {
	return {
		type: 'SAVE_SAD',
		data
	}
}

export const saveH2HStats = (data) => {
	return {
		type: 'SAVE_H2H_STATS',
		data
	}
}

export const saveMatchupAnalysis = (data) => {
	return {
		type: 'SAVE_MATCHUP_ANALYSIS',
		data
	}
}

export const clearFantasy = () => {
	return {
		type: 'CLEAR_FANTASY'

	}
}

export const authenticateAdmin = (loginAttempt) => async dispatch => {
	try {
		const res = await requests.authenticateAdmin(loginAttempt)
		dispatch({ type: 'SAVE_ADMIN_TOKEN', data: res.data.result })
	} catch (err) {
		// throw error
	}
}

export const getLeagues = queryString => async dispatch => {
	try {
		const res = await requests.getLeagues(queryString)

		dispatch({ type: 'SAVE_LEAGUE_SEARCH_RESULTS', data: res.data })
	} catch (err) {
		// throw error
	}
}
export const getDKData = () => async dispatch => {
	try {
		const res = await requests.getDKData()
		dispatch({ type: 'SAVE_DRAFTKINGS_DATA', data: res.data })
	} catch (err) {
		// throw error
	}
}


export const clearContent = () => {
	return {
		type: 'CLEAR_CONTENT'

	}
}

export const applyFilters = (data) => {
	return {
		type: 'APPLY_FILTERS',
		data

	}
}

export const updateInitiationStatus = (statusMessage, stepsCompleted) => {
	
	return {
		type: 'UPDATE_INITIATION_STATUS',
		statusMessage,
		stepsCompleted

	}
}

export const startInitiation = () => {
	return {
		type: 'START_INITIATION',


	}
}
export const endInitiation = () => {
	return {
		type: 'END_INITIATION',


	}
}



export const getLeagueData = (abbreviation, dataToGet, week) => async dispatch => {

	try {
		const res = await requests.getLeagueData(abbreviation, dataToGet, week)
		if (res.data.info) {
			dispatch({ type: 'SAVE_LEAGUE_INFO', data: res.data.info })
		}


		if (res.data.skillPositionData) {
			dispatch({ type: 'SAVE_SKILL_POSITION_PLAYERS', data: res.data.skillPositionData })
			const dataKey = `skillPlayers_S${res.data.season}-W${res.data.week}`
			localStorageManager.saveDataToLocalStorage(abbreviation, dataKey, res.data.skillPositionData)

		}
		if (res.data.scoringData) {
			dispatch({ type: 'SAVE_WEEKLY_DATA', data: res.data.scoringData })
			const dataKey = `scoringData_S${res.data.season}-W${res.data.week}`
			localStorageManager.saveDataToLocalStorage(abbreviation, dataKey, res.data.scoringData)

		}
		if (res.data.teams) {
			dispatch({ type: 'SAVE_TEAM_DATA', data: res.data.teams })
			const dataKey = `teams_S${res.data.season}-W${res.data.week}`
			localStorageManager.saveDataToLocalStorage(abbreviation, dataKey, res.data.teams)


		}
	} catch (err) {
		// throw error
	}
}

export const getWeeklyData = (abbreviation, dataToGet, week) => async dispatch => {
	try {
		const res = await requests.getLeagueData(abbreviation, dataToGet, week)

		if (res.data.weeklyData.passingData) {
			dispatch({ type: 'SAVE_PASSING_DATA', data: res.data.weeklyData.passingData })
			const dataKey = `passingData_S${res.data.weeklyData.season}-W${res.data.weeklyData.week}`
			localStorageManager.saveDataToLocalStorage(abbreviation, dataKey, res.data.weeklyData.passingData)
		}
		if (res.data.weeklyData.rushingData) {
			dispatch({ type: 'SAVE_RUSHING_DATA', data: res.data.weeklyData.rushingData })
			const dataKey = `rushingData_S${res.data.weeklyData.season}-W${res.data.weeklyData.week}`
			localStorageManager.saveDataToLocalStorage(abbreviation, dataKey, res.data.weeklyData.rushingData)
		}
		if (res.data.weeklyData.receivingData) {
			dispatch({ type: 'SAVE_RECEIVING_DATA', data: res.data.weeklyData.receivingData })
			const dataKey = `receivingData_S${res.data.weeklyData.season}-W${res.data.weeklyData.week}`
			localStorageManager.saveDataToLocalStorage(abbreviation, dataKey, res.data.weeklyData.receivingData)
		}

	} catch (err) {
		// throw error
	}
}



export const saveLocalStorageDataToStore = (type, data) => async dispatch => {
	switch (type) {
		case 'weeklyData':
			dispatch({ type: 'SAVE_PASSING_DATA', data: data.weeklyData.passingData })
			dispatch({ type: 'SAVE_RUSHING_DATA', data: data.weeklyData.rushingData })
			dispatch({ type: 'SAVE_RECEIVING_DATA', data: data.weeklyData.receivingData })
			break;
		case 'teams': {
			dispatch({ type: 'SAVE_TEAM_DATA', data: data.teams })
			break;


		}
		case 'scoringData': {
			dispatch({ type: 'SAVE_WEEKLY_DATA', data: data.scoringData })
			break;


		}

		case 'skillPlayers': {
			dispatch({ type: 'SAVE_SKILL_POSITION_PLAYERS', data: data.skillPlayers })
			break;


		} default: {
			break;
		}
	}
}
