import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import styled, { injectGlobal } from 'styled-components'
// import coyote from './assets/Coyote.otf'

import * as actions from './redux/actions/actions'
import { } from 'semantic-ui-react'

import Nav from './components/general/Navigation'
import Footer from './components/general/Footer'
import Index from './components/index/Index'
import Browse from './components/browse/Browse'
import AddLeague from './components/add_league/AddLeague'
import League from './components/league/League'
import Team from './components/team/Team'
import ErrorPage from './components/general/ErrorPage'
import Pythagorean from './components/content/Pythagorean'
import EloRankings from './components/content/EloRankings'
import MatchupAnalysis from './components/content/MatchupAnalysis'
import Fantasy from './components/content/Fantasy'
import UserLeaderboard from './components/user/UserLeaderboard'
import Admin from './components/admin/Admin'
import DataDownloader from './components/league/DataDownloader';


import RecStats from './components/content/RecStats'
import RushStats from './components/content/RushStats'
import PassingStats from './components/content/PassingStats'
import DraftKings from './components/content/DraftKings'

import TeamPassingStats from './components/content/teamstats/TeamPassingStats'
import TeamRecStats from './components/content/teamstats/TeamRecStats'
import TeamRushStats from './components/content/teamstats/TeamRushStats'


import { ConnectedRouter } from 'react-router-redux'
import { history } from './redux/store/store'

 injectGlobal`
  @font-face {
    font-family: 'Coyote';
    src: url('');
  }  

` 

class App extends React.Component {


	render() {
		return (
			<ConnectedRouter history={history}>
				<OuterContainer>
					<Nav />

					<Switch >
						<Route exact path="/" component={Index} />
						<Route exact path="/browse" component={Browse} />
						<Route exact path="/admin" component={Admin} />
						<Route exact path="/add_league" component={AddLeague} />
						<Route exact path="/:id/pythagorean" component={Pythagorean} />
						<Route exact path="/:id/elo" component={EloRankings} />
						<Route exact path="/:id/matchup" component={MatchupAnalysis} />
						<Route exact path="/:id/fantasy" component={Fantasy} />
						<Route exact path="/:id/draftkings" component={DraftKings} />
						<Route exact path="/:id/team/:team" component={Team} />
						<Route exact path="/:id/stats/receiving" component={RecStats} />
						<Route exact path="/:id/stats/rushing" component={RushStats} />
						<Route exact path="/:id/stats/passing" component={PassingStats} />
						<Route exact path="/:id/user/leaderboard" component={UserLeaderboard} />
						<Route exact path="/:id/team-stats/passing" component={TeamPassingStats} />
						<Route exact path="/:id/team-stats/receiving" component={TeamRecStats} />
						<Route exact path="/:id/team-stats/rushing" component={TeamRushStats} />
						<Route exact path="/:id" component={League} />
						<Route path="*" component={ErrorPage} />


					</Switch>
					<DataDownloader />
					<Footer />
				</OuterContainer>
			</ConnectedRouter>
		)
	}
}

export default connect(undefined, actions)(App)

const OuterContainer = styled.div`
	/* overflow: hidden; */
	display: flex;
	min-height: 100vh;
	flex-direction: column;

	justify-content: space-between;



`
