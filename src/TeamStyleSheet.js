export default {
  teams: {

    '49ers': {
      primary: '170,0,0',
      secondary: '179,153,94'
    },
    Bears: {
      primary: '5,28,44',
      secondary: '220,68,5'
    },
    Bengals: {
      primary: '252,76,2',
      secondary: '16,24,32'
    },
    Bills: {
      primary: '12,46,130',
      secondary: '200,16,46'
    },
    Broncos: {
      primary: '252,76,2',
      secondary: '12,35,64'
    },
    Browns: {
      primary: '56,47,45',
      secondary: '235,51,0'
    },
    Buccaneers: {
      primary: '200, 16, 46',
      secondary: '61,57,53'
    },
    Cardinals: {
      primary: '151,35,63',
      secondary: '16,24,32'
    },
    Chargers: {
      primary: '0,114,206',
      secondary: '255,184,28'
    },
    Chiefs: {
      primary: '200,16,46',
      secondary: '255,184,28'
    },
    Colts: {
      primary: '0,20,137',
      secondary: '255,255,255'
    },
    Cowboys: {
      primary: '4,30,66',
      secondary: '134,147,151'
    },
    Dolphins: {
      primary: '0,142,151',
      secondary: '245,130,32'
    },
    Eagles: {
      primary: '6,76,83',
      secondary: '0,0,0'
    },
    Falcons: {
      primary: '166,25,46',
      secondary: '16,24,32'
    },
    Giants: {
      primary: '0,30,98',
      secondary: '166,25,46'
    },
    Jaguars: {
      primary: '0,96,115',
      secondary: '212,159,18'
    },
    Jets: {
      primary: '12,55,29',
      secondary: '255,255,255'
    },
    Lions: {
      primary: '0,105,177',
      secondary: '162,170,173'
    },
    Packers: {
      primary: '23,94,34',
      secondary: '255,184, 28'
    },
    Panthers: {
      primary: '0,133,202',
      secondary: '16,24,32'
    },
    Patriots: {
      primary: '12,35,64',
      secondary: '200,16,46'
    },
    Raiders: {
      primary: '16,24,32',
      secondary: '191,192,191'
    },
    Rams: {
      primary: '0,21,50',
      secondary: '255,255,255'
    },
    Ravens: {
      primary: '26,25,95',
      secondary: '16,24,32'
    },
    Redskins: {
      primary: '134,38,51',
      secondary: '255,205,0'
    },
    Saints: {
      primary: '211,188,141',
      secondary: '16,24,31'
    },
    Seahawks: {
      primary: '0,21,51',
      secondary: '77,255,0'
    },
    Steelers: {
      primary: '255,184,28',
      secondary: '16,24,32'
    },
    Texans: {
      primary: '9,31,44',
      secondary: '166,25,46'
    },
    Titans: {
      primary: '65,143,222',
      secondary: '191,192,191'
    },
    Vikings: {
      primary: '79,38,131',
      secondary: '255, 198,47'
    }
  }

};
