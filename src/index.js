import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import store from './redux/store/store';

import './index.css'
import '../node_modules/font-awesome/css/font-awesome.min.css'
import 'semantic-ui-css/semantic.min.css';
import injectTapEventPlugin from 'react-tap-event-plugin'

import App from './App'
import registerServiceWorker from './registerServiceWorker'
injectTapEventPlugin()

ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  );
registerServiceWorker()
