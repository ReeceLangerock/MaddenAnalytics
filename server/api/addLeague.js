const express = require("express");
const router = express.Router();
const League = require("../models/leagueModel");

router.post("/",  async function (req, res, next) {
	reservedWords = ["about", "blog", "contact", "browse", "help", "add", "admin"];
	const { name, abbreviation } = req.body;

	if (reservedWords.includes(name) || reservedWords.includes(abbreviation)) {
		res.json({
			success: false,
			error: "Your league name or abbreviation is a reserved word, please try a different one.",
			league: {
				abbreviation: "",
				name: ``,
			},
		});
	}

	try {
		const foundLeague = await League.findOne({
			$or: [
				{ abbreviation: { $regex: new RegExp(abbreviation, "ig") } },
				{ name: { $regex: new RegExp(name, "ig") } },
			],
		});

		if (foundLeague) {
			let error = "";
			if (
				foundLeague.name.toLowerCase() !== name.toLowerCase() &&
				foundLeague.abbreviation.toLowerCase() === abbreviation.toLowerCase()
			) {
				error = "A league with that abbreviation already exists!";
			} else if (
				foundLeague.name.toLowerCase() === name.toLowerCase() &&
				foundLeague.abbreviation.toLowerCase() !== abbreviation.toLowerCase()
			) {
				error = "A league with that name already exists!";
			} else {
				error = "A league with that name and abbreviation already exists!";
			}

			res.json({
				success: false,
				error: error,
				league: foundLeague,
			});
		} else {
			const newLeague = new League(req.body);
			newLeague.console = "pending";
			newLeague.leagueId = "pending";
			const savedLeague = await newLeague.save();

			return res.status(200).json({
				success: true,
				league: savedLeague,
			});
		}
	} catch (err) {
		res.status(401).json({
			success: false,
			error: err,
		});
	}
});

module.exports = router;
