const express = require("express");
const router = express.Router();
const League = require("../models/leagueModel");
const PassingData = require("../models/statModels/passingDataModel");
const RushingData = require("../models/statModels/rushingDataModel");
const ReceivingData = require("../models/statModels/receivingDataModel");
const DefenseData = require("../models/statModels/defenseDataModel");
const Teams = require("../models/teamModel");
const ScoringData = require("../models/scoringDataModel");
const Roster = require("../models/rosterModel");

var teamDataParser = require("./../middleware/teamDataParser.js");
var scoringDataParser = require("./../middleware/scoringDataParser.js");

var rosterParser = require("./../middleware/rosterParser.js");

router.get("/", async function (req, res) {
  console.log("get");

  roster = await Roster.findOne({ _id: "5a3d5992668de40014f2ddbb" });
  console.log(roster);
  const result = roster.skillPositions.filter((player) => {
    return player.speedRating > 93;
  });
  res.json(result);
});

async function getLeague(req, res) {
  const { leagueId, abv, consoleType } = req.params;

  if (!abv) {
    res.end();
  }
  try {
    const foundLeague = await League.findOne({
      $or: [{ abbreviation: abv }, { leagueId: leagueId }],
    });

    // if league doesn't exist, do nothing
    if (!foundLeague) {
      res.status(404).send("league not found");
      res.end();
    }

    // if league is found, but isn't active this is the first time data is being sent
    // set league status to active and set up
    if (!foundLeague.leagueActivated) {
      console.log("activating league: " + abv);
      foundLeague.leagueActivated = true;
      foundLeague.leagueId = leagueId;
      foundLeague.console = consoleType;
      const teams = await new Teams({ leagueId: leagueId });
      const scoring = await new ScoringData({ leagueId: leagueId });
      const passing = await new PassingData({ leagueId: leagueId });
      const rushing = await new RushingData({ leagueId: leagueId });
      const receiving = await new ReceivingData({ leagueId: leagueId });
      const defense = await new DefenseData({ leagueId: leagueId });

      foundLeague.teams = teams._id;
      foundLeague.scoring = scoring._id;
      foundLeague.passing = passing._id;
      foundLeague.rushing = rushing._id;
      foundLeague.receiving = receiving._id;
      foundLeague.defense = defense._id;
      await teams.save();
      await scoring.save();
      await passing.save();
      await rushing.save();
      await receiving.save();
      await defense.save();
      await foundLeague.save();
    }

    // IF THE LEAGUE WAS FOUND, BUT THE ABBREVIATION OF LEAGUE ID DOESN'T MATCH
    // THEN THE USER IS SENDING INFO FOR THE WRONG LEAGUE, SO END THE CONNECTION
    if (foundLeague.abbreviation !== abv || foundLeague.leagueId !== leagueId) {
      res.status(403).send("leagueID and abbreviation don't match");
      res.end();
    }
    return foundLeague;
  } catch (e) {
    console.log("e", e);
  }
}

router.post("/:abv/:consoleType/:leagueId/leagueteams", function (req, res) {
  console.log("receiving leagueteams");

  let body = "";
  req.on("data", (chunk) => {
    body += chunk.toString();
  });
  req.on("end", async () => {
    try {

      const { leagueTeamInfoList } = JSON.parse(body);
      const foundLeague = await getLeague(req, res);

      // parse incoming data
      const teamData = await Teams.findOne({ _id: foundLeague.teams });

      if (teamData.teams.length === 0) {
        teamData.teams = teamDataParser.addTeamData(leagueTeamInfoList);
      } else {
        teamData.teams = teamDataParser.updateExistingTeamData(teamData.teams, leagueTeamInfoList);
      }
      await teamData.save();

    } catch (e) {
      console.log("here", e);
    }
  });
  res.status(200).end();
});

router.post("/:abv/:consoleType/:leagueId/standings", function (req, res) {
  console.log("receiving standings");

  let body = "";
  req.on("data", (chunk) => {
    body += chunk.toString();
  });
  req.on("end", async () => {
    try {
      const foundLeague = await getLeague(req, res);
      const { teamStandingInfoList } = JSON.parse(body);

      const teamData = await Teams.findOne({ _id: foundLeague.teams });
      const { weekIndex, seasonIndex, stageIndex } = teamStandingInfoList[0];

      foundLeague.currentSeason = seasonIndex;
      foundLeague.currentWeek = weekIndex;
      foundLeague.currentStage = stageIndex;
      // NEED TO CHECK STAGE INDEX, THINK 0 IS PRESEASON, 1 IS REGULAR SEASON AND 2 IS OFFSEASON
      if ((teamData.teams && stageIndex === 1) || stageIndex === 2) {
        teamData.teams = teamDataParser.updateUsers(teamData.teams, weekIndex, seasonIndex);
        await teamData.save();
      }
      await foundLeague.save();

      res.status(200).end();
    } catch (e) {
      console.log("standings error: ", e);
    }
  });
});

router.post("/:abv/:consoleType/:leagueId/team/*", function (req, res) {
  try {

    let body = "";
    req.on("data", (chunk) => {
      body += chunk.toString();
    });
    req.on("end", async () => {
      const { leagueId } = req.params;
      console.log("receiving rosters: ", leagueId, req.params[0]);

      const foundLeague = await getLeague(req, res);

      // ONLY CHECKING TEAMS ON ACTIVE ROSTER NOW, FREE AGENTS WHEN ON ACTIVE ROSTER WILL BE ADDED THEN AND REMAIN EVEN AFTER
      // if roster hasn't been created yet
      let roster;
      if (!foundLeague.roster) {
        console.log("adding roster");
        roster = await new Roster({ leagueId: leagueId });
        foundLeague.roster = roster._id;
        await foundLeague.save();
        await roster.save();
      } else {
        roster = await Roster.findOne({ _id: foundLeague.roster });
      }

      const { rosterInfoList } = JSON.parse(body);

      const newPlayers = await rosterParser.addSkillPlayers(rosterInfoList);

      newPlayers.forEach((player) => {
        // if the player already is in DB, remove them
        try {
          if (roster.skillPositions.id(player._id)) {
            roster.skillPositions.id(player._id).remove();
          }
          // add the player to the DB
          roster.skillPositions.push(player);
        } catch (e) {
          console.log(e);
        }
      });

      if (roster) {
        console.log('before reoster save')
        await roster.save();
        console.log('after reoster save')

      }
      res.end();
    });
  } catch (e) {
    console.log("Roster error:", e);
  }
});

router.post("/:abv/:consoleType/:leagueId/*", function (req, res) {
  try {
    let body = "";
    req.on("data", (chunk) => {
      body += chunk.toString();
    });
    req.on("end", async () => {
      const foundLeague = await getLeague(req, res);
      const importType = req.params[0];

      if (importType.includes("pre")) {
        res.end();
      }

      if (importType.includes("reg")) {
        const {
          gameScheduleInfoList,
          playerPassingStatInfoList,
          playerRushingStatInfoList,
          playerDefensiveStatInfoList,
          playerReceivingStatInfoList,
        } = JSON.parse(body);

        const week = req.params[0].split("/")[2] - 1; // -1 to get weeks to 0 index
        const dataType = req.params[0].split("/")[3];
        console.log(req.params[0].split("/"), week, dataType);

        const scoring = await ScoringData.findOne({ _id: foundLeague.scoring });
        let season;
        let weekCompleted;
        switch (dataType) {
          case "schedules":
            season = gameScheduleInfoList[0].seasonIndex;
            try {
              if (scoring.data[season] && scoring.data[season][week].weekCompleted) {
                break;
              }
            } catch (e) {
              console.log('score e',e)
            }

            let [parsedData, weekCompleted] = scoringDataParser.addScoringData(
              gameScheduleInfoList,
              week
            );
            if (scoring.data[season]) {
              if (scoring.data[season][week]) {
                scoring.data[season][week] = {
                  week: week,
                  season: season,
                  teamData: parsedData,
                  weekCompleted: weekCompleted,
                };
              } else {
                scoring.data[season][week] = {
                  week: week,
                  season: season,
                  teamData: parsedData,
                  weekCompleted: weekCompleted,
                };
              }
            } else {
              scoring.data[season] = [];
              scoring.data[season][week] = {
                week: week,
                season: season,
                teamData: parsedData,
                weekCompleted: weekCompleted,
              };

            }
            scoring.markModified('data');
            await scoring.save();
            break;
          
          case "passing":
            const DB_PASSING_DATA = await PassingData.findOne({ _id: foundLeague.passing });

            season = playerPassingStatInfoList[0].seasonIndex;
            let passWeekCompleted = false;
            try {
              passWeekCompleted = DB_PASSING_DATA.passingData[season][week].weekCompleted;
            } catch (e) { }

            try {
              if (scoring.data[season][week].weekCompleted && passWeekCompleted) {
                console.log(`Season ${season}, Week ${week} passing data locked, skipping import`);
                break;
              }
            } catch (e) { }

            let passingData = scoringDataParser.addPassingData(playerPassingStatInfoList, week);
            if (DB_PASSING_DATA.passingData[season]) {
              if (DB_PASSING_DATA.passingData[season][week]) {
                DB_PASSING_DATA.passingData[season][week] = {
                  week: week,
                  season: season,
                  playerStats: passingData,
                };
              } else {
                DB_PASSING_DATA.passingData[season][week] = {
                  week: week,
                  season: season,
                  playerStats: passingData,
                };
              }
            } else {
              DB_PASSING_DATA.passingData[season] = [];
              DB_PASSING_DATA.passingData[season][week] = {
                week: week,
                season: season,
                playerStats: passingData,
              };
            }

            try {
              if (scoring.data[season][week].weekCompleted) {
                DB_PASSING_DATA.passingData[season][week].weekCompleted = true;
              }
            } catch (e) { }

            DB_PASSING_DATA.markModified('passingData');

            await DB_PASSING_DATA.save();
            break;

          case "rushing":
            const DB_RUSHING_DATA = await RushingData.findOne({ _id: foundLeague.rushing });

            season = playerRushingStatInfoList[0].seasonIndex;
            let rushWeekCompleted = false;
            try {
              rushWeekCompleted = DB_RUSHING_DATA.rushingData[season][week].weekCompleted;
            } catch (e) { }

            try {
              if (scoring.data[season][week].weekCompleted && rushWeekCompleted) {
                console.log(`Season ${season}, Week ${week} rushing data locked, skipping import`);

                break;
              }
            } catch (e) { }

            let rushingData = scoringDataParser.addRushingData(playerRushingStatInfoList, week);
            if (DB_RUSHING_DATA.rushingData[season]) {
              if (DB_RUSHING_DATA.rushingData[season][week]) {
                DB_RUSHING_DATA.rushingData[season][week] = {
                  week: week,
                  season: season,
                  playerStats: rushingData,
                };
              } else {
                DB_RUSHING_DATA.rushingData[season][week] = {
                  week: week,
                  season: season,
                  playerStats: rushingData,
                };
              }
            } else {
              DB_RUSHING_DATA.rushingData[season] = [];
              DB_RUSHING_DATA.rushingData[season][week] = {
                week: week,
                season: season,
                playerStats: rushingData,
              };
            }

            try {
              if (scoring.data[season][week].weekCompleted) {
                DB_RUSHING_DATA.rushingData[season][week].weekCompleted = true;
              }
            } catch (e) { }
            DB_RUSHING_DATA.markModified('rushingData');

            await DB_RUSHING_DATA.save();
            break;

          case "receiving":
            const DB_RECEIVING_DATA = await ReceivingData.findOne({ _id: foundLeague.receiving });

            season = playerReceivingStatInfoList[0].seasonIndex;
            let recWeekCompleted = false;
            try {
              recWeekCompleted = DB_RECEIVING_DATA.receivingData[season][week].weekCompleted;
            } catch (e) { }

            try {
              if (scoring.data[season][week].weekCompleted && recWeekCompleted) {
                console.log(
                  `Season ${season}, Week ${week} receiving data locked, skipping import`
                );

                break;
              }
            } catch (e) { }

            let receivingData = scoringDataParser.addReceivingData(
              playerReceivingStatInfoList,
              week
            );
            if (DB_RECEIVING_DATA.receivingData[season]) {
              if (DB_RECEIVING_DATA.receivingData[season][week]) {
                DB_RECEIVING_DATA.receivingData[season][week] = {
                  week: week,
                  season: season,
                  playerStats: receivingData,
                };
              } else {
                DB_RECEIVING_DATA.receivingData[season][week] = {
                  week: week,
                  season: season,
                  playerStats: receivingData,
                };
              }
            } else {
              DB_RECEIVING_DATA.receivingData[season] = [];
              DB_RECEIVING_DATA.receivingData[season][week] = {
                week: week,
                season: season,
                playerStats: receivingData,
              };
            }
            try {
              if (scoring.data[season][week].weekCompleted) {
                DB_RECEIVING_DATA.receivingData[season][week].weekCompleted = true;
              }
            } catch (e) { }

            DB_RECEIVING_DATA.markModified('receivingData');

            await DB_RECEIVING_DATA.save();
            break;

          case "defense":
            const DB_DEFENSE_DATA = await DefenseData.findOne({ _id: foundLeague.defense });
            season = playerDefensiveStatInfoList[0].seasonIndex;

            let defWeekCompleted = false;
            try {
              defWeekCompleted = DB_DEFENSE_DATA.defenseData[season][week].weekCompleted;
            } catch (e) { }

            try {
              if (scoring.data[season][week].weekCompleted && defWeekCompleted) {
                console.log(
                  `Season ${season}, Week ${week} defensive data locked, skipping import`
                );
                break;
              }
            } catch (e) { }

            let defenseData = scoringDataParser.addDefensiveData(playerDefensiveStatInfoList, week);
            if (DB_DEFENSE_DATA.defenseData[season]) {
              if (DB_DEFENSE_DATA.defenseData[season][week]) {
                DB_DEFENSE_DATA.defenseData[season][week] = {
                  week: week,
                  season: season,
                  playerStats: defenseData,
                };
              } else {
                DB_DEFENSE_DATA.defenseData[season][week] = {
                  week: week,
                  season: season,
                  playerStats: defenseData,
                };
              }
            } else {
              DB_DEFENSE_DATA.defenseData[season] = [];
              DB_DEFENSE_DATA.defenseData[season][week] = {
                week: week,
                season: season,
                playerStats: defenseData,
              };
            }

            try {
              if (scoring.data[season][week].weekCompleted) {
                DB_DEFENSE_DATA.defenseData[season][week].weekCompleted = true;
              }
            } catch (e) { }

            DB_DEFENSE_DATA.markModified('defenseData');

            await DB_DEFENSE_DATA.save();
            break;

          default:
            res.end();
            break;
        }
      }
      res.end();
    });
  } catch (e) {
    console.log("main eerror:", e);
  }
});

module.exports = router;
