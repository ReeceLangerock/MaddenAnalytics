const express = require('express')
const router = express.Router()
const League = require('../models/leagueModel')

const mongoose = require('mongoose')

router.post('/', async function (req, res, next) {
    const { username, password } = req.body
    if (username !== process.env.adminUsername || password !== process.env.adminPassword) {
        res.json({
            result: false
        })
    }
    res.json({ result: true })


    try {
        let leagues = await League.find({}).limit(25)


        return res.status(200).json(leagues)
    } catch (err) {
        console.log(err)
    }

    res.json({ result: true })
})

module.exports = router
