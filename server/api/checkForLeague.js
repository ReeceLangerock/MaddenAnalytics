const express = require('express')
const router = express.Router()
const League = require('../models/leagueModel')

const mongoose = require('mongoose')

router.post('/*', async function (req, res, next) {

	var io = req.app.get('socketio')

	// FOR CHECKING DATA, WE ONLY NEED ONE POST REQUEST, OTHERWISE SOCKET WILL SEND MULTIPLE TIMES
	// SO IF THE POST ISN'T FOR LEAGUETEAMS, THEN END THE CONNECTION

	if (req.params[0].split('/')[2] !== 'leagueteams') {
		res.end()
	}


	const leagueId = req.params[0].split('/')[1]
	const consoleType = req.params[0].split('/')[0]

	try {
		const foundLeague = await League.findOne({
			leagueId: leagueId
		})


		if (foundLeague) {
			io.emit('leagueFoundStatus', foundLeague)
		} else {
			io.emit('leagueFoundStatus', {
				found: false,
				console: consoleType
			})
		}
		res.json({ result: true })
	} catch (err) {
		console.log(err)
		res.json({ result: false })
	}

})



module.exports = router
