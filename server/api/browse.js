const express = require("express");
const router = express.Router();
const League = require("../models/leagueModel");

const mongoose = require("mongoose");

router.post("/", async function (req, res, next) {
  try {
    let leagues;
    if (req.body && req.body.query) {
      const { query } = req.body;
      leagues = await League.find({
        $or: [
          { abbreviation: { $regex: new RegExp(query, "ig") } },
          { name: { $regex: new RegExp(query, "ig") } },
        ],
      }).limit(25);
    } else {
      leagues = await League.find({}).limit(25);
    }

    return res.status(200).json(leagues);
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
