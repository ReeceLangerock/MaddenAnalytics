const router = require('express').Router();

router.use('/add', require('./addLeague'));
router.use('/dk', require('./dk'));
router.use('/league', require('./league'));
router.use('/browse', require('./browse'));
router.use('/import', require('./import'));
router.use('/admin', require('./admin'))


router.use(function (err, req, res, next) {
  if (err.name === 'ValidationError') {
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function (errors, key) {
        errors[key] = err.errors[key].message;
        return errors;
      }, {})
    });
  }
  return next(err);
});

module.exports = router;
