const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')
const DKModel = require('./../models/dkModel')
const validCodes = ['amukuh',
    'ovujat',
    'asisiq',
    'etayef',
   'ajenow',
    'owecux',
    'exudoh',
    'ijodec',
    'efukax',
    'aniniy',
    'asijij',
    'oyocur',
    'akuzuy',
    'epejun',
    'apajen',
    'exuwip',
    'ucaxoz',
    'oxunef',
    'idabot',
    'exawuk',
    'araruj',
    'enunaf',
    'emicir',
    'usetog',
    'izezoh',
    'akoseb',
    'obupuy',
    'asovop',
    'adenaj',
    'oyegan',
    'irevuq',
    'ibutot']

router.post('/', async function (req, res, next) {

	let { team, code, roster } = req.body
    if(validCodes.includes(code) === false && code !== 'fun'){
        res.status(500).json({
            success: 'bad code'

        })
    }
    if(code === 'fun'){
        code = `${team}-${code}`
    }
    let DK = await DKModel.findOne({
        code: code 
    })

	try {
        if(!DK){

            DK = new DKModel(req.body)
        }
        DK.team = team
        DK.code = code
        DK.roster = roster
			const savedDK = await DK.save()

			return res.status(200).json({
				success: 'all good'
			})
	} catch (err) {
		res.status(500).json({
            success: false
        })
    }
    

})

router.get('/', async function (req, res, next) {

   
    let DK = await DKModel.find({
    })

		return res.status(200).json(DK)


	

})

module.exports = router
