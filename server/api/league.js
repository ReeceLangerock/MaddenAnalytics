const express = require('express')
const router = express.Router()
const League = require('../models/leagueModel')
const PassingData = require('../models/statModels/passingDataModel')
const ReceivingData = require('../models/statModels/receivingDataModel')
const RushingData = require('../models/statModels/rushingDataModel')
const DefenseData = require('../models/statModels/defenseDataModel')
const Roster = require('../models/rosterModel')
const ScoringData = require('../models/scoringDataModel')
const Teams = require('../models/teamModel')



const mongoose = require('mongoose')

router.get('/:abbr', async function (req, res, next) {
	const { abbr } = req.body
	try {
		const leagueDataID = await League.findOne({
			abbreviation: { $regex: new RegExp(abbr, "ig") }
		}).select('data ')

		return res.status(200).json(leagues)
	} catch (err) {
		console.log(err)
	}

	res.json({ result: true })
})

router.get('/:abbr/:dataToGet/:week', async function (req, res, next) {
	const { abbr, dataToGet, week } = req.params

	if (abbr === null) {
		res.end()
	}
	try {
		if (dataToGet === 'info') {
			const info = await League.findOne({
				abbreviation: { $regex: new RegExp(abbr, "ig") }

			}).select('leagueID name abbreviation leagueActivated currentSeason currentWeek currentStage')

			if (info) {
				res.status(200).json({ info: info })

			} else {
				res.status(200).json({ info: { status: 'noleague' } })
			}
		}

		const leagueInfo = await League.findOne({
			abbreviation: { $regex: new RegExp(abbr, "ig") }
		})
		// const currentSeason = leagueInfo.currentSeason
		let data

		switch (dataToGet) {


			case 'scoringData':
				let scoringData = await ScoringData.findOne({
					_id: leagueInfo.scoring
				}).select('data')

				res.status(200).json({
					scoringData: scoringData.data,
					season: leagueInfo.currentSeason,
					week: leagueInfo.currentWeek,
				})
				break



			case 'skill_players_partial', 'skillPlayers':
				let skillPositionData = await Roster.findOne({
					_id: leagueInfo.roster
				}).select('skillPositions.firstName skillPositions.lastName skillPositions._id skillPositions.position skillPositions.teamId ')
				res.status(200).json({
					skillPositionData: skillPositionData.skillPositions,
					season: leagueInfo.currentSeason,
					week: leagueInfo.currentWeek,
				})
				break

			case 'weeklyData':
				console.log('getting weeklyData')

				const DB_PASSING_DATA = await PassingData.findOne({
					_id: leagueInfo.passing
				})
				const DB_RUSHING_DATA = await RushingData.findOne({
					_id: leagueInfo.rushing
				})
				const DB_RECEIVING_DATA = await ReceivingData.findOne({
					_id: leagueInfo.receiving
				})
				// const DB_DEFENSE_DATA = await DefenseData.findOne({
				// 	_id: leagueInfo.defense
				// })

				const weeklyData = {
					season: leagueInfo.currentSeason,
					week: leagueInfo.currentWeek,

					passingData: DB_PASSING_DATA.passingData,
					rushingData: DB_RUSHING_DATA.rushingData,
					receivingData: DB_RECEIVING_DATA.receivingData,
					// defenseData: DB_DEFENSE_DATA.defenseData
				}


				if (week !== 'undefined') {
					res.status(200).json({
						weeklyData: {

							passingData: weeklyData.passingData[week],
							rushingData: weeklyData.rushingData[week],
							receivingData: weeklyData.receivingData[week]
						}
					})
				} else {
					res.status(200).json({ weeklyData: weeklyData })

				}

				break

			case 'teams':

				data = await Teams.findOne({
					_id: leagueInfo.teams
				}).select('teams.abbreviation teams.name teams.teamId teams.users')

				res.status(200).json({
					teams: data.teams,
					season: leagueInfo.currentSeason,
					week: leagueInfo.currentWeek,
				})


		}

		// return res.status(200).json(leagues)
	} catch (err) {
		console.log(err)
	}
})

module.exports = router
