const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Players = require('../models/playerSchema')

const rosterSchema = new Schema(
	{
		leagueId: String,
		skillPositions: [Players],
		allOtherPositions: [Players]

	}, {
		usePushEach: true
	  }


)

const rosterModel = mongoose.model('roster', rosterSchema, 'roster')

module.exports = rosterModel
