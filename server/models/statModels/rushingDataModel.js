const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const RushingData = require('./rushingDataSchema')


const rushingDataSchema = new Schema(
  {
    leagueId: String,
    //SCORING DATA TRACKED ACROSS HISTORY OF LEAGUE, SO IN NESTED ARRAY
    // FIRST FOR SEASON, SECOND FOR WEEK
    rushingData: [[RushingData]],

  }, {
    timestamps: true
  }
);

const rushingDataModel = mongoose.model('rushingData', rushingDataSchema, 'rushingData');
module.exports = rushingDataModel;