const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ReceivingData = require('./receivingDataSchema')


const receivingDataSchema = new Schema(
  {
    leagueId: String,
    //SCORING DATA TRACKED ACROSS HISTORY OF LEAGUE, SO IN NESTED ARRAY
    // FIRST FOR SEASON, SECOND FOR WEEK
    receivingData: [[ReceivingData]],

  }, {
    timestamps: true
  }
);

const receivingDataModel = mongoose.model('receivingData', receivingDataSchema, 'receivingData');
module.exports = receivingDataModel;