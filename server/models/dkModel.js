const mongoose = require('mongoose')
const Schema = mongoose.Schema

const dkSchema = new Schema(
	{
		team: String,
		code: String,
		roster: {
			
		}

	}, {
		usePushEach: true
	  }


)

const dkModel = mongoose.model('dk', dkSchema, 'dk')

module.exports = dkModel
