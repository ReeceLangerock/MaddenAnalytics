const mongoose = require('mongoose')
const Schema = mongoose.Schema

const leagueSchema = new Schema(
	{
		name: String,
		abbreviation: String,
		leagueId: String,
		console: String,
		currentSeason: Number,
		currentWeek: Number,
		currentStage: Number,
		leagueActivated: { type: Boolean, default: false },
		teams: { type: mongoose.Schema.Types.ObjectId, ref: 'teamData' },
		scoring: { type: mongoose.Schema.Types.ObjectId, ref: 'scoringData' },
		passing: { type: mongoose.Schema.Types.ObjectId, ref: 'passingData' },
		defense: { type: mongoose.Schema.Types.ObjectId, ref: 'defenseData' },
		rushing: { type: mongoose.Schema.Types.ObjectId, ref: 'rushingData' },
		receiving: { type: mongoose.Schema.Types.ObjectId, ref: 'receivingData' },
		roster: { type: mongoose.Schema.Types.ObjectId, ref: 'roster' }
	},
	// adds createdAt, updatedAt timestamps automatically
	{
		timestamps: true,
		toJSON: { virtuals: true }
	}

)
leagueSchema.virtual('teamData', {
	ref: 'teamData',
	localField: '_id',
	foreignField: 'leagueId',
	justOne: true
});
leagueSchema.virtual('scoringData', {
	ref: 'scoringData',
	localField: '_id',
	foreignField: 'leagueId',
	justOne: true
});
leagueSchema.virtual('passingData', {
	ref: 'passingData',
	localField: '_id',
	foreignField: 'leagueId',
	justOne: true
});
leagueSchema.virtual('receivingData', {
	ref: 'receivingData',
	localField: '_id',
	foreignField: 'leagueId',
	justOne: true
});
leagueSchema.virtual('rushingData', {
	ref: 'rushingData',
	localField: '_id',
	foreignField: 'leagueId',
	justOne: true
});
leagueSchema.virtual('defenseData', {
	ref: 'defenseData',
	localField: '_id',
	foreignField: 'leagueId',
	justOne: true
});

const leagueModel = mongoose.model('league', leagueSchema, 'leagues')
module.exports = leagueModel
