
var parser = {


	updateExistingTeamData(teams, newData) {
		const sortedTeams = teams.sort((a, b) => {
			return a.teamId - b.teamId
		})

		const sortedNewData = newData.sort((a, b) => {
			return a.teamId - b.teamId
		})

		for (let i = 0; i < teams.length; i++) {
			let last = sortedTeams[i].users.length - 1
			if (
				sortedTeams[i].users[last].username !==
				sortedNewData[i].userName
			) {
				sortedTeams[i].users.push({
					username: sortedNewData[i].userName
				})
			}
		}

		return sortedTeams
	},

	addTeamData(data) {
		const teams = data.map(team => {
			let newTeam = {
				name: team.displayName,
				abbreviation: team.abbrName,
				primaryColor: team.primaryColor,
				teamId: team.teamId,
				users: [
					{
						username: team.userName
					}
				]
			}

			return newTeam
		})

		return teams
	},

	getCapInfo(data) {
		const teams = data.map(team => {
			let newTeam = {
				name: team.displayName,
				abbreviation: team.abbrName,
				primaryColor: team.primaryColor,
				teamId: team.teamId,
				users: [
					{
						username: team.userName
					}
				]
			}

			return newTeam
		})

		return teams
	},
	updateUsers(teams, week, season) {
		const updatedTeams = teams.map(team => {
			const last = [team.users.length - 1]
			if (team.users[last].firstSeen.length === 0) {
				team.users[last].firstSeen = [season, week]
			}
			team.users[last].lastSeen = [season, week]
			return team
		})

		return updatedTeams
	}
}

module.exports = parser
