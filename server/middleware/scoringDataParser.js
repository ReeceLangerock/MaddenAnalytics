var parser = {
	addScoringData(data, week) {
		const weeklyData = []
		// should rename to game?
		let weekCompleted = true
		for (let team of data) {
			weeklyData.push({
				teamID: team.homeTeamId,
				opponentTeamID: team.awayTeamId,
				pointsScored: team.homeScore,
				pointsAllowed: team.awayScore,
				scheduleId: team.scheduleId,
				status: team.status
				
			})

			if(team.status === 1){
				weekCompleted = false;
			}
		}

		return [weeklyData, weekCompleted]
	},


	addPassingData(data, week) {

		const weeklyData = []
		for (let player of data) {
			weeklyData.push({
				teamId: player.teamId,
				rosterId: player.rosterId,
				scheduleId: player.scheduleId,
				passAtt: player.passAtt,
				passComp: player.passComp,
				passInts: player.passInts,
				passLongest: player.passLongest,
				passPts: player.passPts,
				passYds: player.passYds,
				passTDs: player.passTDs,
				passSacks: player.passSacks
			})
		}

		return weeklyData
	},
	addRushingData(data, week) {

		const weeklyData = []
		for (let player of data) {
			weeklyData.push({
				teamId: player.teamId,
				rosterId: player.rosterId,
				scheduleId: player.scheduleId,
				
				rushAtt: player.rushAtt,
				rushBrokenTackles: player.rushBrokenTackles,
				rushFum: player.rushFum,
				rushLongest: player.rushLongest,
				rushPts: player.rushPts,
				rushYds: player.rushYds,
				rushTDs: player.rushTDs,
				rush20PlusYds: player.rush20PlusYds,
				rushYdsAfterContact: player.rushYdsAfterContact
			})
		}

		return weeklyData
	},
	addDefensiveData(data, week) {

		const weeklyData = []
		for (let player of data) {
			weeklyData.push({
				teamId: player.teamId,
				rosterId: player.rosterId,
				scheduleId: player.scheduleId,
				
				defCatchAllowed: player.defCatchAllowed,
				defDeflections: player.defDeflections,
				defForcedFum: player.defForcedFum,
				defFumRec: player.defFumRec,
				defInts: player.defInts,
				defIntReturnYds: player.defIntReturnYds,
				defSacks: player.defSacks,
				defSafeties: player.defSafeties,
				defTDs: player.defTDs,
				defTotalTackles: player.defTotalTackles
			})
		}
		return weeklyData
	},
	addReceivingData(data, week) {

		const weeklyData = []
		for (let player of data) {
			weeklyData.push({
				teamId: player.teamId,
				rosterId: player.rosterId,
				scheduleId: player.scheduleId,
				
				recCatches: player.recCatches,
				recDrops: player.recDrops,
				recLongest: player.recLongest,
				recPts: player.recPts,
				recYds: player.recYds,
				recTDs: player.recTDs,
				passTDs: player.passTDs,
				recYdsAfterCatch: player.recYdsAfterCatch
			})
		}

		return weeklyData
	}
}

module.exports = parser
