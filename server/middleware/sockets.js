module.exports.listen = function(app) {
	const io = require('socket.io')(app)

    io.on('connection', (client) => {
        console.log('connected')
        
})
    io.on('subscribeToTimer', (interval) => {
        console.log('client is subscribing to timer with interval ', interval);
        setInterval(() => {
          client.emit('timer', new Date());
        }, interval);
      });

	return io
}
