//SETUP
const express = require("express");
// const expressWinston = require('express-winston');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
// const winstonInstance = require('./winston');
const cors = require('cors')
const config = require("./config");
const app = express();

app.use(cors())
// app.use(
// 	bodyParser.urlencoded({
// 		extended: true
// 	})
// )
// app.use(
// 	bodyParser.json({
// 		limit: '50mb'
// 	})
// )

// create application/json parser
app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	res.header(
		'Access-Control-Allow-Headers',
		'Origin, X-Requested-With, Content-Type, Accept'
	)
	next()
})

const URI = (MONGO_URI = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.bno9v.mongodb.net/test?retryWrites=true&w=majority`);
mongoose.connect(URI, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });
const connection = mongoose.connection;
connection.once("open", () => {
  console.log("MongoDB database connection established successfully");
});

// register services, models and routes, routes
// app.use('/api/dk', function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });
require("./models/leagueModel");

app.use("/im", require("./api/import"));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use("/api", require("./api"));
app.use("/lg", require("./api/checkForLeague"));
app.use(express.static(path.join(__dirname, "./../build")));
// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "./../build/index.html"));
});

// generic error handler
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

//SOCKET SETUP
const server = require("http").createServer(app);
const io = require("socket.io")(server);
app.set("socketio", io);

// required for testing, don't listen on port twice
if (!module.parent) {
  server.listen(config.port, function () {
    console.log(`DeepDive DEV SERVER listening on port ${config.port}!`);
  });
}

module.exports = server;
