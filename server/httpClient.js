import axios from 'axios';

const baseApiUrl = 'http://localhost:3001/api'; //hard coded for now - use env var later

// store token globally - updated on login/logout
let token = null;
const setToken = _token => {
  token = _token;
};

const instance = axios.create();
// add token to request if it exists
instance.interceptors.request.use(
  config => {
    if (token) {
      config.headers = { Authorization: `Token ${token}` };
    }
    return config;
  },
  err => Promise.reject(err)
);

const requests = {
  getResourceById: id => {
    return instance.get(`${baseApiUrl}/resource/${id}`);
  },
  getResources: (sort, count, pagination, categories) => {
    return instance.get(`${baseApiUrl}/resource`, {
      params: {
        sort,
        count,
        pagination,
        categories
      }
    });
  },
  handleResourceRating: (resourceId, rating) => {
    // rateResource with null assume deletion request
    if (rating === null) {
      return instance.delete(`${baseApiUrl}/resource/${resourceId}/rating`);
    }
    return instance.put(`${baseApiUrl}/resource/${resourceId}/rating`, {
      rating
    });
  },
  searchResources: searchText => {
    return instance.post(`${baseApiUrl}/resource/search`, {
      searchText
    });
  },
  postResource: resource => {
    return instance.post(`${baseApiUrl}/resource`, resource);
  },
  getCurrentUser: () => {
    return instance.get(`${baseApiUrl}/auth/currentUser`);
  }
};

export { requests, setToken };
